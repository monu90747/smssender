<?php

/**
 * Autoloading classes
 * @param $class
 */
function __autoload($class){
    $class = str_replace("\\", "/", $class).".php";
    $classFile = str_replace("MRPHPSDK/MRTerminal", "", __DIR__);
    $classFile.=$class;
    if(is_file($classFile)&&!class_exists($class)) include_once $classFile;
}
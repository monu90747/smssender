<?php

namespace MRPHPSDK\MRDatabase;

use PDO;
use MRPHPSDK\MRConfig\MRConfig;

class MRDatabase{

    protected $connection;

    /**
     * Database initialization.
     */
    function __construct(){
        try {
            $this->connection = new PDO("mysql:host=".MRConfig::get('database')['hostname'].";dbname=".MRConfig::get('database')['databaseName'], MRConfig::get('database')['userName'], MRConfig::get('database')['password']);
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch(\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * Call this method to get singleton
     *
     * @return Route
     */
    public static function instance()
    {
        static $inst = null;
        if ($inst === null) {
            $inst = new MRDatabase();
        }
        return $inst;
    }

    public static function query($query){
        try {
            $statement = MRDatabase::instance()->connection->prepare($query);
            $statement->execute();
        } catch(\Exception $e ) {
            throw new \Exception($e->getMessage());
        }
    }

    public static function select($query){
        try {
            $statement = MRDatabase::instance()->connection->prepare($query);
            $statement->execute();
            return $statement->fetchAll(PDO::FETCH_ASSOC);
        } catch(\Exception $e ) {
            return [];
        }
    }

    public static function getColumns($table){
        try{
            $statement = MRDatabase::instance()->connection->prepare("SHOW COLUMNS FROM $table");
            $statement->execute();
            return $statement->fetchAll(PDO::FETCH_CLASS);
        }
        catch(\Exception $e){
            throw new \Exception($e->getMessage());
        }
    }

    public static function insert($table, $params){
        if(count($params)>0){
            $keys = [];
            $allValues = [];
            foreach($params as $key=>$value) {
                $keys[] = $key;
                $allValues[":".$key] = $value;
            }
            $query = "INSERT INTO $table (".implode(',', $keys).") VALUES (:".implode(",:", $keys).")";
            try {
                $statement = MRDatabase::instance()->connection->prepare($query);
                $statement->execute($allValues);
                return MRDatabase::instance()->connection->lastInsertId();
            } catch(\Exception $e ) {
                throw new \Exception($e->getMessage());
            }
        }
        else{
            return "There is not data to save.";
        }
    }

    public static function update($table, $params){
        if(count($params)>0){
            $keys = [];
            $allValues = [];
            foreach($params as $key=>$value) {
                if($key == "id") continue;
                $keys[] = $key."=:".$key;
                $allValues[":".$key] = $value;
            }
            $query = "UPDATE $table SET ".implode(", ",$keys)." WHERE id='".$params['id']."'";
            try {
                $statement = MRDatabase::instance()->connection->prepare($query);
                $statement->execute($allValues);
            } catch(\Exception $e ) {
                throw new \Exception($e->getMessage());
            }
        }
        else{
            return "There is not data to update.";
        }
    }

}
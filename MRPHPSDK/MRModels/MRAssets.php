<?php

namespace MRPHPSDK\MRModels;

use MRPHPSDK\MRVendor\MRAccessToken\MRAccessToken;
use MRPHPSDK\MRException\MRException;

class MRAssets{

	function saveImageAssets($tempImage, $savedLocation){
        if($tempImage && $tempImage!=''){
            $fileName = strtotime(date('Y-m-d H:i:s'));
            $encoded_photo = $tempImage;
            $photo = base64_decode($encoded_photo);
            $f = finfo_open();
            $mime_type = finfo_buffer($f, $photo, FILEINFO_MIME_TYPE);
            $imgtype = explode('/', $mime_type);
            $imgextn = $imgtype[1];
            $userimg = $fileName . "." . $imgextn;
            $filePath = $savedLocation . $userimg;
            $file = fopen($filePath, 'wb');
            fwrite($file, $photo);
            fclose($file);
            return $userimg;
        }
        else{
            return "";
        }
    }

    function saveAssetsFromURL($url, $savedLocation){
        if($url!=''){
            $fileName = strtotime(date('Y-m-d H:i:s')).".png";
            $img = $savedLocation."/".$fileName;
            file_put_contents($img, file_get_contents($url));
            return $fileName;
        }
        else{
            return "";
        }

    }

}
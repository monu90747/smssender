<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Sign In | <?=PRODUCTNAME ?></title>
    <!-- Favicon-->
    <link rel="icon" href="/assets/logo-small.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="/assets/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="/assets/plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="/assets/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="/assets/css/style.css" rel="stylesheet">
</head>

<body style="background-color: #00BCD4">

<div class="row" style="margin: 0; padding: 0;">
    <div class="col-lg-8" style="padding: 0; margin: 0;">
        <div class="login-page">
            <div class="login-box">
                <div class="logo" style="text-align: center">
                    <img src="/assets/logo.png"><br><br>
                    <small style="font-size: 20px;"><b>We are sms provider</b></small>
                </div>
                <div class="card">
                    <div class="body">
                        <form id="sign_in" method="POST" autocomplete="off">
                            <?php include("include/message.php"); ?>
                            <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">email</i>
                        </span>
                                <div class="form-line">
                                    <input type="text" class="form-control" name="email" value="" placeholder="Email or Mobile Number" autocomplete="false" required autofocus>
                                </div>
                            </div>
                            <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                                <div class="form-line">
                                    <input type="password" class="form-control" name="password" placeholder="Password" autocomplete="false" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-8 p-t-5">
                                    <input type="checkbox" name="rememberme" id="rememberme" class="filled-in chk-col-pink">
                                    <label for="rememberme">Remember Me</label>
                                </div>
                                <div class="col-xs-4">
                                    <button class="btn btn-block bg-pink waves-effect" type="submit">SIGN IN</button>
                                </div>
                            </div>
                            <div class="row m-t-15 m-b--20">
                                <div class="col-xs-6">
                                    <a href="/auth/signup">Register Now!</a>
                                </div>
                                <div class="col-xs-6 align-right">
                                    <a href="/auth/forgotpassword">Forgot Password?</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4 bg-teal" style="padding: 5px; margin: 0; min-height: 700px;">
        <div style="height: 30px;"></div>
        <div style="text-align: center">
            Welcome to HamaraDamoh.com
            <hr/>
        </div>

        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <a href="/auth/price"><h2><u>View SMS Pricing</u></h2></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            CONTACT US FOR MORE DETAILS
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-4 form-control-label" style="margin-bottom: 0;">
                                <label for="email_address_2">CONTACT :</label>
                            </div>
                            <div class="col-lg-8" style="top:8px;margin-bottom: 0;">
                                <div class="form-group">
                                    +91-8103478849
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-4 form-control-label">
                                <label for="email_address_2">EMAIL :</label>
                            </div>
                            <div class="col-lg-8" style="top:8px;margin-bottom: 0;">
                                <div class="form-group">
                                    vikasaj9098@gmail.com
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Jquery Core Js -->
<script src="/assets/plugins/jquery/jquery.min.js"></script>

<!-- Bootstrap Core Js -->
<script src="/assets/plugins/bootstrap/js/bootstrap.js"></script>

<!-- Waves Effect Plugin Js -->
<script src="/assets/plugins/node-waves/waves.js"></script>

<!-- Validation Plugin Js -->
<script src="/assets/plugins/jquery-validation/jquery.validate.js"></script>

<!-- Custom Js -->
<script src="/assets/js/admin.js"></script>
<script src="/assets/js/pages/examples/sign-in.js"></script>
</body>

</html>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Sign In | <?=PRODUCTNAME ?></title>
    <!-- Favicon-->
    <link rel="icon" href="/assets/logo-small.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="/assets/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="/assets/plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="/assets/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="/assets/css/style.css" rel="stylesheet">
</head>

<body style="background-color: #00BCD4">
<div class="logo" style="text-align: center; margin: 20px;padding: 20px">
    <img src="/assets/logo.png"><br><br>
    <small style="font-size: 20px;color: white"><b>We are sms provider</b></small>
</div>
<div class="row" style="margin: 0; padding: 0;">

    <div class="col-lg-3">
        <div class="card">
            <div class="header bg-pink" style="text-align: center">
                <h2>
                    10,000 SMS
                </h2>
            </div>
            <div class="body" style="padding: 0;">
                <div class="list-group">
                    <a href="javascript:void(0);" class="list-group-item">Transactional SMS</a>
                    <a href="javascript:void(0);" class="list-group-item">Unlimited Validity</a>
                    <a href="javascript:void(0);" class="list-group-item">Low in Price</a>
                    <a href="javascript:void(0);" class="list-group-item active" style="text-align: center">
                        ₹ 2,000/-
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-3">
        <div class="card">
            <div class="header bg-teal" style="text-align: center">
                <h2>
                    30,000 SMS
                </h2>
            </div>
            <div class="body" style="padding: 0;">
                <div class="list-group">
                    <a href="javascript:void(0);" class="list-group-item">Transactional SMS</a>
                    <a href="javascript:void(0);" class="list-group-item">Unlimited Validity</a>
                    <a href="javascript:void(0);" class="list-group-item">Low in Price</a>
                    <a href="javascript:void(0);" class="list-group-item active" style="text-align: center">
                        ₹ 6,000/-
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-3">
        <div class="card">
            <div class="header bg-purple" style="text-align: center">
                <h2>
                    50,000 SMS
                </h2>
            </div>
            <div class="body" style="padding: 0;">
                <div class="list-group">
                    <a href="javascript:void(0);" class="list-group-item">Transactional SMS</a>
                    <a href="javascript:void(0);" class="list-group-item">Unlimited Validity</a>
                    <a href="javascript:void(0);" class="list-group-item">Low in Price</a>
                    <a href="javascript:void(0);" class="list-group-item active" style="text-align: center">
                        ₹ 9,500/-
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-3">
        <div class="card">
            <div class="header bg-brown" style="text-align: center">
                <h2>
                    1,00,000 SMS
                </h2>
            </div>
            <div class="body" style="padding: 0;">
                <div class="list-group">
                    <a href="javascript:void(0);" class="list-group-item">Transactional SMS</a>
                    <a href="javascript:void(0);" class="list-group-item">Unlimited Validity</a>
                    <a href="javascript:void(0);" class="list-group-item">Low in Price</a>
                    <a href="javascript:void(0);" class="list-group-item active" style="text-align: center">
                        Rs 18,500/-
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="logo" style="text-align: center; margin: 20px;padding: 20px">
    <small style="font-size: 18px;color: white">To purchase any package please contact us at email <b>vikasaj9098@gmail.com</b> or mobile <b>8103478849, 9755341315</b></small>
</div>

<!-- Jquery Core Js -->
<script src="/assets/plugins/jquery/jquery.min.js"></script>

<!-- Bootstrap Core Js -->
<script src="/assets/plugins/bootstrap/js/bootstrap.js"></script>

<!-- Waves Effect Plugin Js -->
<script src="/assets/plugins/node-waves/waves.js"></script>

<!-- Validation Plugin Js -->
<script src="/assets/plugins/jquery-validation/jquery.validate.js"></script>

<!-- Custom Js -->
<script src="/assets/js/admin.js"></script>
<script src="/assets/js/pages/examples/sign-in.js"></script>
</body>

</html>
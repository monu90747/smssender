<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <title><?=PRODUCTNAME?></title>
    <!-- Favicon-->
    <link rel="icon" href="/assets/logo-small.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="/assets/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="/assets/plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="/assets/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="/assets/css/style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="/assets/css/themes/all-themes.css" rel="stylesheet" />

    <link href="/assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />

    <!-- Sweetalert Css -->
    <link href="/assets/plugins/sweetalert/sweetalert.css" rel="stylesheet" />

</head>

<body class="theme-red">
<!-- Page Loader -->
<?php //include "include/pageloader.php"; ?>
<!-- #END# Page Loader -->
<!-- Overlay For Sidebars -->
<div class="overlay"></div>
<!-- #END# Overlay For Sidebars -->

<!-- Top Bar -->
<?php include("include/header.php") ?>
<!-- #Top Bar -->
<section>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar" style="width: 220px;">
        <!-- User Info -->
        <div class="user-info" style="height: 80px;">
            <div class="info-container" style="margin-top: -25px;">
                <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><h4><?=$this->user->full_name?></h4></div>
                <div class="email"><?=$this->user->email?></div>
            </div>
        </div>
        <!-- #User Info -->
        <!-- Menu -->
        <div class="menu">
            <ul class="list">
                <li>
                    <a href="#" data-toggle="modal" data-target="#modelAddGroup">
                        <i class="material-icons">group_add</i>
                        <span>ADD GROUP</span>
                    </a>
                </li>
                <hr style="margin: 0;" />
                <li class="<?=($this->currentGroupId == 0)?"active ":""?>slide_menu_group" id="slide_menu_group_0">
                    <a href="/?page=0" data_name="0">
                        <i class="material-icons">contacts</i>
                        <span>Default Group (<?=$this->totalCount ?>)</span>
                    </a>
                </li>

                <?php
                foreach($this->groups as $group){
                    if($group["group_name"] == "Default Group") { continue; }
                    ?>
                    <li class="<?=($this->currentGroupId == $group['id'])?"active ":""?> slide_menu_group" id="slide_menu_group_<?=$group['id']?>">
                        <a href="/?page=<?=$group['id']?>" data_name="<?=$group["id"] ?>">
                            <i class="material-icons">contacts</i>
                            <span><?=$group["group_name"]?> (<?=$group["count"]?>)</span>
                        </a>
                    </li>
                <?php
                }
                ?>

                <hr style="margin: 0;" />
                <li class="slide_menu_group" id="slide_menu_group_send">
                    <a href="/sms/send">
                        <i class="material-icons">send</i>
                        <span>Send SMS</span>
                    </a>
                </li>
                <hr style="margin: 0;" />
                <li class="slide_menu_group" id="slide_menu_group_send">
                    <a href="/user/profile">
                        <i class="material-icons">person</i>
                        <span>Profile</span>
                    </a>
                </li>
                <hr style="margin: 0;" />
                <li class="slide_menu_group" id="slide_menu_group_send">
                    <a href="/transaction/deliveryreport">
                        <i class="material-icons">info</i>
                        <span>Delivery Report</span>
                    </a>
                </li>
                <hr style="margin: 0;" />
                <li class="slide_menu_group" id="slide_menu_group_blacklist">
                    <a href="/transaction/all">
                        <i class="material-icons">account_balance</i>
                        <span>Transaction</span>
                    </a>
                </li>
                <hr style="margin: 0;" />
                <li class="slide_menu_group" id="slide_menu_group_blacklist">
                    <a href="/sms/price">
                        <i class="material-icons">shopping_cart</i>
                        <span>Buy More Credits</span>
                    </a>
                </li>
                <hr style="margin: 0;" />
            </ul>
        </div>
        <!-- #Menu -->
        <!-- Footer -->
        <div class="legal">
            <div class="copyright">
                &copy; 2017 <a href="javascript:void(0);"><?=PRODUCTNAME?></a>
            </div>
            <div class="version">
                All Rights Reserved.
            </div>
        </div>
        <!-- #Footer -->
    </aside>
    <!-- #END# Left Sidebar -->
</section>

<section class="content">
    <div class="container-fluid">
        <div class="block-header">

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px; margin: 0px;">
                    <div class="input-group" style="margin-top: -20px; margin-bottom: 30px;">
                        <div class="form-line">
                            <input type="text" class="form-control" name="search_textfield" id="search_textfield" placeholder="Search by name or mobile number" value="<?=$this->search ?>">
                        </div>
                    </div>
                    <div class="row" id="response_container">

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="modal fade" id="defaultModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="card" style="margin-top: -20px; padding-bottom: 50px;">
                <div class="header" style="padding-bottom: 0">
                    <div class="row" style="padding-bottom: 10px;">
                        <div class="col-lg-8">
                            <h2>SELECT GROUP</h2>
                        </div>
                    </div>
                </div>
                <div class="body table-responsive" style="padding: 5px;">
                    <table class="table table-hover">
                        <tbody>
                        <?php
                        foreach($this->groups as $group){
                            ?>
                            <tr>
                                <td style="padding: 0; text-align: center; padding-top: 10px; width: 60px;">
                                    <input name="group_checkbox" type="checkbox" id="md_group_<?=$group["id"]?>" value="<?=$group["id"]?>" data="<?=$group["group_name"]?>" class="filled-in chk-col-teal" />
                                    <label for="md_group_<?=$group["id"]?>"></label>
                                </td>
                                <td><?=$group["group_name"]?></td>
                            </tr>
                        <?php
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
                <div class="col-lg-12" style="text-align: right">
                    <button type="button" class="btn btn-warning waves-effect" data-dismiss="modal">CLOSE</button>
                    <button type="button" id="clickedSelectGroup" class="btn btn-primary waves-effect" data-dismiss="modal">DONE</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modelAddGroup" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">ADD NEW GROUP</h4>
            </div>
            <hr/>
            <div class="modal-body">
                <div class="row clearfix">
                    <div class="col-lg-3 col-md-2 col-sm-4 col-xs-5 form-control-label">
                        <label for="email_address_2">Group Name *</label>
                    </div>
                    <div class="col-lg-9 col-md-10 col-sm-8 col-xs-7">
                        <div class="form-group">
                            <div class="form-line">
                                <input type="text" id="group_name" class="form-control" placeholder="Enter group name">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr/>
            <div class="modal-footer">
                <div class="col-lg-8">
                    <div id="model_message_group" style="color:red; text-align: center"></div>
                </div>
                <div class="col-lg-4">
                    <button type="button" id="addGroup" class="btn btn-primary waves-effect">SAVE</button>
                    <button type="button" class="btn btn-warning waves-effect" data-dismiss="modal">CLOSE</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Jquery Core Js -->
<script src="/assets/plugins/jquery/jquery.min.js"></script>

<!-- Bootstrap Core Js -->
<script src="/assets/plugins/bootstrap/js/bootstrap.js"></script>

<!-- Select Plugin Js -->
<script src="/assets/plugins/bootstrap-select/js/bootstrap-select.js"></script>

<!-- Slimscroll Plugin Js -->
<script src="/assets/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

<!-- Waves Effect Plugin Js -->
<script src="/assets/plugins/node-waves/waves.js"></script>

<!-- Custom Js -->
<script src="/assets/js/admin.js"></script>

<!-- Demo Js -->
<script src="/assets/js/demo.js"></script>

<!-- Multi Select Plugin Js -->
<script src="/assets/plugins/bootstrap-select/js/bootstrap-select.js"></script>

<!-- Bootstrap Notify Plugin Js -->
<script src="/assets/plugins/bootstrap-notify/bootstrap-notify.js"></script>

<script src="/assets/js/pages/ui/notifications.js"></script>

<script src="/assets/js/pages/ui/tooltips-popovers.js"></script>

<script src="/assets/js/home.js"></script>

<script>

    $selectedGroupId = 0;

    function loadPaging(index){
        window.location = "/?page=<?=$this->currentGroupId ?>&index="+index+"&search="+$("#search_textfield").val();
    }

    function openGroupContacts(val){
        var groupId = $(val).attr("data_name");
        $selectedGroupId = groupId;
        $(".slide_menu_group").attr("class", "slide_menu_group");
        $("#slide_menu_group_"+groupId).attr("class", "active slide_menu_group");

        loadViewContent("/page/groupcontact",{ groupId: groupId }, "#response_container");
    }

    function loadViewContent(page, json, container){
        startLoading(container);
        $.post( page, json)
            .done(function( data ) {
                $(container).html(data);
            });
    }

    function showSuccessMessageNotification(message){
        showNotification("bg-green", message, "top", "right", "animated bounceInUp", "animated bounceOutUp");
    }

    function showErrorMessageNotification(message){
        showNotification("bg-orange", message, "top", "right", "animated bounceInUp", "animated bounceOutUp");
    }

    loadViewContent("/page/groupcontact",{ groupId: "<?=$this->currentGroupId ?>", index:"<?=$this->index ?>", search:"<?=$this->search ?>" }, "#response_container");

    $("#search_textfield").keyup(function(){
        var text = $("#search_textfield").val();
        if(text.length >= 3){
            loadViewContent("/page/groupcontact",{ groupId: "<?=$this->currentGroupId ?>", index:0, search:text }, "#response_container");
        }
        else if(text.length <= 0){
            loadViewContent("/page/groupcontact",{ groupId: "<?=$this->currentGroupId ?>", index:0, search:text }, "#response_container");
        }
    });

</script>

</body>

</html>
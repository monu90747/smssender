<div class="col-lg-12 js-sweetalert" style="padding-right: 5px;">
    <div style="margin-top: -20px;margin-bottom: 10px;margin-left: 5px;">
        <button type="button" data-toggle="modal" data-target="#modelAddContact" class="btn btn-primary waves-effect">ADD NEW CONTACT</button>
        <span id="contact_menu_change"></span>
        <?php
        if($this->currentGroupId > 0){
            echo '<button type="button" class="btn btn-danger waves-effect" onclick="confirmDeleteGroup();">DELETE GROUP</button>';
        }
        ?>
    </div>
    <div class="card" id="contact_list">
        <div class="body table-responsive" style="padding: 5px;">
            <?php
            if(count($this->contacts) <= 0){
                echo '<div class="header" style="text-align: center; padding: 50px;">';
                echo '<i class="material-icons" style="font-size: 90px;">contact_phone</i>';
                echo'<h1>No Contact!</h1>';
                echo'<h2><a href="javascript:void(0);" data-toggle="modal" data-target="#modelAddContact">Click here to add new contact</a></h2></div>';
            }
            else{
                ?>
                <table class="table table-hover">
                <thead>
                <tr>
                    <th style="padding: 0; text-align: center; padding-top: 10px;">
                    </th>
                    <th>MOBILE</th>
                    <th>NAME</th>
                    <th>REFERENCE</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach($this->contacts as $contact){
                    ?>
                    <tr>
                        <td style="padding: 0; text-align: center; padding-top: 10px;">
                            <input name="contactNumberSelect" type="checkbox" id="md_checkbox_<?=$contact["id"]?>" class="filled-in chk-col-teal contactNumberSelect" data="<?=$contact["mobile"]?>" data_id="<?=$contact["id"]?>"/>
                            <label for="md_checkbox_<?=$contact["id"]?>"></label>
                        </td>
                        <td><?=$contact["mobile"]?></td>
                        <td><?=$contact["name"]?></td>
                        <td><?=$contact["reference"]?></td>
                    </tr>
                <?php
                }
                ?>
                </tbody>
            </table>
                <nav>
                    <ul class="pagination">
                        <?php
                        if($this->current_page == 1){
                            echo '<li class="disabled"><a href="javascript:void(0);"><i class="material-icons">chevron_left</i></a></li>';
                        }
                        else{
                            echo '<li><a href="javascript:void(0);" onclick="loadPaging('.($this->current_page-1).');"><i class="material-icons">chevron_left</i></a></li>';
                        }

                        if($this->total_page <= 6){
                            for($page=1; $page<=$this->total_page; $page++){
                                if($page == $this->current_page){
                                    echo '<li class="active"><a href="javascript:void(0);">'.$page.'</a></li>';
                                }
                                else{
                                    echo '<li><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging('.$page.');">'.$page.'</a></li>';
                                }
                            }
                        }
                        else{
                            if($this->current_page == 1){
                                echo '<li class="active"><a href="javascript:void(0);">1</a></li>';
                                echo '<li><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging(2);">2</a></li>';
                                echo '<li><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging(3);">3</a></li>';
                                echo '<li><a href="javascript:void(0);" class="waves-effect">...</a></li>';
                                echo '<li><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging('.$this->total_page.');">'.$this->total_page.'</a></li>';
                            }
                            elseif($this->current_page == $this->total_page){
                                echo '<li><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging(1);">1</a></li>';
                                echo '<li><a href="javascript:void(0);" class="waves-effect">...</a></li>';
                                echo '<li><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging('.($this->total_page-2).');">'.($this->total_page-2).'</a></li>';
                                echo '<li><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging('.($this->total_page-1).');">'.($this->total_page-1).'</a></li>';
                                echo '<li class="active"><a href="javascript:void(0);">'.$this->total_page.'</a></li>';
                            }
                            elseif($this->current_page == 2){
                                echo '<li><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging(1);">1</a></li>';
                                echo '<li class="active"><a href="javascript:void(0);">2</a></li>';
                                echo '<li><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging(3);">3</a></li>';
                                echo '<li><a href="javascript:void(0);" class="waves-effect">...</a></li>';
                                echo '<li><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging('.$this->total_page.');">'.$this->total_page.'</a></li>';
                            }
                            elseif($this->current_page == $this->total_page-1){
                                echo '<li><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging(1);">1</a></li>';
                                echo '<li><a href="javascript:void(0);" class="waves-effect">...</a></li>';
                                echo '<li><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging('.($this->total_page-2).');">'.($this->total_page-2).'</a></li>';
                                echo '<li class="active"><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging('.($this->total_page-1).');">'.($this->total_page-1).'</a></li>';
                                echo '<li><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging('.($this->total_page).');">'.($this->total_page).'</a></li>';
                            }
                            else{
                                echo '<li><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging(1);">1</a></li>';
                                if($this->current_page-2 != 1){
                                    echo '<li><a href="javascript:void(0);" class="waves-effect">...</a></li>';
                                }

                                echo '<li><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging('.($this->current_page-1).');">'.($this->current_page-1).'</a></li>';
                                echo '<li class="active"><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging('.($this->current_page).');">'.($this->current_page).'</a></li>';
                                echo '<li><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging('.($this->current_page+1).');">'.($this->current_page+1).'</a></li>';

                                if($this->current_page+2 != $this->total_page){
                                    echo '<li><a href="javascript:void(0);" class="waves-effect">...</a></li>';
                                }
                                echo '<li><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging('.($this->total_page).');">'.($this->total_page).'</a></li>';
                            }
                        }

                        if($this->current_page == $this->total_page){
                            echo '<li class="disabled"><a href="javascript:void(0);"><i class="material-icons">chevron_right</i></a></li>';
                        }
                        else{
                            echo '<li><a href="javascript:void(0);" onclick="loadPaging('.($this->current_page+1).');"><i class="material-icons">chevron_right</i></a></li>';
                        }
                        ?>
                    </ul>
                </nav>
                <?php
            }
            ?>
        </div>
    </div>
</div>

<div class="modal fade" id="modelAddContact" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="card">
                <div class="body" style="padding-bottom: 0px;padding-top: 0;">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#home_with_icon_title" data-toggle="tab">
                                <i class="material-icons">add_circle</i> ADD CONTACT
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#profile_with_icon_title" data-toggle="tab">
                                <i class="material-icons">import_export</i> IMPORT CONTACT
                            </a>
                        </li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="home_with_icon_title">
                            <br>
                            <form class="form-horizontal" method="post" action="/contact/add">
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="email_address_2">Full Name</label>
                                    </div>
                                    <div class="col-lg-9 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="name" class="form-control" placeholder="Enter full name">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="email_address_2">Mobile Number</label>
                                    </div>
                                    <div class="col-lg-9 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="number" id="mobile" maxlength="10" minlength="10" class="form-control" placeholder="Enter mobile number">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="email_address_2">Reference #</label>
                                    </div>
                                    <div class="col-lg-9 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="reference" class="form-control" placeholder="Enter reference number">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <hr/>
                                <div class="modal-footer" style="padding-top: 0px; padding-bottom: 10px;">
                                    <div class="col-lg-8">
                                        <div id="error_message" style="color:red; text-align: center"></div>
                                    </div>
                                    <div class="col-lg-4">
                                        <button type="button" id="saveContact" class="btn btn-primary waves-effect">SAVE</button>
                                        <button type="button" class="btn btn-warning waves-effect" data-dismiss="modal">CLOSE</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="profile_with_icon_title">
                            <div class="body">
                                <form action="/contact/import" id="frmFileUpload" method="POST" enctype="multipart/form-data">
                                    <div class="dz-message">
                                        <h3>Import Contacts</h3>
                                    </div>
                                    <input type="hidden" name="groupId" value="<?=$this->currentGroupId ?>">
                                    <div class="fallback">
                                        <input name="fileToUpload" type="file" id="fileToUpload" />
                                    </div>
                                    <input type="hidden" name="groupId" value="<?=$this->currentGroupId ?>">
                                    <div class="modal-footer" style="padding-top: 20px; padding-bottom: 0;">
                                        <div class="col-lg-4">
                                            <div id="error_message" style="color:red; text-align: center"></div>
                                        </div>
                                        <div class="col-lg-8" style="margin-bottom: 0;">
                                            <button type="submit" id="uploadContactList" class="btn btn-primary waves-effect">UPLOAD</button>
                                            <button type="button" class="btn btn-warning waves-effect" data-dismiss="modal">CLOSE</button>
                                        </div>
                                    </div>
                                    <a href="/contact/DownloadSampleContactFile">Click here to download sample</a>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- SweetAlert Plugin Js -->
<script src="/assets/plugins/sweetalert/sweetalert.min.js"></script>
<script src="/assets/js/pages/ui/dialogs.js"></script>

<script>

    var selectedMobileNumbers = [];

    $("input:checkbox.contactNumberSelect").click(function() {
        if($(this).is(":checked")){
            selectedMobileNumbers.push($(this).attr("data_id"));
        }
        else {
            removeNumberItem(selectedMobileNumbers, $(this).attr("data_id"));
        }

        if(selectedMobileNumbers.length > 0){
            var buttons = "";
            $("#contact_menu_change").html('<button type="button" class="btn btn-danger waves-effect" onclick="confirmDeleteContact()">DELETE CONTACT</button>');
        }
        else{
            $("#contact_menu_change").html("");
        }

    });

    function removeNumberItem(array, item){
        for(var i in array){
            if(array[i]==item){
                array.splice(i,1);
                break;
            }
        }
    }

    $("#saveContact").click(function(){
        var name = $("#name").val();
        var mobile = $("#mobile").val();
        var groups = ["<?=$this->currentGroupId ?>"];
        var reference = $("#reference").val();

        var json = {};
        if(groups){
            json = { name: name, mobile:mobile, groups:groups, reference:reference };
        }
        else{
            json = { name: name, mobile:mobile, reference:reference };
        }

        if(mobile.length != 10){
            $("#error_message").html("Please enter a valid mobile number");
        }
        else{
            $("#saveContact").attr("disabled", true);
            $.post( "/contact/add", json)
                .done(function( data ) {
                    $("#saveContact").attr("disabled", false);
                    console.log(data);
                    var response = JSON.parse(data);
                    if(response.status == 1){
                        window.location = "/?page=<?=$this->currentGroupId ?>";
                    }
                    else{
                        $("#error_message").html(response.message);
                    }
                });
        }
    });

    function confirmDeleteGroup() {
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this group",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        }, function () {
            var group_id = "<?=$this->currentGroupId ?>";
            if(group_id > 0){
                swal({
                    title: "Please wait...",
                    text: '<div class="preloader pl-size-xl">' +
                    '<div class="spinner-layer">' +
                    '<div class="circle-clipper left">' +
                    '<div class="circle"></div>' +
                    '</div>' +
                    '<div class="circle-clipper right">' +
                    '<div class="circle"></div>' +
                    '</div>' +
                    '</div>' +
                    '</div>',
                    html: true,
                    showConfirmButton: false
                });
                $.post( "/group/delete", {group_id:group_id})
                    .done(function( data ) {
                        var response = JSON.parse(data);
                        if(response.status == 1){
                            swal({
                                title: "Deleted!",
                                text: "Group has been deleted.",
                                type: "success",
                                showCancelButton: false,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "OK",
                                closeOnConfirm: true
                            }, function () {
                                window.location = "/";
                            });
                        }
                        else{
                            swal("Failed!", "Something went wrong.", "error");
                        }
                    });
            }
        });
    }

    function confirmDeleteContact(){
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this contact",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        }, function () {
            console.log(selectedMobileNumbers);
            if(selectedMobileNumbers.length > 0){
                swal({
                    title: "Please wait...",
                    text: '<div class="preloader pl-size-xl">' +
                    '<div class="spinner-layer">' +
                    '<div class="circle-clipper left">' +
                    '<div class="circle"></div>' +
                    '</div>' +
                    '<div class="circle-clipper right">' +
                    '<div class="circle"></div>' +
                    '</div>' +
                    '</div>' +
                    '</div>',
                    html: true,
                    showConfirmButton: false
                });

                var group_id = "<?=$this->currentGroupId ?>";

                $.post( "/contact/delete", {contact_ids:selectedMobileNumbers, group_id:group_id})
                    .done(function( data ) {
                        console.log(data);
                        var response = JSON.parse(data);
                        if(response.status == 1){
                            swal({
                                title: "Deleted!",
                                text: "Contact has been deleted.",
                                type: "success",
                                showCancelButton: false,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "OK",
                                closeOnConfirm: true
                            }, function () {
                                window.location = "/";
                            });
                        }
                        else{
                            swal("Failed!", "Something went wrong.", "error");
                        }
                    });
            }
        });
    }

</script>
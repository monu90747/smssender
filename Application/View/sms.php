<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <title><?=PRODUCTNAME?></title>
    <!-- Favicon-->
    <link rel="icon" href="/assets/logo-small.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="/assets/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="/assets/plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="/assets/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="/assets/css/style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="/assets/css/themes/all-themes.css" rel="stylesheet" />

    <link href="/assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />

    <!-- Sweetalert Css -->
    <link href="/assets/plugins/sweetalert/sweetalert.css" rel="stylesheet" />

    <!-- Bootstrap Material Datetime Picker Css -->
    <link href="/assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />


    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">

        // Load the Google Transliterate API
        google.load("elements", "1", {
            packages: "transliteration"
        });

        var control = null;

        function onLoad() {
            var unicode = $("#unicode").val();
            console.log("Language:"+unicode);
            var options = [];
            if(unicode == 0){
                options = {
                    sourceLanguage:
                        google.elements.transliteration.LanguageCode.ENGLISH,
                    destinationLanguage:
                        [google.elements.transliteration.LanguageCode.HINDI],
                    shortcutKey: 'ctrl+g',
                    transliterationEnabled: false
                };
            }
            else{
                options = {
                    sourceLanguage:
                        google.elements.transliteration.LanguageCode.ENGLISH,
                    destinationLanguage:
                        [google.elements.transliteration.LanguageCode.HINDI],
                    shortcutKey: 'ctrl+g',
                    transliterationEnabled: true
                };
            }

            if(control != null){
                control = null;
            }
            control = new google.elements.transliteration.TransliterationControl(options);
            control.makeTransliteratable(['message_box']);
        }
        google.setOnLoadCallback(onLoad);
    </script>

</head>

<body class="theme-red">
<!-- Page Loader -->
<?php //include "include/pageloader.php"; ?>
<!-- #END# Page Loader -->
<!-- Overlay For Sidebars -->
<div class="overlay"></div>
<!-- #END# Overlay For Sidebars -->

<!-- Top Bar -->
<?php include("include/header.php") ?>
<!-- #Top Bar -->
<section>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar" style="width: 220px;">
        <!-- User Info -->
        <div class="user-info" style="height: 80px;">
            <div class="info-container" style="margin-top: -25px;">
                <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><h4><?=$this->user->full_name?></h4></div>
                <div class="email"><?=$this->user->email?></div>
            </div>
        </div>
        <!-- #User Info -->
        <!-- Menu -->
        <div class="menu">
            <ul class="list">
                <li>
                    <a href="#" data-toggle="modal" data-target="#modelAddGroup">
                        <i class="material-icons">group_add</i>
                        <span>ADD GROUP</span>
                    </a>
                </li>
                <hr style="margin: 0;" />
                <li id="slide_menu_group_0">
                    <a href="/?page=0" data_name="0">
                        <i class="material-icons">contacts</i>
                        <span>Default Group (<?=$this->totalCount ?>)</span>
                    </a>
                </li>

                <?php
                foreach($this->groups as $group){
                    if($group["group_name"] == "Default Group") { continue; }
                    ?>
                    <li class="<?=($this->currentGroupId == $group['id'])?"active ":""?> slide_menu_group" id="slide_menu_group_<?=$group['id']?>">
                        <a href="/?page=<?=$group['id']?>" data_name="<?=$group["id"] ?>">
                            <i class="material-icons">contacts</i>
                            <span><?=$group["group_name"]?> (<?=$group["count"]?>)</span>
                        </a>
                    </li>
                <?php
                }
                ?>

                <hr style="margin: 0;" />
                <li class="active slide_menu_group" id="slide_menu_group_send">
                    <a href="/sms/send">
                        <i class="material-icons">send</i>
                        <span>Send SMS</span>
                    </a>
                </li>
                <hr style="margin: 0;" />
                <li class="slide_menu_group" id="slide_menu_group_send">
                    <a href="/user/profile">
                        <i class="material-icons">person</i>
                        <span>Profile</span>
                    </a>
                </li>
                <hr style="margin: 0;" />
                <li class="slide_menu_group" id="slide_menu_group_send">
                    <a href="/transaction/deliveryreport">
                        <i class="material-icons">info</i>
                        <span>Delivery Report</span>
                    </a>
                </li>
                <hr style="margin: 0;" />
                <li class="slide_menu_group" id="slide_menu_group_blacklist">
                    <a href="/transaction/all">
                        <i class="material-icons">account_balance</i>
                        <span>Transaction</span>
                    </a>
                </li>
                <hr style="margin: 0;" />
                <li class="slide_menu_group" id="slide_menu_group_blacklist">
                    <a href="/sms/price">
                        <i class="material-icons">shopping_cart</i>
                        <span>Buy More Credits</span>
                    </a>
                </li>
                <hr style="margin: 0;" />
            </ul>
        </div>
        <!-- #Menu -->
        <!-- Footer -->
        <div class="legal">
            <div class="copyright">
                &copy; 2017 <a href="javascript:void(0);"><?=PRODUCTNAME?></a>
            </div>
            <div class="version">
                All Rights Reserved.
            </div>
        </div>
        <!-- #Footer -->
    </aside>
    <!-- #END# Left Sidebar -->
</section>

<section class="content">
    <div class="container-fluid">
        <div class="block-header">

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px; margin: 0px;">
                    <div class="row" id="response_container">
                        <div class="col-lg-6" style="padding-right: 5px;">
                            <div class="info-box bg-teal hover-expand-effect" style="margin-top: -20px;">
                                <div class="icon">
                                    <i class="material-icons">message</i>
                                </div>
                                <div class="content">
                                    <div class="text">SMS CREDIT</div>
                                    <div id="sms_credit_balance" class="number" style="margin-top: 0;"><?=$this->user->balance->transactional ?></div>
                                </div>
                            </div>

                            <div class="card" style="margin-top: -20px;">
                                <div class="header">
                                    <div class="col-lg-6" style="margin-top: -6px">
                                        <h2>SEND SMS</h2>
                                    </div>
                                </div>
                                <div class="body" style="padding: 5px;">
                                    <form class="form-horizontal" method="post">

                                        <div class="row clearfix" style="margin-left: 0; margin-top: 10px;">
                                            <div class="col-lg-12" style="padding-right: 25px;">
                                                <select id="sender_id" class="form-control show-tick" title="Select Sender">
                                                    <option value="">Select Sender</option>
                                                    <?php
                                                    foreach($this->senders as $sender){
                                                        echo '<option value="'.$sender["sender_id"].'">'.$sender["sender_id"].'</option>';
                                                    }

                                                    ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="row clearfix" style="margin-left: 10px;">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 7px">
                                                <label id="selected_groups"></label>
                                            </div>
                                        </div>

                                        <div class="row clearfix" style="margin-left: 20px;">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <textarea rows="2" id="mobile_numbers" class="form-control no-resize" style="border: solid;border-width: 1px;padding: 5px;border-color: lightgray;" placeholder="Enter mobile numbers. eg. XXXXXXXXXX, XXXXXXXXXX"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row clearfix" style="margin-left: 20px;">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-lg-3" style="margin-bottom: 0;padding-bottom: 0;">
                                                            <select name="unicode" id="unicode" class="form-control show-tick">
                                                                <option value="0">English</option>
                                                                <option value="1">Unicode</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-lg-9" style="text-align: right; padding-top: 10px; padding-right: 25px; margin-bottom: 0;padding-bottom: 0;">
                                                            <div id="message_counter"><label style='color: green'>(0/0)</label></div>
                                                        </div>
                                                    </div>
                                                    <div class="form-line">
                                                        <textarea id="message_box" rows="5" class="form-control no-resize" style="border: solid;border-width: 1px;padding: 5px;border-color: lightgray;" placeholder="Enter message text"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row clearfix" style="margin-left: 20px; margin-right: 0;">
                                            <div class="col-sm-12" style="text-align: right">
                                                <button type="button" class="btn bg-orange waves-effect" id="scheduleSMSNow">SCHEDULE</button>&nbsp;&nbsp;
                                                <button type="button" class="btn bg-pink waves-effect" id="sendSMSNow">SEND NOW</button>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="margin-top: -20px;">
                            <div class="card">
                                <div class="body">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs" role="tablist" style="margin-top: -20px">
                                        <li role="presentation" class="active">
                                            <a href="#home_with_icon_title" data-toggle="tab">
                                                <i class="material-icons">group</i> GROUPS
                                            </a>
                                        </li>
                                        <li role="presentation">
                                            <a href="#profile_with_icon_title" data-toggle="tab">
                                                <i class="material-icons">contact_phone</i> CONTACTS
                                            </a>
                                        </li>
                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane fade in active" id="home_with_icon_title">
                                            <div class="body table-responsive" style="padding: 5px;">
                                                <table class="table table-hover">
                                                    <tbody>
                                                    <?php
                                                    foreach($this->groups as $group){
                                                        ?>
                                                        <tr>
                                                            <td style="padding: 0; text-align: center; padding-top: 10px; width: 60px;">
                                                                <input name="contactGroupCheck" type="checkbox" id="md_group_<?=$group["id"]?>" value="<?=$group["id"]?>" data="<?=$group["group_name"]?>" class="filled-in chk-col-teal contactGroupCheck" />
                                                                <label for="md_group_<?=$group["id"]?>"></label>
                                                            </td>
                                                            <td><?=$group["group_name"]?> (<?=$group["count"]?>)</td>
                                                        </tr>
                                                    <?php
                                                    }
                                                    ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade" id="profile_with_icon_title">
                                            <div class="input-group">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" name="search_textfield" id="search_textfield" placeholder="Search by name or mobile number" value="">
                                                </div>
                                            </div>
                                            <div id="contact_list">
                                                <table class="table table-hover">
                                                    <thead>
                                                    <tr>
                                                        <th style="padding: 0; text-align: center; padding-top: 10px;">
                                                        </th>
                                                        <th>MOBILE</th>
                                                        <th>NAME</th>
                                                        <th>REFERENCE</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php
                                                    foreach($this->contacts as $contact){
                                                        ?>
                                                        <tr>
                                                            <td style="padding: 0; text-align: center; padding-top: 10px;">
                                                                <input name="contactNumberCheck" type="checkbox" id="md_checkbox_<?=$contact["id"]?>" class="filled-in chk-col-teal contactNumberCheck" data="<?=$contact["mobile"]?>"/>
                                                                <label for="md_checkbox_<?=$contact["id"]?>"></label>
                                                            </td>
                                                            <td><?=$contact["mobile"]?></td>
                                                            <td><?=$contact["name"]?></td>
                                                            <td><?=$contact["reference"]?></td>
                                                        </tr>
                                                        <?php
                                                    }
                                                    ?>
                                                    </tbody>
                                                </table>
                                                <nav>
                                                    <ul class="pagination">
                                                        <?php
                                                        if($this->current_page == 1){
                                                            echo '<li class="disabled"><a href="javascript:void(0);"><i class="material-icons">chevron_left</i></a></li>';
                                                        }
                                                        else{
                                                            echo '<li><a href="javascript:void(0);" onclick="loadPaging('.($this->current_page-1).');"><i class="material-icons">chevron_left</i></a></li>';
                                                        }

                                                        if($this->total_page <= 6){
                                                            for($page=1; $page<=$this->total_page; $page++){
                                                                if($page == $this->current_page){
                                                                    echo '<li class="active"><a href="javascript:void(0);">'.$page.'</a></li>';
                                                                }
                                                                else{
                                                                    echo '<li><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging('.$page.');">'.$page.'</a></li>';
                                                                }
                                                            }
                                                        }
                                                        else{
                                                            if($this->current_page == 1){
                                                                echo '<li class="active"><a href="javascript:void(0);">1</a></li>';
                                                                echo '<li><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging(2);">2</a></li>';
                                                                echo '<li><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging(3);">3</a></li>';
                                                                echo '<li><a href="javascript:void(0);" class="waves-effect">...</a></li>';
                                                                echo '<li><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging('.$this->total_page.');">'.$this->total_page.'</a></li>';
                                                            }
                                                            elseif($this->current_page == $this->total_page){
                                                                echo '<li><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging(1);">1</a></li>';
                                                                echo '<li><a href="javascript:void(0);" class="waves-effect">...</a></li>';
                                                                echo '<li><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging('.($this->total_page-2).');">'.($this->total_page-2).'</a></li>';
                                                                echo '<li><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging('.($this->total_page-1).');">'.($this->total_page-1).'</a></li>';
                                                                echo '<li class="active"><a href="javascript:void(0);">'.$this->total_page.'</a></li>';
                                                            }
                                                            elseif($this->current_page == 2){
                                                                echo '<li><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging(1);">1</a></li>';
                                                                echo '<li class="active"><a href="javascript:void(0);">2</a></li>';
                                                                echo '<li><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging(3);">3</a></li>';
                                                                echo '<li><a href="javascript:void(0);" class="waves-effect">...</a></li>';
                                                                echo '<li><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging('.$this->total_page.');">'.$this->total_page.'</a></li>';
                                                            }
                                                            elseif($this->current_page == $this->total_page-1){
                                                                echo '<li><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging(1);">1</a></li>';
                                                                echo '<li><a href="javascript:void(0);" class="waves-effect">...</a></li>';
                                                                echo '<li><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging('.($this->total_page-2).');">'.($this->total_page-2).'</a></li>';
                                                                echo '<li class="active"><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging('.($this->total_page-1).');">'.($this->total_page-1).'</a></li>';
                                                                echo '<li><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging('.($this->total_page).');">'.($this->total_page).'</a></li>';
                                                            }
                                                            else{
                                                                echo '<li><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging(1);">1</a></li>';

                                                                if($this->current_page-2 != 1){
                                                                    echo '<li><a href="javascript:void(0);" class="waves-effect">...</a></li>';
                                                                }

                                                                echo '<li><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging('.($this->current_page-1).');">'.($this->current_page-1).'</a></li>';
                                                                echo '<li class="active"><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging('.($this->current_page).');">'.($this->current_page).'</a></li>';
                                                                echo '<li><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging('.($this->current_page+1).');">'.($this->current_page+1).'</a></li>';

                                                                if($this->current_page+2 != $this->total_page){
                                                                    echo '<li><a href="javascript:void(0);" class="waves-effect">...</a></li>';
                                                                }

                                                                echo '<li><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging('.($this->total_page).');">'.($this->total_page).'</a></li>';
                                                            }
                                                        }

                                                        if($this->current_page == $this->total_page){
                                                            echo '<li class="disabled"><a href="javascript:void(0);"><i class="material-icons">chevron_right</i></a></li>';
                                                        }
                                                        else{
                                                            echo '<li><a href="javascript:void(0);" onclick="loadPaging('.($this->current_page+1).');"><i class="material-icons">chevron_right</i></a></li>';
                                                        }
                                                        ?>
                                                    </ul>
                                                </nav>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="modal fade" id="defaultModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="card" style="margin-top: -20px; padding-bottom: 50px;">
                <div class="header" style="padding-bottom: 0">
                    <div class="row" style="padding-bottom: 10px;">
                        <div class="col-lg-8">
                            <h2>SELECT GROUP</h2>
                        </div>
                    </div>
                </div>
                <div class="body table-responsive" style="padding: 5px;">
                    <table class="table table-hover">
                        <tbody>
                        <?php
                        foreach($this->groups as $group){
                            ?>
                            <tr>
                                <td style="padding: 0; text-align: center; padding-top: 10px; width: 60px;">
                                    <input name="group_checkbox" type="checkbox" id="md_group_<?=$group["id"]?>" value="<?=$group["id"]?>" data="<?=$group["group_name"]?>" class="filled-in chk-col-teal" />
                                    <label for="md_group_<?=$group["id"]?>"></label>
                                </td>
                                <td><?=$group["group_name"]?></td>
                            </tr>
                        <?php
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
                <div class="col-lg-12" style="text-align: right">
                    <button type="button" class="btn btn-warning waves-effect" data-dismiss="modal">CLOSE</button>
                    <button type="button" id="clickedSelectGroup" class="btn btn-primary waves-effect" data-dismiss="modal">DONE</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modelAddGroup" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">ADD NEW GROUP</h4>
            </div>
            <hr/>
            <div class="modal-body">
                <div class="row clearfix">
                    <div class="col-lg-3 col-md-2 col-sm-4 col-xs-5 form-control-label">
                        <label for="email_address_2">Group Name *</label>
                    </div>
                    <div class="col-lg-9 col-md-10 col-sm-8 col-xs-7">
                        <div class="form-group">
                            <div class="form-line">
                                <input type="text" id="group_name" class="form-control" placeholder="Enter group name">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr/>
            <div class="modal-footer">
                <div class="col-lg-8">
                    <div id="model_message_group" style="color:red; text-align: center"></div>
                </div>
                <div class="col-lg-4">
                    <button type="button" id="addGroup" class="btn btn-primary waves-effect">SAVE</button>
                    <button type="button" class="btn btn-warning waves-effect" data-dismiss="modal">CLOSE</button>
                </div>
            </div>
        </div>
    </div>
</div>

<a href="#" data-toggle="modal" data-target="#modelScheduleSMS" id="scheduleSMSNowButton"></a>
<div class="modal fade" id="modelScheduleSMS" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">Schedule SMS</h4>
            </div>
            <hr/>
            <div class="modal-body">
                <div class="row clearfix">
                    <div class="col-lg-3 col-md-2 col-sm-4 col-xs-5 form-control-label">
                        <label for="date">Date:</label>
                    </div>
                    <div class="col-lg-9 col-md-10 col-sm-8 col-xs-7">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" id="scheduleDate" class="datepicker form-control" placeholder="Please choose a date...">
                                </div>
                            </div>
                    </div>  
                </div>
                <div class="row clearfix">
                    <div class="col-lg-3 col-md-2 col-sm-4 col-xs-5 form-control-label">
                        <label for="time">Time:</label>
                    </div>
                    <div class="col-lg-9">
                        <div class="row clearfix">
                            <div class="col-md-3">
                                <div class="input-group">
                                <select id="scheduleHours">
                                    <option value="">Hours</option>
                                    <?php 
                                        for($i=0; $i<24; $i++){
                                            $value = $i;
                                            if($value < 10) {
                                                $value = "0".$value;
                                            }
                                            echo "<option value='$value'>$value</option>";
                                        }
                                    ?>
                                </select>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="input-group">
                                    <select id="scheduleMinuts">
                                        <option value="">Minuts</option>
                                        <option value="00">00</option>
                                        <option value="15">15</option>
                                        <option value="30">30</option>
                                        <option value="45">45</option>
                                    </select>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            <hr/>
            <div class="modal-footer">
                <div class="col-lg-6">
                    <div id="model_message_schedule" style="color:red; text-align: center"></div>
                </div>
                <div class="col-lg-6">
                    <button type="button" id="scheduleSMSSendNow" class="btn btn-primary waves-effect">SCHEDULE SMS</button>
                    <button type="button" id="scheduleSMSClose" class="btn btn-warning waves-effect" data-dismiss="modal">CLOSE</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Jquery Core Js -->
<script src="/assets/plugins/jquery/jquery.min.js"></script>

<!-- Bootstrap Core Js -->
<script src="/assets/plugins/bootstrap/js/bootstrap.js"></script>

<!-- Select Plugin Js -->
<script src="/assets/plugins/bootstrap-select/js/bootstrap-select.js"></script>

<!-- Slimscroll Plugin Js -->
<script src="/assets/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

<!-- Waves Effect Plugin Js -->
<script src="/assets/plugins/node-waves/waves.js"></script>

<!-- Custom Js -->
<script src="/assets/js/admin.js"></script>

<!-- Demo Js -->
<script src="/assets/js/demo.js"></script>

<!-- Multi Select Plugin Js -->
<script src="/assets/plugins/bootstrap-select/js/bootstrap-select.js"></script>

<!-- Bootstrap Notify Plugin Js -->
<script src="/assets/plugins/bootstrap-notify/bootstrap-notify.js"></script>

<script src="/assets/js/pages/ui/notifications.js"></script>

<script src="/assets/js/pages/ui/tooltips-popovers.js"></script>

<script src="/assets/js/home.js"></script>

<script src="/assets/js/pages/ui/dialogs.js"></script>

<!-- Bootstrap Material Datetime Picker Plugin Js -->
<script src="/assets/plugins/autosize/autosize.js"></script>
<script src="/assets/plugins/momentjs/moment.js"></script>
<script src="/assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>


<script>

$('.datepicker').bootstrapMaterialDatePicker({
    format: 'dddd, DD MMMM YYYY',
    clearButton: false,
    weekStart: 1,
    time: false,
    minDate: new Date('<?=date("Y-m-d") ?>')
});



    $selectedGroupId = 0;

    function openGroupContacts(val){
        var groupId = $(val).attr("data_name");
        $selectedGroupId = groupId;
        $(".slide_menu_group").attr("class", "slide_menu_group");
        $("#slide_menu_group_"+groupId).attr("class", "active slide_menu_group");

        loadViewContent("/page/groupcontact",{ groupId: groupId }, "#response_container");
    }

    function openMenu(val){
        if(val == "send"){
            $(".slide_menu_group").attr("class", "slide_menu_group");
            $("#slide_menu_group_send").attr("class", "active slide_menu_group");
            loadViewContent("/page/sendsms",{}, "#response_container");
        }
        else if(val == "blacklist"){
            $(".slide_menu_group").attr("class", "slide_menu_group");
            $("#slide_menu_group_blacklist").attr("class", "active slide_menu_group");
        }
        else if(val == "report"){
            $(".slide_menu_group").attr("class", "slide_menu_group");
            loadViewContent("/notify/test",{}, "#response_container");
        }
    }

    function loadViewContent(page, json, container){
        startLoading(container);
        $.post( page, json)
            .done(function( data ) {
                $(container).html(data);
            });
    }

    function showSuccessMessageNotification(message){
        showNotification("bg-green", message, "top", "left", "animated bounceInUp", "animated bounceOutUp");
    }

    function showErrorMessageNotification(message){
        showNotification("bg-orange", message, "top", "left", "animated bounceInUp", "animated bounceOutUp");
    }

    //    loadViewContent("/page/groupcontact",{ groupId: "<?//=$this->currentGroupId ?>//" }, "#response_container");

</script>

<script>

    $("#search_textfield").keyup(function(){
        var text = $("#search_textfield").val();
        if(text.length >= 3){
            loadPaging(0)
        }
        else if(text.length <= 0){
            loadPaging(0)
        }
    });

    function loadPaging(index){
        loadPagingContact("/page/PagingContact",{index:index, search:$("#search_textfield").val()}, "#contact_list");
    }

    function loadPagingContact(page, json, container){
        startLoading(container);
        $.post( page, json)
            .done(function( data ) {
                $(container).html(data);
                var mobile_numbers = $("#mobile_numbers").val();
                $('input[name=contactNumberCheck]').map(function() {
                    if(mobile_numbers.indexOf($(this).attr("data")) >= 0){
                        $(this).attr('checked', true);
                    }
                    else{
                        $(this).attr('checked', false);
                    }
                });
            });
    }

    function addPreloader(contentView){
        var loader = '<div class="preloader pl-size-xs">' +
            '<div class="spinner-layer pl-red-grey">' +
            '<div class="circle-clipper left">' +
            '<div class="circle"></div>' +
            '</div>' +
            '<div class="circle-clipper right">' +
            '<div class="circle"></div>' +
            '</div>' +
            '</div>' +
            '</div>';
        $("#"+contentView).html(loader);
    }

    $(document).ready(function(){

        $("#unicode").change(function () {
            onLoad();
        });

        function removeItem(array, item){
            for(var i in array){
                if(array[i]==item){
                    array.splice(i,1);
                    break;
                }
            }
        }

        var groupCheckboxValues = "";
        var groupsName = "";

        $("input:checkbox.contactGroupCheck").click(function() {
            if($(this).is(":checked")){
                groupCheckboxValues = groupCheckboxValues + (groupCheckboxValues==""?"":",") + $(this).val();
                groupsName = groupsName + (groupsName==""?"":",") + $(this).attr("data");
                $("#selected_groups").html(groupsName);
            }
            else {
                var groups = groupsName.split(',');
                removeItem(groups, $(this).attr("data"));
                groupsName = groups.join(",");
                $("#selected_groups").html(groupsName);

                var groupsIds = groupCheckboxValues.split(',');
                removeItem(groupsIds, $(this).val());
                groupCheckboxValues = groupsIds.join(",");
            }
        });

        $("input:checkbox.contactNumberCheck").click(function() {
            if($(this).is(":checked")){
                var mobile_numbers = $("#mobile_numbers").val();
                $("#mobile_numbers").val(mobile_numbers+(mobile_numbers==""?"":",")+$(this).attr("data"));
            }
            else {
                var mobile_numbers = $("#mobile_numbers").val();
                var mobileNumbers = mobile_numbers.split(',');
                removeItem(mobileNumbers, $(this).attr("data"));
                $("#mobile_numbers").val(mobileNumbers.join(","));
            }
        });

        $('#message_box').keyup(function(){
            var message = $('#message_box').val();
            var count = message.length;

            var smsLength = 160;
            if($("#unicode").val() == 1){
                smsLength = 67;
            }

            var totalSMS = Math.ceil(count / smsLength);

            if(totalSMS <= 1){
                $("#message_counter").html("<label style='color: green'>("+ count + "/" + totalSMS +")</label>");
            }
            else{
                $("#message_counter").html("<label style='color: red'>("+ count + "/" + totalSMS +")</label>");
            }            

        }).keypress(function(e){
            
            var message = $('#message_box').val();
            var count = message.length;

            var maxSMS = 7;

            var smsLength = 160;
            if($("#unicode").val() == 1){
                smsLength = 67;
            }

            var totalSMSLength = (smsLength * maxSMS) - 1;
            
            if (totalSMSLength < count)
                e.preventDefault();
        });

        $('#mobile_numbers').keypress(function(e){
            var a = [44];
            var k = e.which;
            for (i = 48; i < 58; i++)
                a.push(i);
            if (!(a.indexOf(k)>=0))
                e.preventDefault();

        }).keyup(function(){
            var numbers = $('#mobile_numbers');
            if(numbers.val() == ","){
                numbers.val("");
            }
            else if(numbers.val().indexOf(",,") >= 0){
                numbers.val(numbers.val().replace(",,", ","));
            }

            numbers.val(numbers.val().replace(/[A-Za-z$-$+]/g, ""));
        });

        var selected_groups = [];

        $("#sendSMSNow").click(function(){
            var route = 4;
            var sender_id = $("#sender_id").val();
            var mobile_numbers = $("#mobile_numbers").val();
            var message_box = $("#message_box").val();
            var campaign = $("#campaign").val();
            var unicode = $("#unicode").val();

            if(mobile_numbers.length <= 9 && groupCheckboxValues == ""){
                showNotification("bg-orange", "Please enter valid mobile number", "top", "left", "animated bounceInUp", "animated bounceOutUp");
            }
            else if(message_box == ""){
                showNotification("bg-orange", "Please enter message text", "top", "left", "animated bounceInUp", "animated bounceOutUp");
            }
            else{
                $("#sendSMSNow").attr("disabled", true);
                var count = message_box.length;
                var smsLength = 160;
                if($("#unicode").val() == 1){
                    smsLength = 67;
                }
                var totalSMS = Math.ceil(count / smsLength);
                var json = {credit:totalSMS, groups:groupCheckboxValues, unicode:unicode, route: route, sender_id:sender_id, mobile_numbers:mobile_numbers, message:message_box, campaign:campaign, schedule:"" };

                $.post( "/sms/send", json)
                    .done(function( data ) {
                        $("#sendSMSNow").attr("disabled", false);
                        console.log(data);
                        var response = JSON.parse(data);
                        if(response.status == 1){
                            $("#sms_credit_balance").html(response.data.balance);
                            $("#mobile_numbers").val("");
                            $("#message_box").val("");
                            $("#selected_groups").html("");
                            $("#message_counter").html("<label style='color: green'>(0/0)</label>");

                            groupCheckboxValues = "";
                            groupsName = "";

                            $('input[name=contactNumberCheck]:checked').map(function() {
                                $(this).attr('checked', false);
                            });

                            $('input[name=contactGroupCheck]:checked').map(function() {
                                $(this).attr('checked', false);
                            });

                            showNotification("bg-green", response.message, "top", "left", "animated bounceInUp", "animated bounceOutUp");
                        }
                        else{
                            showNotification("bg-orange", response.message, "top", "left", "animated bounceInUp", "animated bounceOutUp");
                        }
                    });
            }
        });

        $("#scheduleSMSNow").click(function(){            
            var route = 4;
            var sender_id = $("#sender_id").val();
            var mobile_numbers = $("#mobile_numbers").val();
            var message_box = $("#message_box").val();
            var campaign = $("#campaign").val();
            var unicode = $("#unicode").val();
            var date = $("#scheduleDate").val();
            var hours = $("#scheduleHours").val();
            var minuts = $("#scheduleMinuts").val();

            if(mobile_numbers.length <= 9 && groupCheckboxValues == ""){
                showNotification("bg-orange", "Please enter valid mobile number", "top", "left", "animated bounceInUp", "animated bounceOutUp");
            }
            else if(message_box == ""){
                showNotification("bg-orange", "Please enter message text", "top", "left", "animated bounceInUp", "animated bounceOutUp");
            }
            else{
                $("#scheduleSMSNowButton").click();
            }
        });

        $("#scheduleSMSSendNow").click(function(){
            var route = 4;
            var sender_id = $("#sender_id").val();
            var mobile_numbers = $("#mobile_numbers").val();
            var message_box = $("#message_box").val();
            var campaign = $("#campaign").val();
            var unicode = $("#unicode").val();
            var date = $("#scheduleDate").val();
            var hours = $("#scheduleHours").val();
            var minuts = $("#scheduleMinuts").val();
            var dateString = myDateFormatter(date) + " " + hours + ":" + minuts + ":00";
            $("#model_message_schedule").html("");

            if(date == ""){
                $("#model_message_schedule").html("Please choose schedule date");
            }
            else if(hours == ""){
                $("#model_message_schedule").html("Please choose schedule hours");
            }
            else if(minuts == ""){
                $("#model_message_schedule").html("Please choose schedule minuts");
            }
            else if (isValidSchedule(dateString) == false){
                $("#model_message_schedule").html("Schedule date time is incorrect. Schedule date always use for future date.");
            }
            else{
                $("#scheduleSMSSendNow").attr("disabled", true);
                var count = message_box.length;
                var smsLength = 160;
                if($("#unicode").val() == 1){
                    smsLength = 67;
                }
                var totalSMS = Math.ceil(count / smsLength);
                var json = {credit:totalSMS, groups:groupCheckboxValues, unicode:unicode, route: route, sender_id:sender_id, mobile_numbers:mobile_numbers, message:message_box, campaign:campaign, schedule:dateString };

                $.post( "/sms/send", json)
                    .done(function( data ) {
                        $("#scheduleSMSSendNow").attr("disabled", false);
                        var response = JSON.parse(data);
                        if(response.status == 1){
                            $("#sms_credit_balance").html(response.data.balance);
                            $("#mobile_numbers").val("");
                            $("#message_box").val("");
                            $("#selected_groups").html("");
                            $("#message_counter").html("<label style='color: green'>(0/0)</label>");

                            groupCheckboxValues = "";
                            groupsName = "";

                            $('input[name=contactNumberCheck]:checked').map(function() {
                                $(this).attr('checked', false);
                            });

                            $('input[name=contactGroupCheck]:checked').map(function() {
                                $(this).attr('checked', false);
                            });

                            $("#scheduleSMSClose").click();

                            showNotification("bg-green", response.message, "top", "left", "animated bounceInUp", "animated bounceOutUp");
                        }
                        else{
                            $("#model_message_schedule").html(response.message);
                        }
                    });
            }

            
        });

    });
</script>

<script>
    function myDateFormatter (dateString) {
        var d = new Date(dateString);
        var day = d.getDate();
        var month = d.getMonth() + 1;
        var year = d.getFullYear();
        if (day < 10) {
            day = "0" + day;
        }
        if (month < 10) {
            month = "0" + month;
        }
        var date = year + "-" + month + "-" + day;

        return date;
    };

    function isValidSchedule(dateString){
        var schedule = new Date(dateString);
        var today = new Date();
        if(schedule > today) {
            return true;
        }
        return false;
    }
</script>

</body>

</html>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title><?=PRODUCTNAME?></title>
    <!-- Favicon-->
    <link rel="icon" href="/assets/logo-small.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="/assets/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="/assets/plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="/assets/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="/assets/css/style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="/assets/css/themes/all-themes.css" rel="stylesheet" />

    <!-- Dropzone Css -->
    <link href="/assets/plugins/dropzone/dropzone.css" rel="stylesheet">

    <!-- Multi Select Css -->
    <link href="/assets/plugins/multi-select/css/multi-select.css" rel="stylesheet">
    <link href="/assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />

    <!-- Bootstrap Material Datetime Picker Css -->
    <link href="/assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />

    <!-- Sweetalert Css -->
    <link href="/assets/plugins/sweetalert/sweetalert.css" rel="stylesheet" />

</head>

<body class="theme-red">
<!-- Page Loader -->
<?php include "include/pageloader.php"; ?>
<!-- #END# Page Loader -->
<!-- Overlay For Sidebars -->
<div class="overlay"></div>
<!-- #END# Overlay For Sidebars -->

<!-- Top Bar -->
<?php include("include/header.php") ?>
<!-- #Top Bar -->
<section>
    <!-- Left Sidebar -->
    <?php include("include/leftmenu.php"); ?>
    <!-- #END# Left Sidebar -->
</section>

<section class="content">
    <div class="container-fluid">
        <div class="block-header">

            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px; margin: 0px;">
                    <div class="card" style="margin-top: -20px;">
                        <div class="header js-sweetalert">
                            <h2>
                                ALL CONTACTS
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <button type="button" class="btn bg-pink waves-effect" data-toggle="modal" data-target="#defaultModal1" data-type="confirm">Add New Contact</button>
                                </li>
                            </ul>
                        </div>
                        <div class="body table-responsive" style="padding: 5px;">
                            <table class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th style="text-align: center">#</th>
                                    <th>MOBILE</th>
                                    <th>NAME</th>
                                    <th>GROUPS</th>
                                    <th>Reference</th>
                                    <th class="text-center">ACTION</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $serial = 0;
                                    foreach($this->contacts as $contact){
                                        $serial++;
                                        ?>
                                        <tr>
                                            <td style="text-align: center"><?=$serial ?></td>
                                            <td><?=$contact["mobile"]?></td>
                                            <td><?=$contact["name"]?></td>
                                            <td><?=$contact["groups"]?></td>
                                            <td><?=$contact["reference"]?></td>
                                            <td class="text-center">
                                                <button type="button" class="btn bg-green waves-effect btn-xs" data-toggle="tooltip" data-placement="top" title="View"><i class="material-icons">visibility</i></button>&nbsp;&nbsp;
                                                <button type="button" class="btn bg-blue waves-effect btn-xs" data-toggle="tooltip" data-placement="top" title="Edit"><i class="material-icons">edit</i></button>&nbsp;&nbsp;
                                                <button type="button" class="btn bg-red waves-effect btn-xs" data-toggle="tooltip" data-placement="top" title="Delete"><i class="material-icons">delete</i></button>
                                            </td>
                                        </tr>
                                    <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->

        </div>
    </div>
</section>

<!-- Default Size -->
<div class="modal fade" id="defaultModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
                <div class="card">
                    <div class="body" style="padding-bottom: 0px;padding-top: 0;">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#home_with_icon_title" data-toggle="tab">
                                    <i class="material-icons">add_circle</i> ADD CONTACT
                                </a>
                            </li>
                            <li role="presentation">
                                <a href="#profile_with_icon_title" data-toggle="tab">
                                    <i class="material-icons">import_export</i> IMPORT CONTACT
                                </a>
                            </li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="home_with_icon_title">
                                <br>
                                <form class="form-horizontal" method="post" action="/contact/add">
                                    <div class="row clearfix">
                                        <div class="col-lg-3 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                            <label for="email_address_2">Full Name</label>
                                        </div>
                                        <div class="col-lg-9 col-md-10 col-sm-8 col-xs-7">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" id="name" class="form-control" placeholder="Enter full name">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row clearfix">
                                        <div class="col-lg-3 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                            <label for="email_address_2">Mobile Number</label>
                                        </div>
                                        <div class="col-lg-9 col-md-10 col-sm-8 col-xs-7">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="number" id="mobile" maxlength="10" minlength="10" class="form-control" placeholder="Enter mobile number">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row clearfix">
                                        <div class="col-lg-3 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                            <label for="email_address_2">Group</label>
                                        </div>
                                        <div class="col-lg-9 col-md-10 col-sm-8 col-xs-7">
                                            <div class="form-group">
                                                <select id="groups" class="form-control show-tick" multiple>
                                                    <?php
                                                    foreach($this->groups as $group){
                                                        if($group["group_name"] == "Default Group") { continue; }
                                                        echo '<option value="'.$group["id"].'">'.$group["group_name"].'</option>';
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row clearfix">
                                        <div class="col-lg-3 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                            <label for="email_address_2">Reference #</label>
                                        </div>
                                        <div class="col-lg-9 col-md-10 col-sm-8 col-xs-7">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" id="reference" class="form-control" placeholder="Enter reference number">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <hr/>
                                    <div class="modal-footer" style="padding-top: 0px; padding-bottom: 10px;">
                                        <div class="col-lg-8">
                                            <div id="error_message" style="color:red; text-align: center"></div>
                                        </div>
                                        <div class="col-lg-4">
                                            <button type="button" id="saveContact" class="btn btn-primary waves-effect">SAVE</button>
                                            <button type="button" class="btn btn-warning waves-effect" data-dismiss="modal">CLOSE</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="profile_with_icon_title">
                                <div class="body">
                                    <form action="/contact/import" id="frmFileUpload" method="POST" enctype="multipart/form-data">
                                        <input type="hidden" name="groupId" value="<?=$this->currentGroupId ?>">
                                        <div class="dz-message">
                                            <h3>Import Contacts</h3>
                                        </div>
                                        <div class="fallback">
                                            <input name="fileToUpload" type="file" id="fileToUpload" />
                                        </div>

                                    </form>
                                    <div class="modal-footer" style="padding-top: 20px; padding-bottom: 0;">
                                        <div class="col-lg-4">
                                            <div id="error_message" style="color:red; text-align: center"></div>
                                        </div>
                                        <div class="col-lg-8" style="margin-bottom: 0;">
                                            <button type="button" id="uploadContactList" class="btn btn-primary waves-effect">UPLOAD</button>
                                            <button type="button" class="btn btn-warning waves-effect" data-dismiss="modal">CLOSE</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>

<!-- Jquery Core Js -->
<script src="/assets/plugins/jquery/jquery.min.js"></script>

<!-- Bootstrap Core Js -->
<script src="/assets/plugins/bootstrap/js/bootstrap.js"></script>

<!-- Select Plugin Js -->
<script src="/assets/plugins/bootstrap-select/js/bootstrap-select.js"></script>

<!-- Slimscroll Plugin Js -->
<script src="/assets/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

<!-- Waves Effect Plugin Js -->
<script src="/assets/plugins/node-waves/waves.js"></script>

<!-- Custom Js -->
<script src="/assets/js/admin.js"></script>
<script src="/assets/js/pages/ui/tooltips-popovers.js"></script>

<!-- Multi Select Plugin Js -->
<script src="/assets/plugins/multi-select/js/jquery.multi-select.js"></script>
<script src="/assets/plugins/bootstrap-select/js/bootstrap-select.js"></script>

<script src="/assets/js/pages/forms/basic-form-elements.js"></script>

<!-- Bootstrap Material Datetime Picker Plugin Js -->
<script src="/assets/plugins/momentjs/moment.js"></script>
<script src="/assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>

<!-- Autosize Plugin Js -->
<script src="/assets/plugins/autosize/autosize.js"></script>

<script src="/assets/js/contact.js"></script>

<!-- Dropzone Plugin Js -->
<script src="/assets/plugins/dropzone/dropzone.js"></script>

<script src="/assets/js/pages/ui/dialogs.js"></script>

<!-- SweetAlert Plugin Js -->
<script src="/assets/plugins/sweetalert/sweetalert.min.js"></script>


</body>

</html>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title><?=PRODUCTNAME?></title>
    <!-- Favicon-->
    <link rel="icon" href="/assets/logo-small.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="/assets/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="/assets/plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="/assets/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="/assets/css/style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="/assets/css/themes/all-themes.css" rel="stylesheet" />

    <link href="/assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
</head>

<body class="theme-red">
<!-- Page Loader -->
<?php include "include/pageloader.php"; ?>
<!-- #END# Page Loader -->
<!-- Overlay For Sidebars -->
<div class="overlay"></div>
<!-- #END# Overlay For Sidebars -->

<!-- Top Bar -->
<?php include("include/header.php") ?>
<!-- #Top Bar -->
<section>
    <!-- Left Sidebar -->
    <?php include("include/leftmenu.php"); ?>
    <!-- #END# Left Sidebar -->
</section>

<section class="content">
    <div class="container-fluid">
        <div class="block-header">

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px; margin: 0px;">
                    <div class="row">
                        <div class="col-lg-8" style="padding-right: 5px;">
                            <div class="card" style="margin-top: -20px;">
                                <div class="header" style="padding-bottom: 0">
                                    <div class="row">
                                        <div class="col-lg-8">
                                            <h2>CONTACTS</h2>
                                        </div>
                                        <div class="col-lg-4">
                                            <select id="select_group_table" class="form-control show-tick">
                                                <option value="0">All</option>
                                                <?php
                                                foreach($this->groups as $group){
                                                    if($group["group_name"] == "Default Group") { continue; }
                                                    echo '<option value="'.$group["id"].'">'.$group["group_name"].'</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="body table-responsive" style="padding: 5px;">
                                    <table class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th style="padding: 0; text-align: center; padding-top: 10px;">
                                            </th>
                                            <th>MOBILE</th>
                                            <th>NAME</th>
                                            <th>GROUPS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        foreach($this->contacts as $contact){
                                            ?>
                                            <tr>
                                                <td style="padding: 0; text-align: center; padding-top: 10px;">
                                                    <input name="contactNumberCheck" type="checkbox" id="md_checkbox_<?=$contact["id"]?>" class="filled-in chk-col-teal contactNumberCheck" data="<?=$contact["mobile"]?>"/>
                                                    <label for="md_checkbox_<?=$contact["id"]?>"></label>
                                                </td>
                                                <td><?=$contact["mobile"]?></td>
                                                <td><?=$contact["name"]?></td>
                                                <td><?=$contact["groups"]?></td>
                                            </tr>
                                        <?php
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4" style="padding-right: 5px;">

                            <div class="info-box bg-teal hover-expand-effect" style="margin-top: -20px;">
                                <div class="icon">
                                    <i class="material-icons">message</i>
                                </div>
                                <div class="content">
                                    <div class="text">SMS CREDIT</div>
                                    <div id="sms_credit_balance" class="number" style="margin-top: 0;"><?=$this->user->balance->transactional ?></div>
                                </div>
                            </div>

                            <div class="card" style="margin-top: -20px;">
                                <div class="header">
                                    <div class="col-lg-6" style="margin-top: -6px">
                                        <h2>SEND SMS</h2>
                                    </div>
                                </div>
                                <div class="body" style="padding: 5px;">
                                    <form class="form-horizontal" method="post">

                                        <div class="row clearfix" style="margin-left: 0; margin-top: 10px;">
                                            <div class="col-lg-6" style="padding-right: 10px;">
                                                <select id="route" class="form-control show-tick">
                                                    <option value="1">Promotional</option>
                                                    <option value="4">Transactional</option>
                                                </select>
                                            </div>
                                            <div class="col-lg-6" style="padding-right: 20px;">
                                                <div class="form-group">
                                                    <select id="sender_id" class="form-control show-tick" title="Select Sender">
                                                        <option value="">Select Sender</option>
                                                        <option value="APAWAN">APAWAN</option>
                                                        <option value="HMDAMO">HMDAMO</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row clearfix" style="margin-left: 10px;">
                                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8" style="margin-bottom: 7px">
                                                <label id="selected_groups"></label>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 form-control-label" style="margin-bottom: 5px">
                                                <div class="form-group">
                                                    <a href="#" data-toggle="modal" data-target="#defaultModal">Select Group</a>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row clearfix" style="margin-left: 20px;">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <textarea rows="2" id="mobile_numbers" class="form-control no-resize" style="border: solid;border-width: 1px;padding: 5px;border-color: lightgray;" placeholder="Enter mobile numbers. eg. XXXXXXXXXX, XXXXXXXXXX"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row clearfix" style="margin-left: 20px;">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-lg-6" style="margin-bottom: 0;padding-bottom: 0;">
                                                            <select class="form-control show-tick">
                                                                <option>English</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-lg-6" style="text-align: right; padding-top: 10px; padding-right: 25px; margin-bottom: 0;padding-bottom: 0;">
                                                            <div id="message_counter"><label style='color: green'>(0/0)</label></div>
                                                        </div>
                                                    </div>
                                                    <div class="form-line">
                                                        <textarea id="message_box" rows="5" class="form-control no-resize" style="border: solid;border-width: 1px;padding: 5px;border-color: lightgray;" placeholder="Enter message text"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row clearfix" style="margin-left: 20px; margin-right: 0;">
                                            <div class="col-sm-12" style="text-align: right">
                                                <button type="button" class="btn bg-orange waves-effect">SCHEDULE</button>&nbsp;&nbsp;
                                                <button type="button" class="btn bg-pink waves-effect" id="sendSMSNow">SEND NOW</button>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>


<div class="modal fade" id="defaultModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="card" style="margin-top: -20px; padding-bottom: 50px;">
                <div class="header" style="padding-bottom: 0">
                    <div class="row" style="padding-bottom: 10px;">
                        <div class="col-lg-8">
                            <h2>SELECT GROUP</h2>
                        </div>
                    </div>
                </div>
                <div class="body table-responsive" style="padding: 5px;">
                    <table class="table table-hover">
                        <tbody>
                        <?php
                        foreach($this->groups as $group){
                            if($group["group_name"] == "Default Group") { continue; }
                            ?>
                            <tr>
                                <td style="padding: 0; text-align: center; padding-top: 10px; width: 60px;">
                                    <input name="group_checkbox" type="checkbox" id="md_group_<?=$group["id"]?>" value="<?=$group["id"]?>" data="<?=$group["group_name"]?>" class="filled-in chk-col-teal" />
                                    <label for="md_group_<?=$group["id"]?>"></label>
                                </td>
                                <td><?=$group["group_name"]?></td>
                            </tr>
                            <?php
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
                <div class="col-lg-12" style="text-align: right">
                    <button type="button" class="btn btn-warning waves-effect" data-dismiss="modal">CLOSE</button>
                    <button type="button" id="clickedSelectGroup" class="btn btn-primary waves-effect" data-dismiss="modal">DONE</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Jquery Core Js -->
<script src="/assets/plugins/jquery/jquery.min.js"></script>

<!-- Bootstrap Core Js -->
<script src="/assets/plugins/bootstrap/js/bootstrap.js"></script>

<!-- Select Plugin Js -->
<script src="/assets/plugins/bootstrap-select/js/bootstrap-select.js"></script>

<!-- Slimscroll Plugin Js -->
<script src="/assets/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

<!-- Waves Effect Plugin Js -->
<script src="/assets/plugins/node-waves/waves.js"></script>

<!-- Custom Js -->
<script src="/assets/js/admin.js"></script>

<!-- Demo Js -->
<script src="/assets/js/demo.js"></script>

<!-- Multi Select Plugin Js -->
<script src="/assets/plugins/bootstrap-select/js/bootstrap-select.js"></script>

<!-- Bootstrap Notify Plugin Js -->
<script src="/assets/plugins/bootstrap-notify/bootstrap-notify.js"></script>

<script src="/assets/js/pages/ui/notifications.js"></script>

<script>

    $(document).ready(function(){

        function removeItem(array, item){
            for(var i in array){
                if(array[i]==item){
                    array.splice(i,1);
                    break;
                }
            }
        }

        $("input:checkbox.contactNumberCheck").click(function() {
            if($(this).is(":checked")){
                var mobile_numbers = $("#mobile_numbers").val();
                $("#mobile_numbers").val(mobile_numbers+(mobile_numbers==""?"":",")+$(this).attr("data"));
            }
            else {
                var mobile_numbers = $("#mobile_numbers").val();
                var mobileNumbers = mobile_numbers.split(',');
                removeItem(mobileNumbers, $(this).attr("data"));
                $("#mobile_numbers").val(mobileNumbers.join(","));
            }
        });

        $('#message_box').keyup(function(){
            var message = $('#message_box').val();
            var count = message.length;
            var totalSMS = Math.ceil(count / 160);

            if(totalSMS <= 1){
                $("#message_counter").html("<label style='color: green'>("+ count + "/" + totalSMS +")</label>");
            }
            else{
                $("#message_counter").html("<label style='color: red'>("+ count + "/" + totalSMS +")</label>");
            }
        });

        $('#mobile_numbers').keypress(function(e){
            var a = [44];
            var k = e.which;
            for (i = 48; i < 58; i++)
                a.push(i);
            if (!(a.indexOf(k)>=0))
                e.preventDefault();

        }).keyup(function(){
            var numbers = $('#mobile_numbers');
            if(numbers.val() == ","){
                numbers.val("");
            }
            else if(numbers.val().indexOf(",,") >= 0){
                numbers.val(numbers.val().replace(",,", ","));
            }

            numbers.val(numbers.val().replace(/[A-Za-z$-$+]/g, ""));
        });

        var selected_groups = [];

        $("#sendSMSNow").click(function(){
            var route = $("#route :selected").val();
            var sender_id = $("#sender_id").val();
            var mobile_numbers = $("#mobile_numbers").val();
            var message_box = $("#message_box").val();
            var campaign = $("#campaign").val();

            if(mobile_numbers.length <= 9 && groupCheckboxValues == ""){
                showErrorMessageNotification("Please enter valid mobile number");
            }
            else if(message_box == ""){
                showErrorMessageNotification("Please enter message text");
            }
            else{
                $("#sendSMSNow").attr("disabled", true);

                var json = { groups:groupCheckboxValues, route: route, sender_id:sender_id, mobile_numbers:mobile_numbers, message:message_box, campaign:campaign, schedule:"" };

                $.post( "/sms/send", json)
                    .done(function( data ) {
                        $("#sendSMSNow").attr("disabled", false);
                        console.log(data);
                        var response = JSON.parse(data);
                        if(response.status == 1){
                            $("#sms_credit_balance").html(response.data.balance);
                            $("#mobile_numbers").val("");
                            $("#message_box").val("");
                            $("#selected_groups").html("");
                            groupCheckboxValues = "";

                            $('input[name=group_checkbox]:checked').map(function() {
                                $(this).attr('checked', false);
                            });

                            showSuccessMessageNotification(response.message);
                        }
                        else{
                            showErrorMessageNotification(response.message);
                        }
                    });
            }
        });

    });

    var groupCheckboxValues = "";

    $("#clickedSelectGroup").click(function(){
        var groupsName = "";
        $('input[name=group_checkbox]:checked').map(function() {
            groupCheckboxValues = groupCheckboxValues + (groupCheckboxValues==""?"":",") + $(this).val();
            groupsName = groupsName + (groupsName==""?"":", ") + $(this).attr("data");
        });
        $("#selected_groups").html(groupsName);
    });

    function showSuccessMessageNotification(message){
        showNotification("bg-green", message, "top", "right", "animated bounceInUp", "animated bounceOutUp");
    }

    function showErrorMessageNotification(message){
        showNotification("bg-orange", message, "top", "right", "animated bounceInUp", "animated bounceOutUp");
    }

</script>

</body>

</html>
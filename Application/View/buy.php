<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="google" value="notranslate">

    <title><?=PRODUCTNAME?></title>
    <!-- Favicon-->
    <link rel="icon" href="/assets/logo-small.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="/assets/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="/assets/plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="/assets/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="/assets/css/style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="/assets/css/themes/all-themes.css" rel="stylesheet" />

    <!-- Sweetalert Css -->
    <link href="/assets/plugins/sweetalert/sweetalert.css" rel="stylesheet" />
</head>

<body class="theme-red" style="background-color: #00BCD4">
<!-- Page Loader -->
<!-- #END# Page Loader -->
<!-- Overlay For Sidebars -->
<div class="overlay"></div>
<!-- #END# Overlay For Sidebars -->

<!-- Top Bar -->
<?php include("include/header.php") ?>
<!-- #Top Bar -->
<section>
    <!-- Left Sidebar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar" style="width: 220px;">
            <!-- User Info -->
            <div class="user-info" style="height: 80px;">
                <div class="info-container" style="margin-top: -25px;">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><h4><?=$this->user->full_name?></h4></div>
                    <div class="email"><?=$this->user->email?></div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li>
                        <a href="#" data-toggle="modal" data-target="#modelAddGroup">
                            <i class="material-icons">group_add</i>
                            <span>ADD GROUP</span>
                        </a>
                    </li>
                    <hr style="margin: 0;" />
                    <li id="slide_menu_group_0">
                        <a href="/?page=0" data_name="0">
                            <i class="material-icons">contacts</i>
                            <span>Default Group (<?=$this->totalCount ?>)</span>
                        </a>
                    </li>

                    <?php
                    foreach($this->groups as $group){
                        if($group["group_name"] == "Default Group") { continue; }
                        ?>
                        <li class="<?=($this->currentGroupId == $group['id'])?"active ":""?> slide_menu_group" id="slide_menu_group_<?=$group['id']?>">
                            <a href="/?page=<?=$group['id']?>" data_name="<?=$group["id"] ?>">
                                <i class="material-icons">contacts</i>
                                <span><?=$group["group_name"]?> (<?=$group["count"]?>)</span>
                            </a>
                        </li>
                    <?php
                    }
                    ?>

                    <hr style="margin: 0;" />
                    <li class="slide_menu_group" id="slide_menu_group_send">
                        <a href="/sms/send">
                            <i class="material-icons">send</i>
                            <span>Send SMS</span>
                        </a>
                    </li>
                    <hr style="margin: 0;" />
                    <li class="slide_menu_group" id="slide_menu_group_send">
                        <a href="/user/profile">
                            <i class="material-icons">person</i>
                            <span>Profile</span>
                        </a>
                    </li>
                    <hr style="margin: 0;" />
                    <li class="slide_menu_group" id="slide_menu_group_send">
                        <a href="/transaction/deliveryreport">
                            <i class="material-icons">info</i>
                            <span>Delivery Report</span>
                        </a>
                    </li>
                    <hr style="margin: 0;" />
                    <li class="slide_menu_group" id="slide_menu_group_blacklist">
                        <a href="/transaction/all">
                            <i class="material-icons">account_balance</i>
                            <span>Transaction</span>
                        </a>
                    </li>
                    <hr style="margin: 0;" />
                    <li class="active slide_menu_group" id="slide_menu_group_blacklist">
                        <a href="/sms/price">
                            <i class="material-icons">shopping_cart</i>
                            <span>Buy More Credits</span>
                        </a>
                    </li>
                    <hr style="margin: 0;" />
                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    &copy; 2017 <a href="javascript:void(0);"><?=PRODUCTNAME?></a>
                </div>
                <div class="version">
                    All Rights Reserved.
                </div>
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
    </section>
    <!-- #END# Left Sidebar -->
</section>

<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <div class="logo" style="text-align: center; margin: 20px;padding: 20px">
                <img src="/assets/logo.png"><br><br>
                <small style="font-size: 20px;color: white"><b>We are sms provider</b></small>
            </div>
            <div class="row" style="margin: 0; padding: 0;">
                <div class="col-lg-3">
                    <div class="card">
                        <div class="header bg-pink" style="text-align: center">
                            <h2>
                                10,000 SMS
                            </h2>
                        </div>
                        <div class="body" style="padding: 0;">
                            <div class="list-group">
                                <a href="javascript:void(0);" class="list-group-item">Transactional SMS</a>
                                <a href="javascript:void(0);" class="list-group-item">Unlimited Validity</a>
                                <a href="javascript:void(0);" class="list-group-item">Low in Price</a>
                                <a href="javascript:void(0);" class="list-group-item active" style="text-align: center">
                                    ₹ 2,000/-
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3">
                    <div class="card">
                        <div class="header bg-teal" style="text-align: center">
                            <h2>
                                30,000 SMS
                            </h2>
                        </div>
                        <div class="body" style="padding: 0;">
                            <div class="list-group">
                                <a href="javascript:void(0);" class="list-group-item">Transactional SMS</a>
                                <a href="javascript:void(0);" class="list-group-item">Unlimited Validity</a>
                                <a href="javascript:void(0);" class="list-group-item">Low in Price</a>
                                <a href="javascript:void(0);" class="list-group-item active" style="text-align: center">
                                    ₹ 6,000/-
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3">
                    <div class="card">
                        <div class="header bg-purple" style="text-align: center">
                            <h2>
                                50,000 SMS
                            </h2>
                        </div>
                        <div class="body" style="padding: 0;">
                            <div class="list-group">
                                <a href="javascript:void(0);" class="list-group-item">Transactional SMS</a>
                                <a href="javascript:void(0);" class="list-group-item">Unlimited Validity</a>
                                <a href="javascript:void(0);" class="list-group-item">Low in Price</a>
                                <a href="javascript:void(0);" class="list-group-item active" style="text-align: center">
                                    ₹ 9,500/-
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3">
                    <div class="card">
                        <div class="header bg-brown" style="text-align: center">
                            <h2>
                                1,00,000 SMS
                            </h2>
                        </div>
                        <div class="body" style="padding: 0;">
                            <div class="list-group">
                                <a href="javascript:void(0);" class="list-group-item">Transactional SMS</a>
                                <a href="javascript:void(0);" class="list-group-item">Unlimited Validity</a>
                                <a href="javascript:void(0);" class="list-group-item">Low in Price</a>
                                <a href="javascript:void(0);" class="list-group-item active" style="text-align: center">
                                    Rs 18,500/-
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="logo" style="text-align: center; margin: 20px;padding: 20px">
                <small style="font-size: 18px;color: white">To purchase any package please contact us at email <b>vikasaj9098@gmail.com</b> or mobile <b>8103478849, 9755341315</b></small>
            </div>

        </div>
    </div>
</section>

<!-- Jquery Core Js -->
<script src="/assets/plugins/jquery/jquery.min.js"></script>

<!-- Bootstrap Core Js -->
<script src="/assets/plugins/bootstrap/js/bootstrap.js"></script>

<!-- Select Plugin Js -->
<script src="/assets/plugins/bootstrap-select/js/bootstrap-select.js"></script>

<!-- Slimscroll Plugin Js -->
<script src="/assets/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

<!-- Waves Effect Plugin Js -->
<script src="/assets/plugins/node-waves/waves.js"></script>

<!-- Custom Js -->
<script src="/assets/js/admin.js"></script>

<!-- Demo Js -->
<script src="/assets/js/demo.js"></script>

<script src="/assets/js/pages/ui/notifications.js"></script>
<!-- Bootstrap Notify Plugin Js -->
<script src="/assets/plugins/bootstrap-notify/bootstrap-notify.js"></script>


</body>

</html>
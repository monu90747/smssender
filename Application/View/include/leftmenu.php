<?php
$page = "Dashboard";
if($this->view == "dashboard"){
    $page = "Dashboard";
}
elseif($this->view == "contact"){
    $page = "Contact";
}
elseif($this->view == "group"){
    $page = "Group";
}
elseif($this->view == "setting"){
    $page = "Setting";
}

?>

<aside id="leftsidebar" class="sidebar" style="width: 220px;">
    <!-- User Info -->
    <div class="user-info" style="height: 80px;">
        <div class="info-container" style="margin-top: -25px;">
            <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><h4><?=$this->user->full_name?></h4></div>
            <div class="email"><?=$this->user->email?></div>
        </div>
    </div>
    <!-- #User Info -->
    <!-- Menu -->
    <div class="menu">
        <ul class="list">
            <li <?=($page == "Dashboard")? "class='active'":"" ?>>
                <a href="/">
                    <i class="material-icons">home</i>
                    <span>Dashboard</span>
                </a>
            </li>

            <li <?=($page == "Contact")? "class='active'":"" ?>>
                <a href="/contact">
                    <i class="material-icons">contacts</i>
                    <span>Contact</span>
                </a>
            </li>

            <li <?=($page == "Group")? "class='active'":"" ?>>
                <a href="/group">
                    <i class="material-icons">group</i>
                    <span>Group</span>
                </a>
            </li>

            <li <?=($page == "Setting")? "class='active'":"" ?>>
                <a href="/setting">
                    <i class="material-icons">settings</i>
                    <span>Setting</span>
                </a>
            </li>

        </ul>
    </div>
    <!-- #Menu -->
    <!-- Footer -->
    <div class="legal">
        <div class="copyright">
            &copy; 2017 <a href="javascript:void(0);"><?=PRODUCTNAME?></a>
        </div>
        <div class="version">
            All Rights Reserved.
        </div>
    </div>
    <!-- #Footer -->
</aside>
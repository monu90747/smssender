<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="google" value="notranslate">

    <title><?=PRODUCTNAME?></title>
    <!-- Favicon-->
    <link rel="icon" href="/assets/logo-small.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="/assets/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="/assets/plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="/assets/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="/assets/css/style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="/assets/css/themes/all-themes.css" rel="stylesheet" />

    <!-- Sweetalert Css -->
    <link href="/assets/plugins/sweetalert/sweetalert.css" rel="stylesheet" />
</head>

<body class="theme-red">
<!-- Page Loader -->
<!-- #END# Page Loader -->
<!-- Overlay For Sidebars -->
<div class="overlay"></div>
<!-- #END# Overlay For Sidebars -->

<!-- Top Bar -->
<?php include("include/header.php") ?>
<!-- #Top Bar -->
<section>
    <!-- Left Sidebar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar" style="width: 220px;">
            <!-- User Info -->
            <div class="user-info" style="height: 80px;">
                <div class="info-container" style="margin-top: -25px;">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><h4><?=$this->user->full_name?></h4></div>
                    <div class="email"><?=$this->user->email?></div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li>
                        <a href="#" data-toggle="modal" data-target="#modelAddGroup">
                            <i class="material-icons">group_add</i>
                            <span>ADD GROUP</span>
                        </a>
                    </li>
                    <hr style="margin: 0;" />
                    <li id="slide_menu_group_0">
                        <a href="/?page=0" data_name="0">
                            <i class="material-icons">contacts</i>
                            <span>Default Group (<?=$this->totalCount ?>)</span>
                        </a>
                    </li>

                    <?php
                    foreach($this->groups as $group){
                        if($group["group_name"] == "Default Group") { continue; }
                        ?>
                        <li class="<?=($this->currentGroupId == $group['id'])?"active ":""?> slide_menu_group" id="slide_menu_group_<?=$group['id']?>">
                            <a href="/?page=<?=$group['id']?>" data_name="<?=$group["id"] ?>">
                                <i class="material-icons">contacts</i>
                                <span><?=$group["group_name"]?> (<?=$group["count"]?>)</span>
                            </a>
                        </li>
                    <?php
                    }
                    ?>

                    <hr style="margin: 0;" />
                    <li class="slide_menu_group" id="slide_menu_group_send">
                        <a href="/sms/send">
                            <i class="material-icons">send</i>
                            <span>Send SMS</span>
                        </a>
                    </li>
                    <hr style="margin: 0;" />
                    <li class="slide_menu_group" id="slide_menu_group_send">
                        <a href="/user/profile">
                            <i class="material-icons">person</i>
                            <span>Profile</span>
                        </a>
                    </li>
                    <hr style="margin: 0;" />
                    <li class="active slide_menu_group" id="slide_menu_group_send">
                        <a href="/transaction/deliveryreport">
                            <i class="material-icons">info</i>
                            <span>Delivery Report</span>
                        </a>
                    </li>
                    <hr style="margin: 0;" />
                    <li class="slide_menu_group" id="slide_menu_group_blacklist">
                        <a href="/transaction/all">
                            <i class="material-icons">account_balance</i>
                            <span>Transaction</span>
                        </a>
                    </li>
                    <hr style="margin: 0;" />
                    <li class="slide_menu_group" id="slide_menu_group_blacklist">
                        <a href="/sms/price">
                            <i class="material-icons">shopping_cart</i>
                            <span>Buy More Credits</span>
                        </a>
                    </li>
                    <hr style="margin: 0;" />
                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    &copy; 2017 <a href="javascript:void(0);"><?=PRODUCTNAME?></a>
                </div>
                <div class="version">
                    All Rights Reserved.
                </div>
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
    </section>
    <!-- #END# Left Sidebar -->
</section>

<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <div class="row clearfix">
                <div class="col-lg-4 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-light-blue hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">sms</i>
                        </div>
                        <div class="content">
                            <div class="text">Total Sent</div>
                            <div id="sms_credit_balance" class="number" style="margin-top: 0;"><?=$this->total_sent?></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-light-green hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">playlist_add_check</i>
                        </div>
                        <div class="content">
                            <div class="text">Delivered</div>
                            <div id="sms_credit_balance" class="number" style="margin-top: 0;"><?=$this->sent ?></div>
                        </div>
                    </div>
                </div>
                <!-- <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-orange hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">warning</i>
                        </div>
                        <div class="content">
                            <div class="text">Pending</div>
                            <div id="sms_credit_balance" class="number" style="margin-top: 0;"><?=$this->pending ?></div>
                        </div>
                    </div>
                </div> -->
                <div class="col-lg-4 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-red hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">error</i>
                        </div>
                        <div class="content">
                            <div class="text">Failed</div>
                            <div id="sms_credit_balance" class="number" style="margin-top: 0;"><?=$this->failed?></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="body table-responsive" style="padding: 5px;">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>SENT</th>
                            <th>MESSAGE</th>
                            <th>REQUEST ID</th>
                            <th style="text-align: center">CREDIT</th>
                            <th style="text-align: center">TOTAL</th>
                            <th style="text-align: center">DEDUCTED</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach($this->messages as $msg){
                            echo '<tr><td>'.$msg["created_at"].'</td>';
                            echo '<td>'.$msg["body"].'</td>';
                            echo '<td>'.$msg["request_id"].'</td>';
                            echo '<td style="text-align: center">'.$msg["credit"].'</td>';
                            echo '<td style="text-align: center">'.$msg["total_sms"].'</td>';
                            echo '<td style="text-align: center">'.($msg["credit"]*$msg["total_sms"]).'</td></tr>';
                        }
                        ?>
                        </tbody>
                    </table>
                    <nav>
                        <ul class="pagination">
                            <?php
                            if($this->current_page == 1){
                                echo '<li class="disabled"><a href="javascript:void(0);"><i class="material-icons">chevron_left</i></a></li>';
                            }
                            else{
                                echo '<li><a href="javascript:void(0);" onclick="loadPaging('.($this->current_page-1).');"><i class="material-icons">chevron_left</i></a></li>';
                            }

                            if($this->total_page <= 6){
                                for($page=1; $page<=$this->total_page; $page++){
                                    if($page == $this->current_page){
                                        echo '<li class="active"><a href="javascript:void(0);">'.$page.'</a></li>';
                                    }
                                    else{
                                        echo '<li><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging('.$page.');">'.$page.'</a></li>';
                                    }
                                }
                            }
                            else{
                                if($this->current_page == 1){
                                    echo '<li class="active"><a href="javascript:void(0);">1</a></li>';
                                    echo '<li><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging(2);">2</a></li>';
                                    echo '<li><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging(3);">3</a></li>';
                                    echo '<li><a href="javascript:void(0);" class="waves-effect">...</a></li>';
                                    echo '<li><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging('.$this->total_page.');">'.$this->total_page.'</a></li>';
                                }
                                elseif($this->current_page == $this->total_page){
                                    echo '<li><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging(1);">1</a></li>';
                                    echo '<li><a href="javascript:void(0);" class="waves-effect">...</a></li>';
                                    echo '<li><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging('.($this->total_page-2).');">'.($this->total_page-2).'</a></li>';
                                    echo '<li><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging('.($this->total_page-1).');">'.($this->total_page-1).'</a></li>';
                                    echo '<li class="active"><a href="javascript:void(0);">'.$this->total_page.'</a></li>';
                                }
                                elseif($this->current_page == 2){
                                    echo '<li><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging(1);">1</a></li>';
                                    echo '<li class="active"><a href="javascript:void(0);">2</a></li>';
                                    echo '<li><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging(3);">3</a></li>';
                                    echo '<li><a href="javascript:void(0);" class="waves-effect">...</a></li>';
                                    echo '<li><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging('.$this->total_page.');">'.$this->total_page.'</a></li>';
                                }
                                elseif($this->current_page == $this->total_page-1){
                                    echo '<li><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging(1);">1</a></li>';
                                    echo '<li><a href="javascript:void(0);" class="waves-effect">...</a></li>';
                                    echo '<li><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging('.($this->total_page-2).');">'.($this->total_page-2).'</a></li>';
                                    echo '<li class="active"><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging('.($this->total_page-1).');">'.($this->total_page-1).'</a></li>';
                                    echo '<li><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging('.($this->total_page).');">'.($this->total_page).'</a></li>';
                                }
                                else{
                                    echo '<li><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging(1);">1</a></li>';
                                    if($this->current_page-2 != 1){
                                        echo '<li><a href="javascript:void(0);" class="waves-effect">...</a></li>';
                                    }

                                    echo '<li><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging('.($this->current_page-1).');">'.($this->current_page-1).'</a></li>';
                                    echo '<li class="active"><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging('.($this->current_page).');">'.($this->current_page).'</a></li>';
                                    echo '<li><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging('.($this->current_page+1).');">'.($this->current_page+1).'</a></li>';

                                    if($this->current_page+2 != $this->total_page){
                                        echo '<li><a href="javascript:void(0);" class="waves-effect">...</a></li>';
                                    }
                                    echo '<li><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging('.($this->total_page).');">'.($this->total_page).'</a></li>';
                                }
                            }

                            if($this->current_page == $this->total_page){
                                echo '<li class="disabled"><a href="javascript:void(0);"><i class="material-icons">chevron_right</i></a></li>';
                            }
                            else{
                                echo '<li><a href="javascript:void(0);" onclick="loadPaging('.($this->current_page+1).');"><i class="material-icons">chevron_right</i></a></li>';
                            }
                            ?>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Jquery Core Js -->
<script src="/assets/plugins/jquery/jquery.min.js"></script>

<!-- Bootstrap Core Js -->
<script src="/assets/plugins/bootstrap/js/bootstrap.js"></script>

<!-- Select Plugin Js -->
<script src="/assets/plugins/bootstrap-select/js/bootstrap-select.js"></script>

<!-- Slimscroll Plugin Js -->
<script src="/assets/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

<!-- Waves Effect Plugin Js -->
<script src="/assets/plugins/node-waves/waves.js"></script>

<!-- Custom Js -->
<script src="/assets/js/admin.js"></script>

<!-- Demo Js -->
<script src="/assets/js/demo.js"></script>

<script src="/assets/js/pages/ui/notifications.js"></script>
<!-- Bootstrap Notify Plugin Js -->
<script src="/assets/plugins/bootstrap-notify/bootstrap-notify.js"></script>

<!-- SweetAlert Plugin Js -->
<script src="/assets/plugins/sweetalert/sweetalert.min.js"></script>
<script src="/assets/js/pages/ui/dialogs.js"></script>

<script>

    function loadPaging(page){
        window.location = "/transaction/deliveryreport?page="+page;
    }

    function showSuccessMessageNotification(message){
        showNotification("bg-green", message, "top", "center", "animated bounceInUp", "animated bounceOutUp");
    }

    function showErrorMessageNotification(message){
        showNotification("bg-orange", message, "top", "center", "animated bounceInUp", "animated bounceOutUp");
    }

</script>

</body>

</html>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title><?=PRODUCTNAME?></title>
    <!-- Favicon-->
    <link rel="icon" href="/assets/logo-small.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="/assets/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="/assets/plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="/assets/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="/assets/css/style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="/assets/css/themes/all-themes.css" rel="stylesheet" />
    <link href="/assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />

    <!-- Sweetalert Css -->
    <link href="/assets/plugins/sweetalert/sweetalert.css" rel="stylesheet" />
</head>

<body class="theme-red">
<!-- Page Loader -->
<!-- #END# Page Loader -->
<!-- Overlay For Sidebars -->
<div class="overlay"></div>
<!-- #END# Overlay For Sidebars -->

<!-- Top Bar -->
<?php include(__DIR__."/../include/header.php") ?>
<!-- #Top Bar -->
<section>
    <!-- Left Sidebar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar" style="width: 220px;">
            <!-- User Info -->
            <div class="user-info" style="height: 80px;">
                <div class="info-container" style="margin-top: -25px;">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><h4><?=$this->user->full_name?></h4></div>
                    <div class="email"><?=$this->user->email?></div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li>
                        <a href="#" data-toggle="modal" data-target="#modelAddGroup">
                            <i class="material-icons">group_add</i>
                            <span>ADD GROUP</span>
                        </a>
                    </li>
                    <hr style="margin: 0;" />
                    <li id="slide_menu_group_0">
                        <a href="/?page=0" data_name="0">
                            <i class="material-icons">contacts</i>
                            <span>Default Group (<?=$this->totalCount ?>)</span>
                        </a>
                    </li>

                    <?php
                    foreach($this->groups as $group){
                        ?>
                        <li class="<?=($this->currentGroupId == $group['id'])?"active ":""?> slide_menu_group" id="slide_menu_group_<?=$group['id']?>">
                            <a href="/?page=<?=$group['id']?>" data_name="<?=$group["id"] ?>">
                                <i class="material-icons">contacts</i>
                                <span><?=$group["group_name"]?> (<?=$group["count"]?>)</span>
                            </a>
                        </li>
                    <?php
                    }
                    ?>

                    <hr style="margin: 0;" />
                    <li class="slide_menu_group" id="slide_menu_group_send">
                        <a href="/sms/send">
                            <i class="material-icons">send</i>
                            <span>Send SMS</span>
                        </a>
                    </li>
                    <hr style="margin: 0;" />
                    <li class="active slide_menu_group" id="slide_menu_group_send">
                        <a href="/user/profile">
                            <i class="material-icons">person</i>
                            <span>Profile</span>
                        </a>
                    </li>
                    <hr style="margin: 0;" />
                    <li class="slide_menu_group" id="slide_menu_group_send">
                        <a href="/transaction/deliveryreport">
                            <i class="material-icons">info</i>
                            <span>Delivery Report</span>
                        </a>
                    </li>
                    <hr style="margin: 0;" />
                    <li class="slide_menu_group" id="slide_menu_group_blacklist">
                        <a href="/transaction/all">
                            <i class="material-icons">account_balance</i>
                            <span>Transaction</span>
                        </a>
                    </li>
                    <hr style="margin: 0;" />
                    <li class="slide_menu_group" id="slide_menu_group_blacklist">
                        <a href="/sms/price">
                            <i class="material-icons">shopping_cart</i>
                            <span>Buy More Credits</span>
                        </a>
                    </li>
                    <hr style="margin: 0;" />
                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    &copy; 2017 <a href="javascript:void(0);"><?=PRODUCTNAME?></a>
                </div>
                <div class="version">
                    All Rights Reserved.
                </div>
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
    </section>
    <!-- #END# Left Sidebar -->
</section>

<section class="content">
    <div class="container-fluid">
        <div class="block-header">    
            <div class="row clearfix">
                
                <div class="row clearfix">
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box-2 bg-teal hover-expand-effect">
                            <div class="icon">
                                <i class="material-icons">sms</i>
                            </div>
                            <div class="content">
                                <div class="text">AVAILABLE SMS</div>
                                <div class="number"><?=$this->admin->totalSMS ?></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box-2 bg-blue hover-expand-effect">
                            <div class="icon">
                                <i class="material-icons">shopping_cart</i>
                            </div>
                            <div class="content">
                                <div class="text">SALE SMS</div>
                                <div class="number"><?=($this->admin->totalCredit - $this->admin->totalFree) ?></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box-2 bg-orange hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">equalizer</i>
                        </div>
                        <div class="content">
                            <div class="text">BONOUS SMS</div>
                            <div class="number"><?=$this->admin->totalFree ?></div>
                        </div>
                    </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box-2 bg-brown hover-expand-effect">
                            <div class="icon">
                                <i class="material-icons">account_balance_wallet</i>
                            </div>
                            <div class="content">
                                <div class="text">AMOUNT</div>
                                <div class="number"><?=$this->admin->totalPrice ?></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box-2 bg-deep-orange hover-expand-effect">
                            <div class="icon">
                                <i class="material-icons">sms</i>
                            </div>
                            <div class="content">
                                <div class="text">Portal SMS</div>
                                <div class="number"><?=$this->portalSMSCount ?></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box-2 bg-deep-orange hover-expand-effect">
                            <div class="icon">
                                <i class="material-icons">sms</i>
                            </div>
                            <div class="content">
                                <div class="text">Customers Balance</div>
                                <div class="number"><?=$this->custBalance ?></div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">                    
                    <div class="card">
                        <div class="body">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" <?=($this->tab == 1)?"class='active'":"" ?>>
                                    <a href="#home_with_icon_title" data-toggle="tab">
                                        <i class="material-icons">home</i> SALE SMS
                                    </a>
                                </li>
                                
                                <li role="presentation" <?=($this->tab == 2)?"class='active'":"" ?>>
                                    <a href="#transaction_with_icon_title" data-toggle="tab">
                                        <i class="material-icons">home</i> TRANSACTIONS
                                    </a>
                                </li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane fade in active" id="home_with_icon_title">
                                    <div class="row clearfix">
                                        <div class="col-lg-6" style="padding-left:50px; padding-top:30px">
                                        <form id="transaction" method="POST">
                                        <?php include(__DIR__."/../include/message.php"); ?>                                       <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">account_circle</i>
                                            </span>
                                            <select id="user_id" class="form-control show-tick" title="Select Sender">
                                                <option value="0">Select User</option>
                                                <?php
                                                    foreach($this->app_users as $appUser){
                                                        echo "<option value='".$appUser["id"]."'>".$appUser["mobile"]." (".$appUser["full_name"].")</option>";
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                            
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">account_balance_wallet</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="email" class="form-control" id="mode" maxlength="100" placeholder="Enter payment mode" value="">
                                            </div>
                                        </div>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">message</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="number" class="form-control" id="credit" maxlength="10" placeholder="SMS Credit Count" value="">
                                            </div>
                                        </div>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">credit_card</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="number" class="form-control" id="price" maxlength="10" placeholder="Amount (INR)" value="">
                                            </div>
                                        </div>
                                            
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">new_releases</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" class="form-control" id="description" maxlength="255" placeholder="Description" value="">
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-12">
                                                <button class="btn btn-block btn-lg bg-pink waves-effect" type="button" id="saveTransactionButton" onclick="saveTransaction()">Save</button>
                                            </div>
                                        </div>
                                    </form>
                                    </div>
                                    </div>
                                </div>
                                
                                <div role="tabpanel" class="tab-pane fade in" id="transaction_with_icon_title">
                                    <div class="row clearfix">
                                        <div class="body table-responsive" style="padding: 5px;">
                                            <table class="table table-hover">
                                                <thead>
                                                <tr>
                                                    <th>DATE</th>
                                                    <th>CUSTOMER</th>
                                                    <th>AMOUNT</th>
                                                    <th>CREDIT</th>
                                                    <th>DESCRIPTION</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    foreach($this->transactions as $transaction){
                                                        echo "<tr>";
                                                        echo "<td>".date('d F, Y', strtotime($transaction["dateTime"]))."</td>";
                                                        echo "<td>".$transaction["mobile"]." - ".$transaction["full_name"]."</td>";
                                                        echo "<td>".$transaction["price"]."</td>";
                                                        echo "<td>".$transaction["credit"]."</td>";
                                                        echo "<td>".$transaction["description"]."</td>";
                                                        echo "</tr>";
                                                    }
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Jquery Core Js -->
<script src="/assets/plugins/jquery/jquery.min.js"></script>

<!-- Bootstrap Core Js -->
<script src="/assets/plugins/bootstrap/js/bootstrap.js"></script>

<!-- Select Plugin Js -->
<script src="/assets/plugins/bootstrap-select/js/bootstrap-select.js"></script>

<!-- Slimscroll Plugin Js -->
<script src="/assets/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

<!-- Waves Effect Plugin Js -->
<script src="/assets/plugins/node-waves/waves.js"></script>

<!-- Custom Js -->
<script src="/assets/js/admin.js"></script>

<!-- Demo Js -->
<script src="/assets/js/demo.js"></script>

<script src="/assets/js/pages/ui/notifications.js"></script>
<!-- Bootstrap Notify Plugin Js -->
<script src="/assets/plugins/bootstrap-notify/bootstrap-notify.js"></script>

<!-- SweetAlert Plugin Js -->
<script src="/assets/plugins/sweetalert/sweetalert.min.js"></script>
<script src="/assets/js/pages/ui/dialogs.js"></script>

<script>

    function saveTransaction(){
        var userId = $("#user_id").val();
        var mode = $("#mode").val();
        var credit = $("#credit").val();
        var price = $("#price").val();
        var description = $("#description").val();
        var remainSMS = "<?=$this->admin->totalSMS ?>";

        if(userId == 0){
            showErrorMessageNotification("Please select user mobile.");
        }
        else if(mode == ""){
            showErrorMessageNotification("Please select payment mode.");
        }
        else if(credit == ""){
            showErrorMessageNotification("Please enter SMS credit count.");
        }
        else if(price == ""){
            showErrorMessageNotification("Please enter amount.");
        }
        else if(description == ""){
            showErrorMessageNotification("Please enter description.");
        }
        else if(parseInt(remainSMS) < parseInt(credit)) {
            console.log(remainSMS);
            showErrorMessageNotification("You have not sufficient SMS balance.");
        }
        else{
            $("#saveTransactionButton").attr("disabled", true);
            $.post( "/admin/credit", {user_id:userId, mode:mode, credit:credit, price:price, description:description})
                .done(function( data ) {
                    $("#saveTransactionButton").attr("disabled", false);
                console.log(data);
                    var response = JSON.parse(data);
                    if(response.status == 1){
                        swal({
                            title: "Transaction",
                            text: "Transaction created successfully",
                            type: "success",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "OK",
                            closeOnConfirm: true
                        }, function () {
                            window.location.reload();
                        });
                    }
                    else{
                        swal("Failed!", response.message, "error");
                    }
                });
        }
    }

    function showSuccessMessageNotification(message){
        showNotification("bg-green", message, "top", "center", "animated bounceInUp", "animated bounceOutUp");
    }

    function showErrorMessageNotification(message){
        showNotification("bg-orange", message, "top", "center", "animated bounceInUp", "animated bounceOutUp");
    }

</script>

</body>

</html>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title><?=PRODUCTNAME?></title>
    <!-- Favicon-->
    <link rel="icon" href="/assets/logo-small.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="/assets/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="/assets/plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="/assets/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="/assets/css/style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="/assets/css/themes/all-themes.css" rel="stylesheet" />

    <!-- Sweetalert Css -->
    <link href="/assets/plugins/sweetalert/sweetalert.css" rel="stylesheet" />
</head>

<body class="theme-red">
<!-- Page Loader -->
<!-- #END# Page Loader -->
<!-- Overlay For Sidebars -->
<div class="overlay"></div>
<!-- #END# Overlay For Sidebars -->

<!-- Top Bar -->
<?php include("include/header.php") ?>
<!-- #Top Bar -->
<section>
    <!-- Left Sidebar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar" style="width: 220px;">
            <!-- User Info -->
            <div class="user-info" style="height: 80px;">
                <div class="info-container" style="margin-top: -25px;">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><h4><?=$this->user->full_name?></h4></div>
                    <div class="email"><?=$this->user->email?></div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li>
                        <a href="#" data-toggle="modal" data-target="#modelAddGroup">
                            <i class="material-icons">group_add</i>
                            <span>ADD GROUP</span>
                        </a>
                    </li>
                    <hr style="margin: 0;" />
                    <li id="slide_menu_group_0">
                        <a href="/?page=0" data_name="0">
                            <i class="material-icons">contacts</i>
                            <span>Default Group (<?=$this->totalCount ?>)</span>
                        </a>
                    </li>

                    <?php
                    foreach($this->groups as $group){
                        if($group["group_name"] == "Default Group") { continue; }
                        ?>
                        <li class="<?=($this->currentGroupId == $group['id'])?"active ":""?> slide_menu_group" id="slide_menu_group_<?=$group['id']?>">
                            <a href="/?page=<?=$group['id']?>" data_name="<?=$group["id"] ?>">
                                <i class="material-icons">contacts</i>
                                <span><?=$group["group_name"]?> (<?=$group["count"]?>)</span>
                            </a>
                        </li>
                    <?php
                    }
                    ?>

                    <hr style="margin: 0;" />
                    <li class="slide_menu_group" id="slide_menu_group_send">
                        <a href="/sms/send">
                            <i class="material-icons">send</i>
                            <span>Send SMS</span>
                        </a>
                    </li>
                    <hr style="margin: 0;" />
                    <li class="active slide_menu_group" id="slide_menu_group_send">
                        <a href="/user/profile">
                            <i class="material-icons">person</i>
                            <span>Profile</span>
                        </a>
                    </li>
                    <hr style="margin: 0;" />
                    <li class="slide_menu_group" id="slide_menu_group_send">
                        <a href="/transaction/deliveryreport">
                            <i class="material-icons">info</i>
                            <span>Delivery Report</span>
                        </a>
                    </li>
                    <hr style="margin: 0;" />
                    <li class="slide_menu_group" id="slide_menu_group_blacklist">
                        <a href="/transaction/all">
                            <i class="material-icons">account_balance</i>
                            <span>Transaction</span>
                        </a>
                    </li>
                    <hr style="margin: 0;" />
                    <li class="slide_menu_group" id="slide_menu_group_blacklist">
                        <a href="/sms/price">
                            <i class="material-icons">shopping_cart</i>
                            <span>Buy More Credits</span>
                        </a>
                    </li>
                    <hr style="margin: 0;" />
                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    &copy; 2017 <a href="javascript:void(0);"><?=PRODUCTNAME?></a>
                </div>
                <div class="version">
                    All Rights Reserved.
                </div>
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
    </section>
    <!-- #END# Left Sidebar -->
</section>

<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="body">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" <?=($this->tab == 1)?"class='active'":"" ?>>
                                    <a href="#home_with_icon_title" data-toggle="tab">
                                        <i class="material-icons">home</i> BASIC DETAILS
                                    </a>
                                </li>
                                <li role="presentation" <?=($this->tab == 2)?"class='active'":"" ?>>
                                    <a href="#profile_with_icon_title" data-toggle="tab">
                                        <i class="material-icons">face</i> CHANGE PASSWORD
                                    </a>
                                </li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane fade in active" id="home_with_icon_title">
                                    <form id="sign_up" method="POST">
                                        <?php include("include/message.php"); ?>
                                        <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                                            <div class="form-line">
                                                <input type="text" class="form-control" id="full_name" maxlength="100" placeholder="Full Name" value="<?=$this->user->full_name?>" required>
                                            </div>
                                        </div>
                                        <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">email</i>
                        </span>
                                            <div class="form-line">
                                                <input type="email" class="form-control" id="email" maxlength="100" placeholder="Email Address" value="<?=$this->user->email?>" disabled>
                                            </div>
                                        </div>
                                        <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">phone_iphone</i>
                        </span>
                                            <div class="form-line">
                                                <input type="number" class="form-control" id="mobile" maxlength="10" minlength="10" placeholder="Mobile Number" value="<?=$this->user->mobile?>" disabled>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-4">
                                                <button class="btn btn-block btn-lg bg-pink waves-effect" type="button" id="btnUpdateProfile" onclick="updateProfile()">UPDATE</button>
                                            </div>
                                            <div class="col-lg-8"></div>
                                        </div>
                                    </form>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="profile_with_icon_title">
                                    <form id="sign_up" method="POST">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">lock</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="password" class="form-control" name="old_password" id="old_password" minlength="6" maxlength="50" placeholder="Enter Old Password" required>
                                            </div>
                                        </div>
                                        <div class="input-group">
                                                    <span class="input-group-addon">
                                                    <i class="material-icons">lock</i>
                                                    </span>
                                            <div class="form-line">
                                                <input type="password" class="form-control" name="password" id="password" minlength="6" maxlength="50" placeholder="Enter New Password" required>
                                            </div>
                                        </div>
                                        <div class="input-group">
                                                    <span class="input-group-addon">
                                                    <i class="material-icons">lock</i>
                                                    </span>
                                            <div class="form-line">
                                                <input type="password" class="form-control" name="confirm" id="confirm" minlength="6" maxlength="50" placeholder="Confirm New Password" required>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <button class="btn btn-block btn-lg bg-pink waves-effect" type="button" id="btnChangePassword" onclick="changePassword()">CHANGE PASSWORD</button>
                                            </div>
                                            <div class="col-lg-8"></div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                API Key
                            </h2>
                        </div>
                        <div class="body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <?php
                                    if($this->user->apikey == "" || $this->user->apikey == null) {
                                        echo '<a href="/user/GenerateAPIKey" class="btn btn-block btn-lg bg-blue waves-effect">Click here to generate API Key</a>';
                                    } else {
                                        ?>
                                        <div class="row">
                                            <div class="col-lg-8">
                                                <div class="input-group">
                                                    <div class="form-line">
                                                        <input type="text" class="form-control" id="apikeyfield" name="apikeyfield" maxlength="100" placeholder="" value="<?=$this->user->apikey ?>" disabled>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <button type="button" class="btn btn-default waves-effect" onclick="copyAPIKey()">
                                                    <i class="material-icons">save</i>
                                                </button>
                                            </div>
                                        </div>

                                        <?php
                                    }
                                    ?>
                                </div>
                                <div class="col-lg-6"></div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <a href="/contact/DownloadAPIDoc" class="btn btn-block btn-lg bg-orange waves-effect">Download API Document</a>
                                </div>
                                <div class="col-lg-6"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Jquery Core Js -->
<script src="/assets/plugins/jquery/jquery.min.js"></script>

<!-- Bootstrap Core Js -->
<script src="/assets/plugins/bootstrap/js/bootstrap.js"></script>

<!-- Select Plugin Js -->
<script src="/assets/plugins/bootstrap-select/js/bootstrap-select.js"></script>

<!-- Slimscroll Plugin Js -->
<script src="/assets/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

<!-- Waves Effect Plugin Js -->
<script src="/assets/plugins/node-waves/waves.js"></script>

<!-- Custom Js -->
<script src="/assets/js/admin.js"></script>

<!-- Demo Js -->
<script src="/assets/js/demo.js"></script>

<script src="/assets/js/pages/ui/notifications.js"></script>
<!-- Bootstrap Notify Plugin Js -->
<script src="/assets/plugins/bootstrap-notify/bootstrap-notify.js"></script>

<!-- SweetAlert Plugin Js -->
<script src="/assets/plugins/sweetalert/sweetalert.min.js"></script>
<script src="/assets/js/pages/ui/dialogs.js"></script>

<script>

    function copyAPIKey() {
        var copyText = document.getElementById("apikeyfield");
        $("#apikeyfield").attr("disabled", false);
        copyText.select();
        document.execCommand("Copy");
        showSuccessMessageNotification("Copied the API key.");
        $("#apikeyfield").attr("disabled", true);
    }

    function updateProfile(){
        var full_name = $("#full_name").val();
        if(full_name == ""){
            showErrorMessageNotification("Please enter full name");
        }
        else{
            $("#btnUpdateProfile").attr("disabled", true);
            $.post( "/user/update", {full_name:full_name})
                .done(function( data ) {
                    $("#btnUpdateProfile").attr("disabled", false);
                    var response = JSON.parse(data);
                    if(response.status == 1){
                        swal({
                            title: "Update Profile",
                            text: "Profile has been updated successfully",
                            type: "success",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "OK",
                            closeOnConfirm: true
                        }, function () {

                        });
                    }
                    else{
                        swal("Failed!", response.message, "error");
                    }
                });
        }
    }

    function changePassword(){

        var oldPassword = $("#old_password").val();
        var newPassword = $("#password").val();
        var confirm = $("#confirm").val();

        if(oldPassword.length < 6){
            showErrorMessageNotification("Please enter a valid old password");
        }
        else if(newPassword.length < 6){
            showErrorMessageNotification("Please enter a valid new password");
        }
        else if(newPassword != confirm){
            showErrorMessageNotification("Confirm password do not match");
        }
        else{
            $("#btnChangePassword").attr("disabled", true);
            $.post( "/user/changepassword", {oldPassword:oldPassword, newPassword:newPassword})
                .done(function( data ) {
                    $("#btnChangePassword").attr("disabled", false);
                    var response = JSON.parse(data);
                    if(response.status == 1){
                        swal({
                            title: "Change Password!",
                            text: "Password has been changed successfully",
                            type: "success",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "OK",
                            closeOnConfirm: true
                        }, function () {
                            $("#old_password").val("");
                            $("#password").val("");
                            $("#confirm").val("");
                        });
                    }
                    else{
                        swal("Failed!", response.message, "error");
                    }
                });
        }

    }

    function showSuccessMessageNotification(message){
        showNotification("bg-green", message, "top", "center", "animated bounceInUp", "animated bounceOutUp");
    }

    function showErrorMessageNotification(message){
        showNotification("bg-orange", message, "top", "center", "animated bounceInUp", "animated bounceOutUp");
    }

</script>

</body>

</html>
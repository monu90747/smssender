<div class="col-lg-6" style="padding-right: 5px;">
    <div class="info-box bg-teal hover-expand-effect" style="margin-top: -20px;">
        <div class="icon">
            <i class="material-icons">message</i>
        </div>
        <div class="content">
            <div class="text">SMS CREDIT</div>
            <div id="sms_credit_balance" class="number" style="margin-top: 0;"><?=$this->user->balance->transactional ?></div>
        </div>
    </div>

    <div class="card" style="margin-top: -20px;">
        <div class="header">
            <div class="col-lg-6" style="margin-top: -6px">
                <h2>SEND SMS</h2>
            </div>
        </div>
        <div class="body" style="padding: 5px;">
            <form class="form-horizontal" method="post">

                <div class="row clearfix" style="margin-left: 0; margin-top: 10px;">
                    <div class="col-lg-12" style="padding-right: 25px;">
                        <select id="sender_id" class="form-control show-tick" title="Select Sender">
                            <option value="">Select Sender</option>
                            <?php
                            foreach($this->senders as $sender){
                                echo '<option value="'.$sender["sender_id"].'">'.$sender["sender_id"].'</option>';
                            }

                            ?>
                        </select>
                    </div>
                </div>

                <div class="row clearfix" style="margin-left: 10px;">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 7px">
                        <label id="selected_groups"></label>
                    </div>
                </div>

                <div class="row clearfix" style="margin-left: 20px;">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <div class="form-line">
                                <textarea rows="2" id="mobile_numbers" class="form-control no-resize" style="border: solid;border-width: 1px;padding: 5px;border-color: lightgray;" placeholder="Enter mobile numbers. eg. XXXXXXXXXX, XXXXXXXXXX"></textarea>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row clearfix" style="margin-left: 20px;">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-3" style="margin-bottom: 0;padding-bottom: 0;">
                                    <select name="unicode" id="unicode" class="form-control show-tick">
                                        <option value="0">English</option>
                                        <option value="1">Unicode</option>
                                    </select>
                                </div>
                                <div class="col-lg-9" style="text-align: right; padding-top: 10px; padding-right: 25px; margin-bottom: 0;padding-bottom: 0;">
                                    <div id="message_counter"><label style='color: green'>(0/0)</label></div>
                                </div>
                            </div>
                            <div class="form-line">
                                <textarea id="message_box" rows="5" class="form-control no-resize" style="border: solid;border-width: 1px;padding: 5px;border-color: lightgray;" placeholder="Enter message text"></textarea>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row clearfix" style="margin-left: 20px; margin-right: 0;">
                    <div class="col-sm-12" style="text-align: right">
                        <button type="button" class="btn bg-orange waves-effect">SCHEDULE</button>&nbsp;&nbsp;
                        <button type="button" class="btn bg-pink waves-effect" id="sendSMSNow">SEND NOW</button>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>

<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="margin-top: -20px;">
    <div class="card">
        <div class="body">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist" style="margin-top: -20px">
                <li role="presentation" class="active">
                    <a href="#home_with_icon_title" data-toggle="tab">
                        <i class="material-icons">group</i> GROUPS
                    </a>
                </li>
                <li role="presentation">
                    <a href="#profile_with_icon_title" data-toggle="tab">
                        <i class="material-icons">contact_phone</i> CONTACTS
                    </a>
                </li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade in active" id="home_with_icon_title">
                    <div class="body table-responsive" style="padding: 5px;">
                        <table class="table table-hover">
                            <tbody>
                            <?php
                            foreach($this->groups as $group){
                                ?>
                                <tr>
                                    <td style="padding: 0; text-align: center; padding-top: 10px; width: 60px;">
                                        <input name="contactGroupCheck" type="checkbox" id="md_group_<?=$group["id"]?>" value="<?=$group["id"]?>" data="<?=$group["group_name"]?>" class="filled-in chk-col-teal contactGroupCheck" />
                                        <label for="md_group_<?=$group["id"]?>"></label>
                                    </td>
                                    <td><?=$group["group_name"]?></td>
                                </tr>
                            <?php
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="profile_with_icon_title">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th style="padding: 0; text-align: center; padding-top: 10px;">
                            </th>
                            <th>MOBILE</th>
                            <th>NAME</th>
                            <th>REFERENCE</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach($this->contacts as $contact){
                            ?>
                            <tr>
                                <td style="padding: 0; text-align: center; padding-top: 10px;">
                                    <input name="contactNumberCheck" type="checkbox" id="md_checkbox_<?=$contact["id"]?>" class="filled-in chk-col-teal contactNumberCheck" data="<?=$contact["mobile"]?>"/>
                                    <label for="md_checkbox_<?=$contact["id"]?>"></label>
                                </td>
                                <td><?=$contact["mobile"]?></td>
                                <td><?=$contact["name"]?></td>
                                <td><?=$contact["reference"]?></td>
                            </tr>
                        <?php
                        }
                        ?>
                        </tbody>
                    </table>
                    <nav>
                        <ul class="pagination">
                            <?php
                            if($this->current_page == 1){
                                echo '<li class="disabled"><a href="javascript:void(0);"><i class="material-icons">chevron_left</i></a></li>';
                            }
                            else{
                                echo '<li><a href="/?index='.($this->current_page-1).'"><i class="material-icons">chevron_left</i></a></li>';
                            }

                            for($page=1; $page<=$this->total_page; $page++){
                                if($page == $this->current_page){
                                    echo '<li class="active"><a href="javascript:void(0);">'.$page.'</a></li>';
                                }
                                else{
                                    echo '<li><a href="javascript:void(0);" class="waves-effect">'.$page.'</a></li>';
                                }
                            }

                            if($this->current_page == $this->total_page){
                                echo '<li class="disabled"><a href="javascript:void(0);"><i class="material-icons">chevron_right</i></a></li>';
                            }
                            else{
                                echo '<li><a href="/?index='.($this->current_page+1).'"><i class="material-icons">chevron_right</i></a></li>';
                            }
                            ?>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){

        function removeItem(array, item){
            for(var i in array){
                if(array[i]==item){
                    array.splice(i,1);
                    break;
                }
            }
        }

        var groupCheckboxValues = "";
        var groupsName = "";

        $("input:checkbox.contactGroupCheck").click(function() {
            if($(this).is(":checked")){
                groupCheckboxValues = groupCheckboxValues + (groupCheckboxValues==""?"":",") + $(this).val();
                groupsName = groupsName + (groupsName==""?"":",") + $(this).attr("data");
                $("#selected_groups").html(groupsName);
            }
            else {
                var groups = groupsName.split(',');
                removeItem(groups, $(this).attr("data"));
                groupsName = groups.join(",");
                $("#selected_groups").html(groupsName);
            }
        });

        $("input:checkbox.contactNumberCheck").click(function() {
            if($(this).is(":checked")){
                var mobile_numbers = $("#mobile_numbers").val();
                $("#mobile_numbers").val(mobile_numbers+(mobile_numbers==""?"":",")+$(this).attr("data"));
            }
            else {
                var mobile_numbers = $("#mobile_numbers").val();
                var mobileNumbers = mobile_numbers.split(',');
                removeItem(mobileNumbers, $(this).attr("data"));
                $("#mobile_numbers").val(mobileNumbers.join(","));
            }
        });

        $('#message_box').keyup(function(){
            var message = $('#message_box').val();
            var count = message.length;
            var totalSMS = Math.ceil(count / 160);

            if(totalSMS <= 1){
                $("#message_counter").html("<label style='color: green'>("+ count + "/" + totalSMS +")</label>");
            }
            else{
                $("#message_counter").html("<label style='color: red'>("+ count + "/" + totalSMS +")</label>");
            }
        });

        $('#mobile_numbers').keypress(function(e){
            var a = [44];
            var k = e.which;
            for (i = 48; i < 58; i++)
                a.push(i);
            if (!(a.indexOf(k)>=0))
                e.preventDefault();

        }).keyup(function(){
            var numbers = $('#mobile_numbers');
            if(numbers.val() == ","){
                numbers.val("");
            }
            else if(numbers.val().indexOf(",,") >= 0){
                numbers.val(numbers.val().replace(",,", ","));
            }

            numbers.val(numbers.val().replace(/[A-Za-z$-$+]/g, ""));
        });

        var selected_groups = [];

        $("#sendSMSNow").click(function(){
            var route = 4;
            var sender_id = $("#sender_id").val();
            var mobile_numbers = $("#mobile_numbers").val();
            var message_box = $("#message_box").val();
            var campaign = $("#campaign").val();
            var unicode = $("#unicode").val();

            if(mobile_numbers.length <= 9 && groupCheckboxValues == ""){
                showNotification("bg-orange", "Please enter valid mobile number", "top", "right", "animated bounceInUp", "animated bounceOutUp");
            }
            else if(message_box == ""){
                showNotification("bg-orange", "Please enter message text", "top", "right", "animated bounceInUp", "animated bounceOutUp");
            }
            else{
                $("#sendSMSNow").attr("disabled", true);

                var json = { groups:groupCheckboxValues, unicode:unicode, route: route, sender_id:sender_id, mobile_numbers:mobile_numbers, message:message_box, campaign:campaign, schedule:"" };

                $.post( "/sms/send", json)
                    .done(function( data ) {
                        $("#sendSMSNow").attr("disabled", false);
                        console.log(data);
                        var response = JSON.parse(data);
                        if(response.status == 1){
                            $("#sms_credit_balance").html(response.data.balance);
                            $("#mobile_numbers").val("");
                            $("#message_box").val("");
                            $("#selected_groups").html("");

                            groupCheckboxValues = "";
                            groupsName = "";

                            $('input[name=contactNumberCheck]:checked').map(function() {
                                $(this).attr('checked', false);
                            });

                            $('input[name=contactGroupCheck]:checked').map(function() {
                                $(this).attr('checked', false);
                            });

                            showNotification("bg-green", response.message, "top", "right", "animated bounceInUp", "animated bounceOutUp");
                        }
                        else{
                            showNotification("bg-orange", response.message, "top", "right", "animated bounceInUp", "animated bounceOutUp");
                        }
                    });
            }
        });

    });
</script>
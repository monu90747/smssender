<div class="body table-responsive" style="padding: 5px;">
    <?php
    if(count($this->contacts) <= 0){
        echo '<div class="header" style="text-align: center; padding: 50px;">';
        echo '<i class="material-icons" style="font-size: 90px;">contact_phone</i>';
        echo'<h1>No Contact!</h1>';
        echo'<h2><a href="javascript:void(0);" data-toggle="modal" data-target="#modelAddContact">Click here to add new contact</a></h2></div>';
    }
    else{
        ?>
        <table class="table table-hover">
            <thead>
            <tr>
                <th style="padding: 0; text-align: center; padding-top: 10px;">
                </th>
                <th>MOBILE</th>
                <th>NAME</th>
                <th>REFERENCE</th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach($this->contacts as $contact){
                ?>
                <tr>
                    <td style="padding: 0; text-align: center; padding-top: 10px;">
                        <input name="contactNumberSelect" type="checkbox" id="md_checkbox_<?=$contact["id"]?>" class="filled-in chk-col-teal contactNumberSelect" data="<?=$contact["mobile"]?>" data_id="<?=$contact["id"]?>"/>
                        <label for="md_checkbox_<?=$contact["id"]?>"></label>
                    </td>
                    <td><?=$contact["mobile"]?></td>
                    <td><?=$contact["name"]?></td>
                    <td><?=$contact["reference"]?></td>
                </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
        <nav>
            <ul class="pagination">
                <?php
                if($this->current_page == 1){
                    echo '<li class="disabled"><a href="javascript:void(0);"><i class="material-icons">chevron_left</i></a></li>';
                }
                else{
                    echo '<li><a href="javascript:void(0);" onclick="loadPaging('.($this->current_page-1).');"><i class="material-icons">chevron_left</i></a></li>';
                }

                if($this->total_page <= 6){
                    for($page=1; $page<=$this->total_page; $page++){
                        if($page == $this->current_page){
                            echo '<li class="active"><a href="javascript:void(0);">'.$page.'</a></li>';
                        }
                        else{
                            echo '<li><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging('.$page.');">'.$page.'</a></li>';
                        }
                    }
                }
                else{
                    if($this->current_page == 1){
                        echo '<li class="active"><a href="javascript:void(0);">1</a></li>';
                        echo '<li><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging(2);">2</a></li>';
                        echo '<li><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging(3);">3</a></li>';
                        echo '<li><a href="javascript:void(0);" class="waves-effect">...</a></li>';
                        echo '<li><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging('.$this->total_page.');">'.$this->total_page.'</a></li>';
                    }
                    elseif($this->current_page == $this->total_page){
                        echo '<li><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging(1);">1</a></li>';
                        echo '<li><a href="javascript:void(0);" class="waves-effect">...</a></li>';
                        echo '<li><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging('.($this->total_page-2).');">'.($this->total_page-2).'</a></li>';
                        echo '<li><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging('.($this->total_page-1).');">'.($this->total_page-1).'</a></li>';
                        echo '<li class="active"><a href="javascript:void(0);">'.$this->total_page.'</a></li>';
                    }
                    elseif($this->current_page == 2){
                        echo '<li><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging(1);">1</a></li>';
                        echo '<li class="active"><a href="javascript:void(0);">2</a></li>';
                        echo '<li><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging(3);">3</a></li>';
                        echo '<li><a href="javascript:void(0);" class="waves-effect">...</a></li>';
                        echo '<li><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging('.$this->total_page.');">'.$this->total_page.'</a></li>';
                    }
                    elseif($this->current_page == $this->total_page-1){
                        echo '<li><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging(1);">1</a></li>';
                        echo '<li><a href="javascript:void(0);" class="waves-effect">...</a></li>';
                        echo '<li><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging('.($this->total_page-2).');">'.($this->total_page-2).'</a></li>';
                        echo '<li class="active"><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging('.($this->total_page-1).');">'.($this->total_page-1).'</a></li>';
                        echo '<li><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging('.($this->total_page).');">'.($this->total_page).'</a></li>';
                    }
                    else{
                        echo '<li><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging(1);">1</a></li>';
                        if($this->current_page-2 != 1){
                            echo '<li><a href="javascript:void(0);" class="waves-effect">...</a></li>';
                        }

                        echo '<li><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging('.($this->current_page-1).');">'.($this->current_page-1).'</a></li>';
                        echo '<li class="active"><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging('.($this->current_page).');">'.($this->current_page).'</a></li>';
                        echo '<li><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging('.($this->current_page+1).');">'.($this->current_page+1).'</a></li>';

                        if($this->current_page+2 != $this->total_page){
                            echo '<li><a href="javascript:void(0);" class="waves-effect">...</a></li>';
                        }
                        echo '<li><a href="javascript:void(0);" class="waves-effect" onclick="loadPaging('.($this->total_page).');">'.($this->total_page).'</a></li>';
                    }
                }

                if($this->current_page == $this->total_page){
                    echo '<li class="disabled"><a href="javascript:void(0);"><i class="material-icons">chevron_right</i></a></li>';
                }
                else{
                    echo '<li><a href="javascript:void(0);" onclick="loadPaging('.($this->current_page+1).');"><i class="material-icons">chevron_right</i></a></li>';
                }
                ?>
            </ul>
        </nav>
        <?php
    }
    ?>
</div>
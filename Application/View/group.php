<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title><?=PRODUCTNAME?></title>
    <!-- Favicon-->
    <link rel="icon" href="/assets/logo-small.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="/assets/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="/assets/plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="/assets/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="/assets/css/style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="/assets/css/themes/all-themes.css" rel="stylesheet" />
</head>

<body class="theme-red">
<!-- Page Loader -->
<?php include "include/pageloader.php"; ?>
<!-- #END# Page Loader -->
<!-- Overlay For Sidebars -->
<div class="overlay"></div>
<!-- #END# Overlay For Sidebars -->

<!-- Top Bar -->
<?php include("include/header.php") ?>
<!-- #Top Bar -->
<section>
    <!-- Left Sidebar -->
    <?php include("include/leftmenu.php"); ?>
    <!-- #END# Left Sidebar -->
</section>

<section class="content">
    <div class="container-fluid">
        <div class="block-header">

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0px; margin: 0px;">
                    <div class="card" style="margin-top: -20px;">
                        <div class="header">
                            <h2>
                                ALL GROUPS
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <button type="button" class="btn bg-pink waves-effect" data-toggle="modal" data-target="#defaultModal">Add New Group</button>
                                </li>
                            </ul>
                        </div>
                        <div class="body table-responsive" style="padding: 5px;">
                            <table class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th style="text-align: center">#</th>
                                    <th>Name</th>
                                    <th>Contacts</th>
                                    <th class="text-center">ACTION</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $index = 0;
                                foreach($this->groups as $group){
                                    $index++;
                                    echo '<tr>';
                                    echo '<td style="text-align: center">'.$index.'</td>';
                                    echo '<td>'.$group["group_name"].'</td>';
                                    echo '<td>'.$group["count"].'</td>';
                                    echo '<td class="text-center">
                                        <button type="button" class="btn bg-green waves-effect btn-xs" data-toggle="tooltip" data-placement="top" title="View"><i class="material-icons">visibility</i></button>&nbsp;&nbsp;
                                        <button type="button" class="btn bg-blue waves-effect btn-xs" data-toggle="tooltip" data-placement="top" title="Edit"><i class="material-icons">edit</i></button>&nbsp;&nbsp;
                                        <button type="button" class="btn bg-red waves-effect btn-xs" data-toggle="tooltip" data-placement="top" title="Delete"><i class="material-icons">delete</i></button>
                                    </td>';
                                    echo '</tr>';
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="defaultModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">ADD NEW GROUP</h4>
            </div>
            <hr/>
            <div class="modal-body">
                <div class="row clearfix">
                    <div class="col-lg-3 col-md-2 col-sm-4 col-xs-5 form-control-label">
                        <label for="email_address_2">Group Name *</label>
                    </div>
                    <div class="col-lg-9 col-md-10 col-sm-8 col-xs-7">
                        <div class="form-group">
                            <div class="form-line">
                                <input type="text" id="group_name" class="form-control" placeholder="Enter group name">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr/>
            <div class="modal-footer">
                <div class="col-lg-8">
                    <div id="error_message" style="color:red; text-align: center"></div>
                </div>
                <div class="col-lg-4">
                    <button type="button" id="addGroup" class="btn btn-primary waves-effect">SAVE</button>
                    <button type="button" class="btn btn-warning waves-effect" data-dismiss="modal">CLOSE</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Jquery Core Js -->
<script src="/assets/plugins/jquery/jquery.min.js"></script>

<!-- Bootstrap Core Js -->
<script src="/assets/plugins/bootstrap/js/bootstrap.js"></script>

<!-- Select Plugin Js -->
<script src="/assets/plugins/bootstrap-select/js/bootstrap-select.js"></script>

<!-- Slimscroll Plugin Js -->
<script src="/assets/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

<!-- Waves Effect Plugin Js -->
<script src="/assets/plugins/node-waves/waves.js"></script>

<script src="/assets/js/pages/ui/tooltips-popovers.js"></script>

<!-- Custom Js -->
<script src="/assets/js/admin.js"></script>

<!-- Demo Js -->
<script src="/assets/js/demo.js"></script>

<script src="/assets/js/group.js"></script>

</body>

</html>
<nav class="navbar">
    <div class="container-fluid">
        <div class="navbar-header">
            <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
            <a href="javascript:void(0);" class="bars"></a>
            <a class="navbar-brand" href="/"><?=PRODUCTNAME?></a>
        </div>
        <div class="collapse navbar-collapse" id="navbar-collapse">
            <ul class="nav navbar-nav navbar-right">

                <li><a href="/sms/send" data-toggle="tooltip" data-placement="bottom"  title="Send SMS"><i class="material-icons">send</i></a></li>
                <li><a href="/transaction/deliveryreport" data-toggle="tooltip" data-placement="bottom"  title="Delivery Report"><i class="material-icons">report</i></a></li>

                <li class="dropdown">
                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                        <i class="material-icons">settings</i>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="body" style="height: 135px;">
                            <ul class="menu">
                                <li>
                                    <a href="/user/profile">
                                        <span>PROFILE</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="/transaction/all">
                                        <span>TRANSACTION</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="/user/changepassword">
                                        <span>CHANGE PASSWORD</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="/user/logout">
                                        <span>LOGOUT</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Sign In | SMS</title>
    <!-- Favicon-->
    <link rel="icon" href="/assets/favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="/assets/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="/assets/plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="/assets/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="/assets/css/style.css" rel="stylesheet">
</head>

<body class="login-page">
<div class="login-box">
    <div class="logo">
        <a href="javascript:void(0);"><?=PRODUCTNAME ?></a>
        <small>You have successfully registered</small>
    </div>

    <div class="card">
        <div class="header bg-teal">
            <h2>
                Reset Your Password
            </h2>
        </div>
        <form method="POST" action="/auth/VerifyResetPassword">
            <div class="body">
                One Time Password (OTP) has been sent to your mobile <?=$_POST["mobile"]?>, please enter the same here to login.
                <br><br>
                <div class="input-group">
                    <div class="form-line">
                        <input type="text" class="form-control" name="otp" maxlength="6" placeholder="One Time Password (OTP)" required>
                    </div>
                </div>
                <div class="input-group">
                    <div class="form-line">
                        <input type="password" class="form-control" name="password" placeholder="Enter New Password" required>
                    </div>
                </div>
                <input type="hidden" name="mobile" value="<?=$this->mobile ?>">
                <?=($this->message != "") ? "<div style='color: red'>".$this->message."</div><br><br>" : "" ?>
                <button class="btn btn-block bg-light-blue waves-effect btn-lg" type="submit">Verify</button>
            </div>
        </form>
    </div>

</div>

<!-- Jquery Core Js -->
<script src="/assets/plugins/jquery/jquery.min.js"></script>

<!-- Bootstrap Core Js -->
<script src="/assets/plugins/bootstrap/js/bootstrap.js"></script>

<!-- Waves Effect Plugin Js -->
<script src="/assets/plugins/node-waves/waves.js"></script>

<!-- Validation Plugin Js -->
<script src="/assets/plugins/jquery-validation/jquery.validate.js"></script>

<!-- Custom Js -->
<script src="/assets/js/admin.js"></script>
<script src="/assets/js/pages/examples/sign-in.js"></script>
</body>

</html>
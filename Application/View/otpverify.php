<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Sign In | SMS</title>
    <!-- Favicon-->
    <link rel="icon" href="/assets/favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="/assets/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="/assets/plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="/assets/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="/assets/css/style.css" rel="stylesheet">
</head>

<body class="login-page">
<div class="login-box">
    <div class="logo">
        <a href="javascript:void(0);"><?=PRODUCTNAME ?></a>
        <small>You have successfully registered</small>
    </div>

    <div class="card">
        <div class="header bg-teal">
            <h2>
                Congratulation!!!
            </h2>
        </div>
        <form method="POST" action="/auth/VerifyOtp">
            <div class="body">
                Your account has been successfully created. You need to verify your mobile number to access our services.</b>
                <br><br>
                One Time Password (OTP) has been sent to your mobile <?=$_POST["mobile"]?>, please enter the same here to login.
                <br><br>
                <div class="input-group">
                    <div class="form-line">
                        <input type="text" class="form-control" name="otp" maxlength="6" placeholder="One Time Password (OTP)" required>
                    </div>
                </div>
                <input type="hidden" name="mobile" value="<?=$this->mobile ?>">
                <?=($this->message != "") ? "<div style='color: red'>".$this->message."</div><br><br>" : "" ?>
                <button class="btn btn-block bg-light-blue waves-effect btn-lg" type="submit">Verify</button><br><br>
                <div class="resend_button" id="resend_button">
                    You have not received OTP yet, <a href="#" onclick="resendOTP();">Click here to Resend OTP</a>
                </div>
            </div>
        </form>
    </div>

</div>

<!-- Jquery Core Js -->
<script src="/assets/plugins/jquery/jquery.min.js"></script>

<!-- Bootstrap Core Js -->
<script src="/assets/plugins/bootstrap/js/bootstrap.js"></script>

<!-- Waves Effect Plugin Js -->
<script src="/assets/plugins/node-waves/waves.js"></script>

<!-- Validation Plugin Js -->
<script src="/assets/plugins/jquery-validation/jquery.validate.js"></script>

<!-- Custom Js -->
<script src="/assets/js/admin.js"></script>
<script src="/assets/js/pages/examples/sign-in.js"></script>
</body>

<script>

function resendOTP() {
    var mobile = "<?=$_POST["mobile"]?>";
    $("#resend_button").html("");
    $.post( "/otp/resend", {mobile: mobile})
        .done(function( data ) {
            var response = JSON.parse(data);
            if(response.status == 1){
                $("#resend_button").html('<div class="alert alert-success">' + response.message + '</div>' + 'You have not received OTP yet, <a href="#" onclick="resendOTP();">Click here to Resend OTP</a>');
            }
            else{
                $("#resend_button").html('<div class="alert alert-danger">' + response.message + '</div>' + 'You have not received OTP yet, <a href="#" onclick="resendOTP();">Click here to Resend OTP</a>');
            }
        });
}

</script>

</html>
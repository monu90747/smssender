<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Sign Up | <?=PRODUCTNAME ?></title>
    <!-- Favicon-->
    <link rel="icon" href="/assets/logo-small.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="/assets/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="/assets/plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="/assets/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="/assets/css/style.css" rel="stylesheet">
</head>

<body class="login-page">
<div class="login-box">
    <div class="logo">
        <a href="javascript:void(0);"><?=PRODUCTNAME ?></a>
        <small>You have successfully registered</small>
    </div>

    <div class="card">
        <div class="header bg-teal">
            <h2>
                Email Verification
            </h2>
        </div>
        <form method="POST" action="/auth/signupsuccess">
            <div class="body">
                Your account has been successfully created. You need to verify your email address. Please open your email inbox and verify your email to sign in <b><?=PRODUCTNAME?></b>.
                <br><br>We may need to send you critical information about our service and it is important that we have an accurate email address. <a href="/auth/login">Clicked Here to sign in</a>
                <br><br>
                <input type="hidden" name="email" value="<?=$_POST["email"]?>">
                <button class="btn btn-block bg-light-blue waves-effect btn-lg" type="submit">Resend Verify Email Link</button>
            </div>
        </form>
    </div>

</div>

<!-- Jquery Core Js -->
<script src="/assets/plugins/jquery/jquery.min.js"></script>

<!-- Bootstrap Core Js -->
<script src="/assets/plugins/bootstrap/js/bootstrap.js"></script>

<!-- Waves Effect Plugin Js -->
<script src="/assets/plugins/node-waves/waves.js"></script>

<!-- Validation Plugin Js -->
<script src="/assets/plugins/jquery-validation/jquery.validate.js"></script>

<!-- Custom Js -->
<script src="/assets/js/admin.js"></script>
<script src="/assets/js/pages/examples/sign-in.js"></script>
</body>

</html>
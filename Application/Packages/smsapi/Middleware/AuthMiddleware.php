<?php
namespace Application\Packages\smsapi\Middleware;

use MRPHPSDK\MRMiddleware\MRMiddleware;
use MRPHPSDK\MRRequest\MRRequest;
use MRPHPSDK\MRVendor\MRAccessToken\MRAccessToken;
use MRPHPSDK\MRException\MRException;

class AuthMiddleware extends MRMiddleware{

	/*
	|--------------------------------------------------------------------------
	| Handle all request
	|--------------------------------------------------------------------------
	*/
	public function handle(MRRequest $request){

		try{
            MRAccessToken::setAuthClass("\\Application\\Model\\User");
            $token = MRAccessToken::validateApiKey();
        }
        catch(MRException $e){
            return $this->failed($e->getMessage(), $e->getStatusCode());
        }

        return $this->success();
	}

}
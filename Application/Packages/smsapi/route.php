<?php

use \MRPHPSDK\MRRoute\Route;

Route::group([
	'package' => 'smsapi',
	'prefix' => 'smsapi',
	'middleware' => ['/Application/Packages/smsapi/Middleware/AuthMiddleware'],
], function(){
	Route::controller("v1", "APIController");
});
<?php
namespace Application\Packages\smsapi\Controller;

use MRPHPSDK\MRController\MRController;
use MRPHPSDK\MRRequest\MRRequest;

class IndexController extends MRController{

	function __construct(){
		parent::__construct();
	}

	public function index(MRRequest $request){

		return phpinfo();
	}

}
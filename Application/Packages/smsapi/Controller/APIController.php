<?php
namespace Application\Packages\smsapi\Controller;

use MRPHPSDK\MRController\MRController;
use MRPHPSDK\MRRequest\MRRequest;
use MRPHPSDK\MRValidation\MRValidation;
use Application\Model\Sender;
use Application\Services\SMSService;
use MRPHPSDK\MRVendor\MRAccessToken\MRAccessToken;
use Application\Model\Response;
use Application\Model\Balance;

class APIController extends MRController{

	function __construct(){
		parent::__construct();
	}

	public function postSendSMS(MRRequest $request){
		$validation = new MRValidation($request->input(), [
			'mobile_numbers' => 'required',
			'message' => 'required',
			'sender_id'=> 'required'
        ], [ ]);

        if($validation->validateFailed()) {
            return Response::toJson([], 0, $validation->getValidationError()[0]);
		}

		$unicode = $request->input("unicode");
		if($unicode == "" || $unicode != 1){
			$unicode = 0;
		}
		
		$user = MRAccessToken::getAuth();
		$sender = Sender::where("user_id", $user->id)->where("sender_id", $request->input("sender_id"))->first();
		if($sender == null){
			return Response::toJson([], 0, "Invalid sender id");
		}

		$totalCount = mb_strlen($request->input("message"), 'UTF-8');
		$totalSMSSize = 1;
		if($unicode == "0") {
			$totalSMSSize = ceil($totalCount/160);
		}
		else{
			$totalSMSSize = ceil($totalCount/67);
		}

		$params = $request->input();
		$params["route"] = "4";
		$params["mode"] = "API";
		$params["unicode"] = $unicode;
		return json_encode(SMSService::send($params, $user));		

	}

	public function getBalance(MRRequest $request){
		$user = MRAccessToken::getAuth();
		$balance = Balance::where("user_id", $user->id)->first();
		return json_encode(["status"=>1, "balance"=>$balance->transactional]);
	}

}
<?php
namespace Application\Packages\studentapi\Controller;

use MRPHPSDK\MRController\MRController;
use MRPHPSDK\MRRequest\MRRequest;
use Application\Packages\studentapi\Services\TestService;
use Application\Model\Response;

class TestController extends MRController{

	function __construct(){
		parent::__construct();
	}

	public function postAll(MRRequest $request){
        $response = TestService::all($request->input(), $this->user->id, $this->user);
        return $response;
	}
    
    public function postSubject(MRRequest $request){
        $response = TestService::getTest($request->input("test_id"));
        return $response;
    }
    
    public function postAnswers(MRRequest $request){
        $response = TestService::getTest($request->input("test_id"), true);
        return $response;
    }
    
    public function postAttempted(MRRequest $request){
        $response = TestService::getAttempted($this->user->id);
        return $response;
    }
    
    public function postSubmit(MRRequest $request){
        $response = TestService::submit($request->input(), $this->user->id);
        return $response;
    }
    
    public function postReport(MRRequest $request){
        $response = TestService::fetchReport($this->user->id);
        return Response::json($response);
    }
    
}
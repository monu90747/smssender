<?php
namespace Application\Packages\studentapi\Controller;

use Application\Model\Response;
use Application\Packages\student\Model\Student;
use Application\Packages\studentapi\Model\MonthRanking;
use Application\Packages\studentapi\Model\Report;
use Application\Packages\studentapi\Model\StudentRanking;
use Application\Packages\studentapi\Model\Test;
use Application\Packages\studentapi\Services\TestService;
use MRPHPSDK\MRController\MRController;
use MRPHPSDK\MRDatabase\MRDatabase;
use MRPHPSDK\MRRequest\MRRequest;

class RankingController extends MRController{

	function __construct(){
		parent::__construct();
	}

	public function getSubject(MRRequest $request) {
		return TestService::fetchRanking($request->input());
	}

	public function getRemoveDuplicate(MRRequest $request) {
		$deletedUser = TestService::removeDuplicateReport();
		return Response::toJson(["count"=>count($deletedUser), "users"=>$deletedUser], 1, "Remove duplicate reports.");
	}

	public function getMonth(MRRequest $request) {
		$previousMonth = date('Y-m', strtotime(date('Y-m') . " -1 month"));
		$monthRanking = MonthRanking::where("month", $previousMonth)->first();
		if($monthRanking == null) {
			return Response::toJson(["ranking"=>[]], 0, "Ranking not generated yet.");
		}

		$page = 1;
		if($request->input("page") != "") {
			$page = $request->input("page");
		}

		$fields = ["Student.full_name as name", "picture", "Student.mobile as mobile", "Student.roll_no as rollNumber", "StudentRanking.studentId", "StudentRanking.rank", "StudentRanking.totalAttempts", "StudentRanking.marks", "StudentRanking.timeTaken", "StudentRanking.totalTest", "StudentRanking.created_at as createdAt", "MonthRanking.month", "MonthRanking.totalQuestion", "MonthRanking.subjectCount"];
		$studentRanking = StudentRanking::where("monthRankingId", $monthRanking->id)->leftJoin("Student", "id", "studentId")->leftJoin("MonthRanking", "id", "monthRankingId")->selectField($fields)->page($page, 20)->orderBy("rank", "ASC")->get();
		$paging = StudentRanking::paging("monthRankingId=".$monthRanking->id, $page, 20);

		$student = StudentRanking::where("monthRankingId", $monthRanking->id)->where("StudentRanking.studentId", $this->user->id)->leftJoin("Student", "id", "studentId")->leftJoin("MonthRanking", "id", "monthRankingId")->selectField($fields)->first();

		return Response::toJson(["ranking"=>$studentRanking, "paging"=>$paging, "student"=>$student], 1, "Students ranking");
	}

	public function getGenerate(MRRequest $request) {
		$previousMonth = date('Y-m', strtotime(date('Y-m') . " -1 month"));
		$check = MonthRanking::where("month", $previousMonth)->first();
		if($check != null) {
			return Response::toJson([], 0, "Ranking already generated for this month.");
		}

		$query = "SELECT DISTINCT(Report.test_id) from Report LEFT JOIN Test ON Test.id=Report.test_id WHERE DATE_FORMAT(Report.created_at, '%Y-%m')='$previousMonth' AND Report.is_live=1 AND Test.institute_code='1000'";
		$results = MRDatabase::select($query);
		$monthLiveTests = [];
		foreach($results as $report) {
			$test = Test::where("id", $report["test_id"])->get()[0];
			$monthLiveTests[] = $test;
		}

		$grandTotal = 0;
		$reports = [];
		$calculatedMarks = [];
		$subjectIds = [];
		foreach($monthLiveTests as $test) {
			$subjectIds[] = $test["id"];
			$grandTotal += $test["total_question"];
			$studentReport = $this->generateTestReport($test, $previousMonth);
			$reports[] = $studentReport;
			// Calculate Monthly Marks
			foreach($studentReport as $report) {
				if(isset($calculatedMarks[$report["student_id"]])) {
					$old = $calculatedMarks[$report["student_id"]];
					$data["studentId"] = $report["student_id"];
					$data["rank"] = 0;
					$data["totalAttempts"] = $old["totalAttempts"] + $report["attampt"];
					$data["marks"] = $old["marks"] + $report["marks"];
					$data["timeTaken"] = $old["timeTaken"] + $report["time_taken"];
					$data["totalTest"] = $old["totalTest"] + 1;
					$calculatedMarks[$report["student_id"]] = $data;
				} else {
					$calculatedMarks[$report["student_id"]] = [
						"studentId" => $report["student_id"],
						"rank" => 0,
						"totalAttempts" => $report["attampt"],
						"marks" => $report["marks"],
						"timeTaken" => $report["time_taken"],
						"totalTest" => 1
					];
				}
			}
		}

		$mRanking = [
			"month" => $previousMonth,
			"totalQuestion" => $grandTotal,
			"subjectIds" => implode(",", $subjectIds),
			"subjectCount" => count($subjectIds)
		];

		$monthRanking = new MonthRanking($mRanking);
		$monthRanking->save();

		if($monthRanking->id > 0) {
			$this->saveAllReports($calculatedMarks, $monthRanking->id);
			$this->updateStudentRankingBy($monthRanking->id);
			return Response::toJson([], 1, "Success");
		} else {
			return Response::toJson([], 0, "Something went wrong");
		}
	}

	private function updateStudentRankingBy($monthRankingId) {
		$query = "SELECT (@row := @row + 1) as Ranking, id FROM StudentRanking, (SELECT @row := 0) r WHERE `monthRankingId`=$monthRankingId ORDER BY marks DESC, timeTaken ASC";
		$records = MRDatabase::select($query);
		foreach($records as $report) {
			$studentReport = StudentRanking::where("id", $report["id"])->first();
			if($studentReport != null) {
				$studentReport->rank = $report["Ranking"];
				$studentReport->save();
			}
		}
	}

	private function saveAllReports($reports, $monthRankingId) {
		foreach($reports as $key=>$report) {
			$studentReport = new StudentRanking($report);
			$studentReport->monthRankingId = $monthRankingId;
			$studentReport->save();
		}
	}

	private function reportFrom($calculatedMarks, $studentId) {
		if(isset($calculatedMarks[$studentId])) {
			return $calculatedMarks[$studentId];
		} else {
			return null;
		}
	}

	private function generateTestReport($test, $month) {
		$report = Report::where("test_id", $test["id"])->where("is_live", "1")->where("DATE_FORMAT(Report.created_at, '%Y-%m')", $month)->selectField(["id", "student_id", "test_id", "attampt", "total_que", "marks", "total_time", "time_taken"])->get();
		return $this->uniqueArray($report, "student_id");
	}

	private function uniqueArray($array, $key) {
		$unique = [];
		$uniqueItems = [];
		foreach($array as $item) {
			if(in_array($item[$key], $unique) == false) {
				$unique[] = $item[$key];
				$uniqueItems[] = $item;
			}
		}
		return $uniqueItems;
	}

}
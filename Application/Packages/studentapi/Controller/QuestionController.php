<?php
namespace Application\Packages\studentapi\Controller;

use MRPHPSDK\MRController\MRController;
use MRPHPSDK\MRRequest\MRRequest;
use Application\Packages\studentapi\Services\QuestionService;
use Application\Model\Response;

class QuestionController extends MRController{

	function __construct(){
		parent::__construct();
	}

	public function postImport(MRRequest $request){
        	return Response::json(QuestionService::import($_FILES["question"]["tmp_name"], $request->input()));
	}

	public function postImportTest(MRRequest $request){
		return Response::json(QuestionService::importTest($_FILES["testfile"]["tmp_name"]));
	}

}
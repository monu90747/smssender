<?php
namespace Application\Packages\studentapi\Controller;

use MRPHPSDK\MRController\MRController;
use MRPHPSDK\MRRequest\MRRequest;
use Application\Model\Response;
use Application\Packages\studentapi\Services\SubscriptionService;
use Application\Packages\studentapi\Services\PrepaidCardService;

class SubscriptionController extends MRController{

	function __construct(){
		parent::__construct();
	}

	public function postPayment(MRRequest $request){
        return Response::json(SubscriptionService::payment($request->input()));
	}

    public function postGenerateCard(MRRequest $request){
        return Response::json(PrepaidCardService::generateCard($request->input("totalCard"), $request->input("numberOfDays"), $request->input("amount"), $request->input("test_card")));
	}
}
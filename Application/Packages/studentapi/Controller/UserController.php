<?php
namespace Application\Packages\studentapi\Controller;

use Application\Model\Ranking;
use MRPHPSDK\MRController\MRController;
use MRPHPSDK\MRDatabase\MRDatabase;
use MRPHPSDK\MRRequest\MRRequest;
use Application\Model\Response;
use Application\Packages\studentapi\Services\UserService;
use MRPHPSDK\MRVendor\MRAccessToken\MRAccessToken;

class UserController extends MRController {

	function __construct(){
		parent::__construct();
	}

	public function getProfile(MRRequest $request){
        $response = Response::toJson(UserService::userProfile($this->user->id), 1, "User details successfully updated.");
        return $response;
	}

    public function postName(MRRequest $request) {
        return Response::json(UserService::name($this->user->id,$request->input()));
	}
    
    public function postFeedback(MRRequest $request) {
        return UserService::sendFeedback($this->user->id,$request->input());
	}

    public function postUpdate(MRRequest $request) {
        return UserService::updateUser($request->input(), $this->user->id);
    }

    public function postUpdateGCM(MRRequest $request) {
        return UserService::updateGCM($this->user->id,$request->input());
	}

    public function getRechargeHistory(MRRequest $request) {
        return UserService::rechargeHistory($this->user);
    }
    
    public function postLogout(MRRequest $request) {
        MRAccessToken::delete();
        return Response::json(Response::data([], 1, "Successfully logged out."));
    }

    public function postJobs(MRRequest $request) {
        return $this->getJobs($request);
    }

    public function getJobs(MRRequest $request) {
        $currentDate = date("Y-m-d");
        $jobs = MRDatabase::select("SELECT * FROM Job WHERE status=1 AND end_date >= '$currentDate'");
        return Response::toJson(["jobs" =>$jobs], 1, "Job list fetched success.");
    }

    public function getRanking(MRRequest $request) {

        return Response::toJson([], 0, "Something went wrong.");

        $todayDate = date('Y-m');
        if($request->input("month") != "") {
            $todayDate = $request->input("month");
        }

        $student = Ranking::where("DATE_FORMAT(CONVERT_TZ(Ranking.created_at,'+00:00','+05:30'), '%Y-%m')", $todayDate)->where("studentId", $this->user->id)->selectField(["Ranking.id as rankingId", "studentId", "marks", "timeTaken", "totalAttemptedTest", "month", "Student.full_name", "Student.mobile", "Student.picture", "rank"])->leftJoin("Student", "id", "studentId", "roll_no")->first();

        if($request->input("page") != "" && $request->input("page") > 0) {
            $paging = Ranking::paging("DATE_FORMAT(CONVERT_TZ(Ranking.created_at,'+00:00','+05:30'), '%Y-%m') = '".$todayDate."'", $request->input("page"), 20);
            $toppers = Ranking::where("DATE_FORMAT(CONVERT_TZ(Ranking.created_at,'+00:00','+05:30'), '%Y-%m')", $todayDate)->page($request->input("page"))->leftJoin("Student", "id", "studentId")->selectField(["Ranking.id as rankingId", "studentId", "marks", "timeTaken", "totalAttemptedTest", "month", "Student.full_name", "Student.mobile", "Student.picture", "rank", "roll_no"])->orderBy("rank", "ASC")->get();
            return Response::toJson(["toppers" =>$toppers, "student"=>$student, "paging"=>$paging], 1, "Toppers list fetched success.");
        } else {
            $toppers = Ranking::where("DATE_FORMAT(CONVERT_TZ(Ranking.created_at,'+00:00','+05:30'), '%Y-%m')", $todayDate)->leftJoin("Student", "id", "studentId")->selectField(["Ranking.id as rankingId", "studentId", "marks", "timeTaken", "totalAttemptedTest", "month", "Student.full_name", "Student.mobile", "Student.picture", "rank", "roll_no"])->limit(5, 0)->orderBy("rank", "ASC")->get();
            return Response::toJson(["toppers" =>$toppers, "student"=>$student], 1, "Toppers list fetched success.");
        }
    }

}
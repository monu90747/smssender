<?php
namespace Application\Packages\studentapi\Controller;

use MRPHPSDK\MRController\MRController;
use MRPHPSDK\MRRequest\MRRequest;
use Application\Model\Response;
use MRPHPSDK\MRValidation\MRValidation;
use Application\Packages\studentapi\Services\PrepaidCardService;

class PaymentController extends MRController{

	function __construct(){
		parent::__construct();
	}

	public function postPrepaidCard(MRRequest $request){
		$validation = new MRValidation($request->input(), [
            'code' => 'required'
        ], []);

        if($validation->validateFailed()){
            return Response::json([], 0, $validation->getValidationError()[0]);
        }
        
        $response = PrepaidCardService::subscribe($request->input("code"), $this->user->id);
        return Response::json($response);
	}

}
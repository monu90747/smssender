<?php
namespace Application\Packages\studentapi\Controller;

use Application\Model\Response;
use Application\Packages\student\Services\AuthService;
use Application\Services\UploadService;
use MRPHPSDK\MRController\MRController;
use MRPHPSDK\MRRequest\MRRequest;

class AuthController extends MRController{

	public function postSignUp(MRRequest $request){
        return Response::json(AuthService::signUp($request->input(), $_FILES));
	}

	public function postLogin(MRRequest $request){
	    return Response::json(AuthService::signIn($request->input()));
    }

    public function postUpload(MRRequest $request){
	    $service = new UploadService();
	    $service->uploadFile($_FILES["profile"], "9109322140");
	    return Response::json($service);
    }

    public function postForgotPassword(MRRequest $request) {
        return Response::json(AuthService::generateOTP($request->input()));
    }

    public function postChangePassword(MRRequest $request) {
        return Response::json(AuthService::changePassword($request->input()));
    }

    public function getUpload(MRRequest $request){
        $this->view("test");
    }

}
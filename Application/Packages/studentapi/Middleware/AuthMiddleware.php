<?php
namespace Application\Packages\studentapi\Middleware;

use MRPHPSDK\MRException\MRException;
use MRPHPSDK\MRMiddleware\MRMiddleware;
use MRPHPSDK\MRModels\MRSession;
use MRPHPSDK\MRRequest\MRRequest;
use MRPHPSDK\MRVendor\MRAccessToken\MRAccessToken;

class AuthMiddleware extends MRMiddleware{

	/*
	|--------------------------------------------------------------------------
	| Handle all request
	|--------------------------------------------------------------------------
	*/
	public function handle(MRRequest $request){
		try{
            MRAccessToken::setAuthClass("\\Application\\Packages\\student\\Model\\Student");
            $token = MRAccessToken::authorize();
        }
        catch(MRException $e){
            MRSession::delete("token");
            return $this->failed($e->getMessage(), $e->getStatusCode());
        }

        return $this->success();
	}

}
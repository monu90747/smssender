<?php

use \MRPHPSDK\MRRoute\Route;

Route::group([
	'package' => 'studentapi',
	'prefix' => 'studentapi',
], function(){
    
    Route::controller("auth", "AuthController");
    Route::controller("subscription", "SubscriptionController");
    Route::controller("question", "QuestionController");
    
    Route::group([
        'middleware' => ['/Application/Packages/studentapi/Middleware/AuthMiddleware'],
    ], function(){
        Route::controller("user", "UserController");
        Route::controller("test", "TestController");
        Route::controller("payment", "PaymentController");
        Route::controller("ranking", "RankingController");
    });
});
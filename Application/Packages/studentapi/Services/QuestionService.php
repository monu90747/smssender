<?php

namespace Application\Packages\studentapi\Services;

use Application\Model\User;
use Application\Model\Response;
use MRPHPSDK\MRValidation\MRValidation;
use Application\Packages\studentapi\Model\Question;
use Application\Packages\studentapi\Model\Answer;
use Application\Packages\studentapi\Model\Test;
use MRPHPSDK\MRVendor\XLSXReader\XLSXReader;

class QuestionService{
    
    public static function question($userId){
        $user = User::where("id", $userId)->selectField(["id", "full_name", "mobile", "email", "picture", "email_verify", "mobile_verify", "role", "created_at", "updated_at"])->get();
        return Response::data(["user"=>$user[0]], 1, "");
    }
    
    public static function saveQuestion($params){
        if($params["subject_id"] > 0){
            $question = new Question();
            $question->subject_id = $params["subject_id"];
            $question->title = $params["question"];
            $question->save();
            if($question->id > 0){
                QuestionService::saveAnswer($params["option1"], $question->id, ($params["answer_index"] == 1) ? 1 : 0);
                QuestionService::saveAnswer($params["option2"], $question->id, ($params["answer_index"] == 2) ? 1 : 0);
                QuestionService::saveAnswer($params["option3"], $question->id, ($params["answer_index"] == 3) ? 1 : 0);
                QuestionService::saveAnswer($params["option4"], $question->id, ($params["answer_index"] == 4) ? 1 : 0);
            }
        }
    }
    
    public static function saveAnswer($name, $que_id, $isCorrect){
        $answer = new Answer();
        $answer->que_id = $que_id;
        $answer->name = $name;
        $answer->is_correct = $isCorrect;
        $answer->save();
    }
    
    public static function import($filename, $params){
        $validation = new MRValidation($params, [
            'subject_id' => 'required|exists:Test:id'
        ], []);

        if($validation->validateFailed()){
            return Response::data([], 0, $validation->getValidationError()[0]);
        }
        
        $reader = new XLSXReader($filename);
        $sheetNames = $reader->getSheetNames();

        foreach ($sheetNames as $name){
            $sheet = $reader->getSheet($name);
            $sheetData = $sheet->getData();
            
            for($i=1; $i<count($sheetData); $i++){
                $queData = [];
                $data = $sheetData[$i];
                $queData["question"] = isset($data[0]) ? $data[0] : "";;
                $queData["option1"] = isset($data[1]) ? $data[1] : "";;
                $queData["option2"] = isset($data[2]) ? $data[2] : "";
                $queData["option3"] = isset($data[3]) ? $data[3] : "";;
                $queData["option4"] = isset($data[4]) ? $data[4] : "";
                $queData["answer_index"] = isset($data[5]) ? $data[5] : "";
                $queData["subject_id"] = $params["subject_id"];
                QuestionService::saveQuestion($queData);
            }
        }
        
        return Response::data([], 1, "Question uploaded successfully.");
    }

    public static function importTest($filename) {
        $reader = new XLSXReader($filename);
        $sheetNames = $reader->getSheetNames();

        foreach ($sheetNames as $name){
            $sheet = $reader->getSheet($name);
            $sheetData = $sheet->getData();
            
            for($i=0; $i<count($sheetData); $i++){
                $queData = [];
                $data = $sheetData[$i];

                $test = new Test();
                $test->title = $data[0];
                $test->details = $data[1];
                $test->instruction = $data[2];
                $test->duration = $data[3];
                $test->total_question = $data[4];
                $test->medium = $data[5];
                $test->status = "1";
                $test->save();                
            }
        }

        return Response::data([], 1, "Test uploaded successfully.");
    }
}
<?php

namespace Application\Packages\studentapi\Services;

use Application\Model\Version;
use Application\Packages\studentapi\Model\Test;
use Application\Model\Response;
use MRPHPSDK\MRDatabase\MRDatabase;
use MRPHPSDK\MRValidation\MRValidation;
use Application\Packages\studentapi\Model\Question;
use Application\Packages\studentapi\Model\Answer;
use Application\Packages\studentapi\Model\Report;
use Application\Packages\studentapi\Model\Subscription;

class TestService{
    
    public static function all($params, $userId, $user) {
        $validation = new MRValidation($params, [
            'medium' => 'required'
        ], []);

        if($validation->validateFailed()){
            return Response::toJson([], 0, $validation->getValidationError()[0]);
        }

        $isLive = 0;
        $institute_code = "";
        if(isset($params["is_live"]) && $params["is_live"] != "") {
            $isLive = $params["is_live"];
        }

        $batchTime = "(batchTime like '%".$user->batchTime."%' OR batchTime='')";

        if($user->institute_code != "" && $user->institute_code > 1000) {
            $institute_code .= "institute_code = 1000 OR institute_code = ".$user->institute_code;
        } else {
            $institute_code .= "institute_code = 1000";
        }

        $query = "SELECT * FROM Test WHERE medium='".$params["medium"]."' AND $batchTime AND is_live=$isLive AND status=1 AND ($institute_code) ORDER BY id DESC";
        $tests = MRDatabase::select($query);
        $data = [];
        foreach($tests as $test) {
            $attempted = Report::where("student_id", $userId)->where("test_id", $test["id"])->get();
            if(count($attempted) <= 0) {
                $test["instruction"] = json_decode($test["instruction"]);
                $data[] = $test;
            }            
        }

        $version = Version::where("id", "1")->first();
        $subData["version"] = $version->number;
        $subData["isForceUpdate"] = $version->isForceUpdate;

        $subscription = Subscription::where("student_id", $userId)->first();
        if($subscription == null){
            $subData["start_date"] = "";
            $subData["expiry_date"] = "";
            $subData["remain_days"] = 0;
        }
        else{
            $subData["start_date"] = $subscription->start_date;
            $subData["expiry_date"] = $subscription->expiry_date;
            $earlier = new \DateTime($subData["expiry_date"]);
            $later = new \DateTime(date('Y-m-d'));
            if($earlier>=$later) {
                $diff = $later->diff($earlier)->format("%a");
                $subData["remain_days"] = $diff;
            } else {
                $subData["remain_days"] = 0;
            }
        }
        return Response::toJson(["test" => $data, "subscription" => $subData], 1, "");
    }
    
    public static function getAttempted($userId) {
        $tests = Test::where("is_live", "0")->get();
        $data = [];
        foreach($tests as $test){
            $attempted = Report::where("student_id", $userId)->where("test_id", $test["id"])->get();
            if(count($attempted) > 0){
                $test["isAttempted"] = 1;
                if($test["is_live"] == 0) {
                    $test["instruction"] = json_decode($test["instruction"]);
                    $test["report"] = $attempted;
                    $test["toast"] = "";
                    $data[] = $test;
                } else {
                    $test["toast"] = "This test result will come soon...";
                    $test["instruction"] = json_decode($test["instruction"]);
                    $test["report"] = [];
                    $data[] = $test;
                }
            } else {
                $test["isAttempted"] = 0;
            }
        }
        return Response::toJson(["attempted" => $data], 1, "");
    }
    
    public static function getTest($testId, $isAnswer = false){
        if($testId <= 0){
            return Response::toJson([], 0, "Test id is required.");
        }
        
        $questions = Question::where("subject_id", $testId)->where("status", "1")->selectField(["id as que_id", "title"])->get();
        $data = [];
        foreach($questions as $que){
            $columns = [];
            $columns[] = "id as ans_id";
            $columns[] = "name";
            if($isAnswer == true) {
                $columns[] = "is_correct";
            }
                
            $answers = Answer::where("que_id", $que["que_id"])->selectField($columns)->get();
            $que["answers"] = $answers;
            $data[] = $que;
        }

        return Response::toJson($data, 1, "");
    }
    
    public static function submit($params, $studentId){
        $validation = new MRValidation($params, [
            'test_id' => 'required|exists:Test:id',
            'total_que' => 'required'
        ], []);

        if($validation->validateFailed()){
            return Response::toJson([], 0, $validation->getValidationError()[0]);
        }

        $report = Report::where("is_live=1 AND test_id", $params["test_id"])->where("student_id", $studentId)->first();
        if($report != null) {
            return Response::toJson($report, 1, "Test submitted successfully.");
        }
        
        $answers = $params["answers"];
        
        $report = new Report($params);
        $report->student_id = $studentId;
        $report->attampt = count($answers);
        
        $correctCount = 0;
        foreach($answers as $ans){
            $correctAnswers = Answer::where("id", $ans["ans_id"])->where("que_id", $ans["que_id"])->where("is_correct", "1")->first();
            if($correctAnswers != null){
                $correctCount = $correctCount+1;
            }
        }
        
        $report->answers = json_encode($answers);
        $report->marks = $correctCount;
        $report->save();
        
        return Response::toJson($report, 1, "Test submitted successfully.");
    }
    
    public static function fetchReport($studentId){
        $report = Report::where("student_id", $studentId)->leftJoin("Test", "id", "test_id")->orderBy("Report.created_at", "DESC")->get();
        return Response::data($report, 1, "");
    }

    public static function fetchRanking($params) {
        $validation = new MRValidation($params, [
            'test_id' => 'required|exists:Test:id'
        ], []);

        if($validation->validateFailed()){
            return Response::toJson([], 0, $validation->getValidationError()[0]);
        }

        $page = 1;
        if(isset($params["page"]) && $params["page"] != null && $params["page"] != "") {
            $page = $params["page"];
        }

        $start = ($page - 1) * 20;

        $testId = $params["test_id"];
        $paging = Report::paging("is_live=1 AND test_id = ".$testId, $page, 20);
        //$query = "SELECT DISTINCT(Student.id), (@row := @row + 1) as rank, full_name as name, picture, roll_no as rollNumber, mobile, marks, time_taken as timeTaken FROM `Report` LEFT JOIN Student on Student.id=Report.student_id, (SELECT @row := $start) r WHERE `test_id` = $testId ORDER BY marks DESC, time_taken ASC LIMIT $start, 20";
        $query = "SELECT (@row := @row + 1) as rank, Report.is_live, full_name as name, picture, roll_no as rollNumber, mobile, marks, time_taken as timeTaken FROM `Report` LEFT JOIN Student on Student.id=Report.student_id, (SELECT @row := $start) r WHERE `test_id` = $testId AND is_live=1 ORDER BY marks DESC, time_taken ASC LIMIT $start, 20";
        $records = MRDatabase::select($query);
        return Response::toJson(["ranking"=> $records, "paging"=> $paging], 1, "Users rank");

    }

    public static function removeDuplicateReport() {
        $deletedUser = [];
        $reports = Report::where("is_live", "1")->get();
        foreach($reports as $report) {
            $fetchReportByUser = Report::where("student_id", $report["student_id"])->where("test_id", $report["test_id"])->get();
            if(count($fetchReportByUser) > 1) {
                for($i=1; $i<count($fetchReportByUser); $i++) {
                    MRDatabase::query("DELETE FROM Report WHERE id=".$fetchReportByUser[$i]["id"]);
                    $deletedUser[] = $fetchReportByUser[$i];
                }
            }
        }
        return $deletedUser;
    }

}
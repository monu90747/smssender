<?php

namespace Application\Packages\studentapi\Services;

use Application\Model\User;
use Application\Packages\student\Model\Student;
use Application\Packages\studentapi\Model\Feedback;
use Application\Packages\studentapi\Model\Institute;
use Application\Packages\studentapi\Model\Notification;
use Application\Model\Response;
use Application\Packages\studentapi\Model\Payment;
use Application\Packages\studentapi\Model\Subscription;
use MRPHPSDK\MRValidation\MRValidation;
use MRPHPSDK\MRModels\MRAssets;

class UserService{
    
    public static function profile($userId) {
        $user = User::where("id", $userId)->selectField(["id", "full_name", "mobile", "email", "picture", "email_verify", "mobile_verify", "role", "created_at", "updated_at"])->get();
        return Response::data(["user"=>$user[0]], 1, "");
    }
    
     public static function name($userId,$params){
        $validation = new MRValidation($params, [
            'name' => 'required',
            'mob'=>   'required|digits:10'
        ], [
            'name.required' => 'Please enter a valid name',
            'mob.required' => 'Mobile no add karo',
            'mob.digits'   => 'Pooe No dalo pagal aadmi'

        ]);

        if($validation->validateFailed()){
            return Response::data([], 0, $validation->getValidationError()[0]);
        }
        $user = User::where("id", $userId)->selectField(["id", "full_name"])->get();
        return Response::data(["user"=>$user[0]], 1, $params["name"].$params["mob"]);
    }
    
    public static function sendFeedback($userId, $params){
        $validation = new MRValidation($params, [
            'student_id' => 'required|exists:Student:id',
            'message'=>   'required'
        ], []);

        if($validation->validateFailed()){
            return Response::toJson([], 0, $validation->getValidationError()[0]);
        }
        
        $feedback = new Feedback($params);
        $feedback->save();
        
        return Response::toJson([], 1, "Your feedback successfully submitted.");
    }

    public static function updateUser($params, $userId) {

        if(isset($params["roll_no"]) && $params["roll_no"] != "" && isset($params["institute_code"]) && $params["institute_code"] != "") {
            $student = Student::where("institute_code", $params["institute_code"])->where("roll_no", $params["roll_no"])->first();
            if($student != null) {
                return Response::toJson([], 0, "Roll number is already registered.");
            }
        }

        $student = Student::where("id", $userId)->first();
        unset($params["password"]);
        unset($params["mobile"]);

        if(isset($params["profile"]) && $params["profile"] != "") {
            $assets = new MRAssets();
            $location = __DIR__."/../../../../public/assets/app/user/";
            $student->picture = $assets->saveImageAssets($params["profile"], $location);
        }

        $student->save($params);
        return Response::toJson(UserService::userProfile($userId), 1, "User details successfully updated.");
    }

    public static function userProfile($userId) {
        $student = Student::where("id", $userId)->first();
        $student->password = null;

        if($student->institute_code != "" || $student->institute_code != null) {
            $student->institute = Institute::where("institute_code", $student->institute_code)->first();
            $student->institute->password = null;
            $student->institute->role = null;
        } else {
            $student->institute = null;
        }

        $subData["version"] = 15;
        $subData["isForceUpdate"] = 1;
        $subscription = Subscription::where("student_id", $userId)->first();
        if($subscription == null){
            $subData["start_date"] = "";
            $subData["expiry_date"] = "";
            $subData["remain_days"] = 0;
        }
        else{
            $subData["start_date"] = $subscription->start_date;
            $subData["expiry_date"] = $subscription->expiry_date;
            $earlier = new \DateTime($subData["expiry_date"]);
            $later = new \DateTime(date('Y-m-d'));
            if($earlier>=$later) {
                $diff = $later->diff($earlier)->format("%a");
                $subData["remain_days"] = $diff;
            } else {
                $subData["remain_days"] = 0;
            }
        }
        $student->subscription = $subData;
        return $student;
    }
    
    public static function updateGCM($userId, $params){
        $validation = new MRValidation($params, [
            'student_id' => 'required|exists:Student:id',
            'gcm_token'=>   'required',
            'device_type' => 'required'
        ], []);

        if($validation->validateFailed()){
            return Response::toJson([], 0, $validation->getValidationError()[0]);
        }
        
        
        $notification = Notification::where("student_id", $params["student_id"])->first();
        if($notification == null){
            $notification = new Notification();
        }

        $notification->save($params);
        return Response::toJson([], 1, "Your GCM token successfully updated.");
    }

    public static function rechargeHistory($user) {
        $payment = Payment::where("ref_id", $user->id)->orderBy("id", "DESC")->get();
        return Response::toJson(["recharges" => $payment], 1, "Your recharge history.");
    }
    
}
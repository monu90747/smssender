<?php

namespace Application\Packages\studentapi\Services;

use Application\Packages\studentapi\Model\PrepaidCard;
use Application\Model\Response;
use Application\Packages\studentapi\Services\SubscriptionService;

class PrepaidCardService {
    
    static function subscribe($code, $studentId){
        $card = PrepaidCard::where("code", $code)->where("status", "0")->first();
        if($card == null){
            return Response::data([], 0, "Prepaid card is invalid. Please enter a valid prepaid card.");
        }
        
        $cardParams = [];
        $cardParams["ref_id"] = $studentId;
        $cardParams["amount"] = ($card->test_card == 1) ? 0 : $card->amount;
        $cardParams["mode"] = ($card->test_card == 1) ? "Prepaid Test Card" : "Prepaid Card";
        $cardParams["credit"] = $card->day;
        $cardParams["details"] = "Make payment by prepaid card";
        $cardParams["prepaidCard"] = $code;
        $cardParams["isTest"] = $card->test_card;
        SubscriptionService::payment($cardParams);
        
        $card->student_id = $studentId;
        $card->status = 1;
        $card->save();
        
        return Response::data([], 1, "You have successfully redeemed your prepaid card.");
        
    }
    
    static function prepaidCard($code) {
        $card = PrepaidCard::where("code", $code)->first();
        return $card;
    }
        
    static function generateCard($totalCard, $numberOfDays, $amount, $testCard = 0){
        $cardNumbers = [];
        for ($i = 0; $i < $totalCard; $i++){
            $code = PrepaidCardService::randomCode();
            PrepaidCardService::addNewCard($code, $numberOfDays, $amount, $testCard);
            $cardNumbers[] = $code;
        }
        return Response::data(["cards" => $cardNumbers], 1, "");
    }
    
    static function addNewCard($code, $numberOfDays, $amount, $testCard = 0){
        $card = new PrepaidCard();
        $card->code = $code;
        $card->day = $numberOfDays;
        $card->student_id = 0;
        $card->status = 0;
        $card->test_card = $testCard;
        $card->amount = $amount;
        $card->expiry = date('Y-m-d', strtotime(date('Y-m-d'). ' + 180 days'));
        $card->save();
    }
    
    static function randomCode(){
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $randstring = '';
        for ($i = 0; $i < 15; $i++) {
            $char = $characters[rand(0, 35)];
            if($char!=''){
                $randstring.=$char;
            }
        }   
        
        $card = PrepaidCardService::prepaidCard($randstring);
        print_r($card);
        if($card != null){
            return randomCode();
        }
        return $randstring;
    }
    
}
<?php

namespace Application\Packages\studentapi\Services;

use MRPHPSDK\MRValidation\MRValidation;
use Application\Model\Response;
use Application\Packages\studentapi\Model\Payment;
use Application\Packages\studentapi\Model\Subscription;

class SubscriptionService {
    
    static function payment($params){
        $validation = new MRValidation($params, [
            'ref_id' => 'required',
            'mode' => 'required',
            'credit' => 'requires'
        ], []);

        if($validation->validateFailed()){
            return Response::data([], 0, $validation->getValidationError()[0]);
        }
        
        $payment = new Payment($params);
        $payment->status = "Success";
        $payment->service = "None";
        $payment->sms_credit = 0;
        $payment->subscription_days = $params["credit"];
        $payment->type = "TEST";
        $payment->save();
        
        $subscription = Subscription::where("student_id", $params["ref_id"])->first();
        if($subscription != null){
            if($subscription->expiry_date >= date('Y-m-d')){
                $subscription->expiry_date = date('Y-m-d', strtotime($subscription->expiry_date. ' + '.$params["credit"].' days'));
            }
            else{
                $subscription->start_date = date('Y-m-d');
                $subscription->expiry_date = date('Y-m-d', strtotime(date('Y-m-d'). ' + '.$params["credit"].' days'));
            }
        }
        else{
            $subscription = new Subscription();
            $subscription->student_id = $params["ref_id"];
            $subscription->start_date = date('Y-m-d');
            $subscription->expiry_date = date('Y-m-d', strtotime(date('Y-m-d'). ' + '.$params["credit"].' days'));
        }
        
        $subscription->save();
        
        return Response::data([], 1, "Your subscription purchased successfully.");
    }
    
    static function freeSubscription($userId, $days){
        $params = [];
        $params["ref_id"] = $userId;
        $params["amount"] = 0;
        $params["mode"] = "Free";
        $params["credit"] = $days;
        $params["details"] = "Free subscription for promotion.";
        SubscriptionService::payment($params);    
    }
    
}
<?php
namespace Application\Packages\student\Middleware;

use MRPHPSDK\MRException\MRException;
use MRPHPSDK\MRMiddleware\MRMiddleware;
use MRPHPSDK\MRModels\MRSession;
use MRPHPSDK\MRRequest\MRRequest;
use MRPHPSDK\MRVendor\MRAccessToken\MRAccessToken;

class AuthMiddleware extends MRMiddleware{

	/*
	|--------------------------------------------------------------------------
	| Handle all request
	|--------------------------------------------------------------------------
	*/
	public function handle(MRRequest $request){
        try{
            MRAccessToken::setAuthClass("\\Application\\Packages\\student\\Model\\Student");
            $token = MRAccessToken::authorize((MRSession::isSetValue("token")?MRSession::getValue("token"):""), "WEB");
        }
        catch(MRException $e){
            MRSession::delete("token");
            return $this->failed($e->getMessage(), $e->getStatusCode());
        }

        return $this->success();
	}

}
<?php
namespace Application\Packages\student\Controller;

use Application\Packages\student\Services\AuthService;
use MRPHPSDK\MRController\MRController;
use MRPHPSDK\MRRequest\MRRequest;

class AuthController extends MRController{

	function __construct(){
		parent::__construct();
	}

	public function getLogin(MRRequest $request){
        $this->view("Student/login");
	}

    public function getSignUp(MRRequest $request){
        $this->view("Student/signup");
    }

    public function getForgotPassword(MRRequest $request){
        $this->view("Student/forgotpassword");
    }

    public function postSignUp(MRRequest $request){
        $result = AuthService::signUp($request->input());
        if($result->status == 1){
            $this->redirect("/student/auth/login", [], "GET");
        }
        else{
            $this->view("Student/signup", ["message"=>$result->message]);
        }
    }

}
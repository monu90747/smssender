<?php
namespace Application\Packages\student\Controller;

use MRPHPSDK\MRController\MRController;
use MRPHPSDK\MRRequest\MRRequest;

class DashboardController extends MRController{

	function __construct(){
		parent::__construct();
	}

	public function getIndex(MRRequest $request){
        $this->view("blank");
	}

}
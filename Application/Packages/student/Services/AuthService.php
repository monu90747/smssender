<?php

namespace Application\Packages\student\Services;

use Application\Model\Response;
use Application\Packages\student\Model\Student;
use Application\Packages\studentapi\Model\Institute;
use Application\Packages\studentapi\Services\OTPService;
use Application\Services\UploadService;
use MRPHPSDK\MRValidation\MRValidation;
use MRPHPSDK\MRVendor\MRAccessToken\MRAccessToken;
use MRPHPSDK\MRModels\MRAssets;
use Application\Packages\studentapi\Model\Subscription;
use Application\Packages\studentapi\Services\SubscriptionService;
use Application\Packages\studentapi\Model\PrepaidCard;

class AuthService {

    public static function generateOTP($params) {
        $validation = new MRValidation($params, [
            'mobile' => 'required|digits:10|exists:Student'
        ], [
            'mobile.required' => 'Please enter mobile number',
            'mobile.digits' => 'Mobile number is in invalid format',
            'mobile.exists' => 'Oops! Mobile number does not registered.'
        ]);

        if($validation->validateFailed()){
            return Response::toObject($validation->getValidationError()[0]);
        }

        OTPService::send($params["mobile"]);
        return Response::toObject("OTP sent successfully.", 1);
    }

    public static function changePassword($params) {
        $validation = new MRValidation($params, [
            'mobile' => 'required|digits:10|exists:Student',
            'password' => 'required'
        ], [
            'mobile.required' => 'Please enter mobile number',
            'mobile.digits' => 'Mobile number is in invalid format',
            'mobile.exists' => 'Oops! Mobile number does not registered.'
        ]);

        if($validation->validateFailed()){
            return Response::toObject($validation->getValidationError()[0]);
        }

        $verify = OTPService::verify($params);
        if($verify["status"] == 1) {
            $student = Student::where("mobile", $params["mobile"])->first();
            $student->password = hash("SHA512", $params["password"]);
            $student->save();
            return Response::toObject("Your password changed successfully.", 1, []);
        } else {
            return $verify;
        }
    }

    public static function signUp($params, $files = []){
        $validation = new MRValidation($params, [
            'full_name' => 'required',
            'mobile' => 'required|digits:10|unique:Student',
            'password' => 'required'
        ], [
            'full_name.required' => 'Please enter full name',
            'mobile.required' => 'Please enter mobile number',
            'mobile.digits' => 'Mobile number is in invalid format',
            'mobile.unique' => 'Oops! Mobile number already associated with another account.',
            'password.required' => 'Please enter a valid password'
        ]);

        if($validation->validateFailed()){
            return Response::toObject($validation->getValidationError()[0]);
        }

        if(isset($params["institute_code"]) && $params["institute_code"] != "") {
            $institute = Institute::where("institute_code", $params["institute_code"])->first();
            if($institute == null) {
                return Response::data([], 0, "Institute code is invalid.");
            }
        }

        if(isset($params["device_id"]) && $params["device_id"] != "") {
            $student = Student::where("device_id", $params["device_id"])->first();
            if($student != null) {
                return Response::toObject("This device is already registered. Please contact to customer support if you have any issue.");
            }
        } else {
            return Response::data([], 0, "Device ID can not be blank.");
        }

        if(isset($params["email"]) && $params["email"] != ""){
            $validation = new MRValidation($params, [
                'email' => 'email|unique:Student',
            ], [
                'email.email' => 'Email address is invalid. Please enter a valid email address',
                'email.unique' => 'Oops! Email address already registered'
            ]);
            if($validation->validateFailed()){
                return Response::toObject($validation->getValidationError()[0]);
            }
        }

        if(isset($params["roll_no"]) && $params["roll_no"] != "" && isset($params["institute_code"]) && $params["institute_code"] != "") {
            $student = Student::where("institute_code", $params["institute_code"])->where("roll_no", $params["roll_no"])->first();
            if($student != null) {
                return Response::data([], 0, "Roll number is already registered.");
            }
        }

        $card = null;
        if(isset($params["prepaidCard"]) && $params["prepaidCard"] != "") {
            $card = PrepaidCard::where("code", $params["prepaidCard"])->where("status", "0")->first();
            if($card == null){
                return Response::toObject("Prepaid card is invalid. Please enter a valid prepaid card.");
            }
        }
        
        $fileName = "";
        if(isset($files["profile"]) && count($files["profile"]) > 0){
            $service = new UploadService();
            $service->uploadFile($_FILES["profile"], $params["mobile"]);            
            if($service->status == true){
                $fileName = $service->fileName;
            }
            else{
                return Response::toObject($service->message);
            }
        }
        
        if(isset($params["profile"]) && $params["profile"] != "") {
            $assets = new MRAssets();
            $location = __DIR__."/../../../../public/assets/app/user/";
            $fileName = $assets->saveImageAssets($params["profile"], $location);
        }

        $student = new Student($params);
        $student->password = hash("SHA512", $student->password);
        $student->picture = $fileName;
        $student->email_verify = 1;
        $student->mobile_verify = 1;
        $student->save();


        if(!(isset($params["institute_code"]) && $params["institute_code"] == "1006")) {
            SubscriptionService::freeSubscription($student->id, 7);
        }
        
        if($card != null){
            $cardParams = [];
            $cardParams["ref_id"] = $student->id;
            $cardParams["amount"] = $card->amount;
            $cardParams["mode"] = "Prepaid Card";
            $cardParams["credit"] = $card->day;
            $cardParams["details"] = "Make payment by prepaid card";            
            SubscriptionService::payment($cardParams);
            $card->student_id = $student->id;
            $card->status = 1;
            $card->save();
        }

        $login = AuthService::signIn($params);
        $login->message = "You have successfully registered";
        return $login;
    }

    public static function signIn($params){
        $validation = new MRValidation($params, [
            'mobile' => 'required|digits:10|exists:Student',
            'password' => 'required'
        ], [
            'mobile.required' => 'Please enter mobile number',
            'mobile.digits' => 'Mobile number is in invalid format',
            'mobile.exists' => 'Oops! Your mobile number does not registered.',
            'password.required' => 'Please enter a valid password'
        ]);

        if($validation->validateFailed()){
            return Response::toObject($validation->getValidationError()[0]);
        }

        $results = Student::where("mobile", $params["mobile"])->where("password", hash("SHA512", $params["password"]))->get();
        if(count($results) > 0){
            $student = $results[0];
            unset($student["password"]);
            $student["token"] = MRAccessToken::generate($student["id"], ["mobile"=>$student["mobile"], "datetime"=>date('Y-m-d H:i:s'), "email"=>$student["email"]]);
            $student["institute"] = Institute::where("institute_code", $student["institute_code"])->first();
            unset($student["institute"]->password);
            unset($student["institute"]->role);
            $subData["version"] = 1;
            $subData["isForceUpdate"] = 0;
            $subscription = Subscription::where("student_id", $student["id"])->first();
            if($subscription == null){
                $subData["start_date"] = "";
                $subData["expiry_date"] = "";
                $subData["remain_days"] = 0;
            }
            else{
                $subData["start_date"] = $subscription->start_date;
                $subData["expiry_date"] = $subscription->expiry_date;
                $earlier = new \DateTime($subData["expiry_date"]);
                $later = new \DateTime(date('Y-m-d'));
                if($earlier>=$later) {
                    $diff = $later->diff($earlier)->format("%a");
                    $subData["remain_days"] = $diff;
                } else {
                    $subData["remain_days"] = 0;
                }
            }
            $student["subscription"] = $subData;
            return Response::toObject("You have successfully logged in.", 1, $student);
        }
        else{
            return Response::toObject("Oops! You have entered wrong credentials");
        }
    }

}
<?php

use \MRPHPSDK\MRRoute\Route;

Route::group([
    'package' => 'student',
    'prefix' => 'student'
], function(){

    Route::controller("auth", "AuthController");

    Route::group([
        'middleware' => ['/Application/Packages/student/Middleware/AuthMiddleware'],
    ], function(){
        Route::controller("", "DashboardController");
    });

});
<?php

use \MRPHPSDK\MRRoute\Route;

Route::group([
//    'domain' => BASE_URL,
], function(){
    Route::controller("auth", "AuthController");
    Route::controller("error", "ErrorController");
    Route::controller("notify", "NotifyController");
    Route::controller("verify", "VerifyController");
    Route::controller("cron", "CronController");
    Route::controller("otp", "OTPController");

    Route::group([
        'middleware' => ['/Application/Middleware/AuthMiddleware'],
    ], function(){
        Route::controller("", "DashboardController");
        Route::controller("user", "UserController");
        Route::controller("contact", "ContactController");
        Route::controller("group", "GroupController");
        Route::controller("setting", "SettingController");
        Route::controller("sms", "SMSController");
        Route::controller("page", "PageController");
        Route::controller("transaction", "TransactionController");
        Route::controller("merge", "MergeController");
    });

    Route::group([
        'middleware' => ['/Application/Middleware/AdminMiddleware'],
        ], function(){
            Route::controller("admin", "AdminController");
    });
    
});
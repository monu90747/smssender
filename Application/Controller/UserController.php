<?php
namespace Application\Controller;

use Application\Model\Response;
use Application\Model\User;
use Application\Packages\studentapi\Services\PrepaidCardService;
use Application\Services\ContactService;
use Application\Services\GroupService;
use MRPHPSDK\MRController\MRController;
use MRPHPSDK\MRModels\MRSession;
use MRPHPSDK\MRRequest\MRRequest;
use MRPHPSDK\MRVendor\MRAccessToken\MRAccessToken;

class UserController extends MRController{

	function __construct(){
		parent::__construct();
	}

    public function getLogout(MRRequest $request){
        MRAccessToken::delete(MRSession::getValue("token"));
        MRSession::delete("token");
        $this->redirect("/auth/login", [], "GET");
    }

    public function getChangePassword(MRRequest $request){
        $groups = GroupService::getGroups($this->user->id);
        $contactCount = GroupService::getUserContactCount($this->user->id);

        $this->view("profile", ["user" => $this->user, "totalCount"=>$contactCount, "groups"=>$groups["data"]["groups"], "tab"=>2]);
    }

    public function getProfile(MRRequest $request){
        $groups = GroupService::getGroups($this->user->id);
        $contactCount = GroupService::getUserContactCount($this->user->id);
        $this->view("profile", ["user" => $this->user, "totalCount"=>$contactCount, "groups"=>$groups["data"]["groups"], "tab"=>1]);
    }

    public function getGenerateAPIKey() {
	    $user = User::where("id", $this->user->id)->first();
	    if($user->apikey == "" || $user->apikey == null) {
            $user->apikey = "H".$user->mobile.$user->id."D".$this->randomCode();
            $user->save();
        }
        $this->redirect("/user/profile", [], "GET");
    }

    function randomCode() {
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789#@!';
        $randstring = '';
        for ($i = 0; $i < 12; $i++) {
            $char = $characters[rand(0, 38)];
            if($char!=''){
                $randstring.=$char;
            }
        }
        return $randstring;
    }

    public function postChangePassword(MRRequest $request){
        $oldPassword = hash("SHA512", $request->input("oldPassword"));
        $newPassword = hash("SHA512", $request->input("newPassword"));

        $user = User::where("id", $this->user->id)->where("password", $oldPassword)->first();
        if($user != null){
            $user->password = $newPassword;
            $user->save();
            return json_encode(Response::data([], 1, "You password has been changed successfully"));
        }
        else{
            return json_encode(Response::data([], 0, "You have entered invalid old password"));
        }
    }

    public function postUpdate(MRRequest $request){
        $full_name = $request->input("full_name");
        $user = User::where("id", $this->user->id)->first();
        if($user != null){
            $user->full_name = $full_name;
            $user->save();
            return json_encode(Response::data([], 1, "Profile updated successfully"));
        }
        else{
            return json_encode(Response::data([], 0, "Something went wrong"));
        }
    }

}

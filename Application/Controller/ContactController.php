<?php
namespace Application\Controller;

use Application\Services\ContactService;
use Application\Services\ExcelReader;
use Application\Services\GroupService;
use Application\Services\SMSService;
use MRPHPSDK\MRController\MRController;
use MRPHPSDK\MRRequest\MRRequest;

class ContactController extends MRController{

	function __construct(){
		parent::__construct();
	}

    public function getIndex(MRRequest $request){
        $groups = GroupService::getGroups($this->user->id);
        $contacts = ContactService::get($this->user->id);
        $this->view("contact", ["user" => $this->user, "groups"=>$groups["data"]["groups"], "contacts"=>$contacts["data"]]);
    }

    public function postAdd(MRRequest $request){
        $contact = ContactService::add($request->input(), $this->user->id);
        return $contact;
    }

    public function postImport(MRRequest $request){
        ExcelReader::read($_FILES["fileToUpload"]["tmp_name"], $request->input("groupId"));
        $this->redirect("/?page=".$request->input("groupId"), [], "GET");
    }

    public function getDownloadSampleContactFile(MRRequest $request){
        $filename = "sample.xlsx";
        header("Content-Disposition: attachment; filename=\"" . basename($filename) . "\";");
        readfile(__DIR__."/../Services/".$filename);
    }

    public function getDownloadAPIDoc(MRRequest $request){
        $filename = "SMSSender-API.docx";
        header("Content-Disposition: attachment; filename=\"" . basename($filename) . "\";");
        readfile(__DIR__."/../Services/".$filename);
    }

    public function postDelete(MRRequest $request){
        $response = ContactService::delete($request->input("contact_ids"), $request->input("group_id"), $this->user->id);
        return json_encode($response);
    }
}
<?php
namespace Application\Controller;

use Application\Model\Balance;
use Application\Model\Message;
use Application\Model\Receiver;
use Application\Model\Response;
use MRPHPSDK\MRController\MRController;
use MRPHPSDK\MRRequest\MRRequest;
use MRPHPSDK\MRVendor\MRLog\MRLog;

class NotifyController extends MRController{

	function __construct(){
		parent::__construct();
	}

	public function postSMS(MRRequest $request){

        $params = $_POST;

        MRLog::add(json_encode($params));
        $data = json_decode($params["data"], true);
        $requestId = $data["requestId"];
        $message = Message::where("request_id", $requestId)->first();
        if($message != null){
            $failedCount = 0;
            foreach($data["numbers"] as $key=>$value){
                $receiver = Receiver::where("mobile", $key)->where("message_id", $message->id)->first();
                if($receiver != null){
                    $receiver->description = $value["desc"];
                    $receiver->status_code = $value["status"];
                    if($value["status"] == 1){
                        $receiver->status = "Sent";
                    }
                    else{
//                        if($receiver->status != "Failed"){
//                            $failedCount = $failedCount + 1;
//                        }
                        $receiver->status = "Failed";
                    }
                    $receiver->save();
                }
                else{
                    echo "Number not found";
                }
            }

//            if($failedCount > 0){
//                $balance = Balance::where("user_id", $message->user_id)->first();
//                if($balance != null){
//                    $newBalance = new Balance();
//                    $newBalance->id = $balance->id;
//                    $newBalance->transactional = $balance->transactional + $failedCount;
//                    $newBalance->save();
//                }
//            }

        }
        else{
            echo "Message or request id is invalid";
        }
	}

}
<?php
namespace Application\Controller;

use Application\Model\Sender;
use Application\Services\ContactService;
use Application\Services\GroupService;
use Application\Services\SMSService;
use MRPHPSDK\MRController\MRController;
use MRPHPSDK\MRRequest\MRRequest;

class SMSController extends MRController{

	function __construct(){
		parent::__construct();
	}

    public function getSend(MRRequest $request){
        $groups = GroupService::getGroups($this->user->id);
        $page = $request->input("page");
        if($page == "" && $page <= 0){
            $page = 1;
        }

        $contactCount = GroupService::getUserContactCount($this->user->id);
        $contacts = ContactService::get($this->user->id, 0, $page);
        $senders = Sender::where("user_id", $this->user->id)->get();
        $this->view("sms", ["currentGroupId"=>0,"groups"=>$groups["data"]["groups"], "contacts"=>$contacts["data"]["contact"],"total_page"=>$contacts["data"]["page"],"current_page"=>$page, "senders"=>$senders, "totalCount"=>$contactCount]);
    }

	public function postSend(MRRequest $request){
        return json_encode(SMSService::send($request->input(), $this->user));
	}

    public function getEmail(MRRequest $request){
        $this->view("template/verify_email");
    }

    public function getPrice(MRRequest $request){
        $groups = GroupService::getGroups($this->user->id);
        $contactCount = GroupService::getUserContactCount($this->user->id);
        $this->view("buy", ["user" => $this->user, "totalCount"=>$contactCount, "groups"=>$groups["data"]["groups"]]);

    }
    
    public function postBalance(MRRequest $request){
        return json_encode(SMSService::getBalance());
    }
}
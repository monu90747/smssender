<?php
namespace Application\Controller;

use Application\Services\GroupService;
use MRPHPSDK\MRController\MRController;
use MRPHPSDK\MRRequest\MRRequest;

class GroupController extends MRController{

	function __construct(){
		parent::__construct();
	}

	public function getIndex(MRRequest $request){
        $groups = GroupService::getGroups($this->user->id);
		$this->view("group", ["user" => $this->user, "groups"=>$groups["data"]["groups"]]);
	}

    public function postAdd(MRRequest $request){
        $group = GroupService::addGroup($request->input(), $this->user->id);
        return json_encode($group);
    }

    public function postDelete(MRRequest $request){
        $response = GroupService::deleteGroup($request->input("group_id"), $this->user->id);
        return json_encode($response);
    }
}
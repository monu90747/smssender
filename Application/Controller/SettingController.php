<?php
namespace Application\Controller;

use MRPHPSDK\MRController\MRController;
use MRPHPSDK\MRRequest\MRRequest;

class SettingController extends MRController{

	function __construct(){
		parent::__construct();
	}

    public function getIndex(MRRequest $request){
        $this->view("setting", ["user" => $this->user]);
    }

}
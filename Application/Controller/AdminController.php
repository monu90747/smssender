<?php
namespace Application\Controller;

use Application\Model\Balance;
use Application\Services\SMSService;
use MRPHPSDK\MRController\MRController;
use MRPHPSDK\MRRequest\MRRequest;
use Application\Services\GroupService;
use Application\Services\UserService;
use Application\Services\AdminService;
use Application\Model\Response;
use Application\Model\PurchaseSMS;
use Application\Model\Admin;

class AdminController extends MRController{

	function __construct(){
		parent::__construct();
	}

	public function getDashboard(MRRequest $request){
		$groups = GroupService::getGroups($this->user->id);
        $contactCount = GroupService::getUserContactCount($this->user->id);
        $users = UserService::all();
        
        $transactions = AdminService::allTransactions();
        $admin = AdminService::admin();

        // Fetch portal balance
        $smsBalance = SMSService::getBalance();
        $portalSMSCount = $smsBalance["data"]["response"];

        $custBalance = Balance::where("promotional", "0")->selectField(["SUM(transactional) as total"])->first();
        $this->view("admin/dashboard", ["custBalance"=>$custBalance->total, "portalSMSCount"=>$portalSMSCount, "admin"=>$admin, "transactions"=>$transactions, "user" => $this->user, "app_users"=>$users, "totalCount"=>$contactCount, "groups"=>$groups["data"]["groups"], "tab"=>1]);
	}
    
    public function getStatement(MRRequest $request){
        $groups = GroupService::getGroups($this->user->id);
        $contactCount = GroupService::getUserContactCount($this->user->id);
        $admin = AdminService::admin();

        $this->view("admin/statement", ["admin"=>$admin, "totalCount"=>$contactCount, "groups"=>$groups["data"]["groups"], "tab"=>1]);
    }

    public function postCredit(MRRequest $request){
        return Response::json(AdminService::addCredit($request->input(), $this->user->id));
    }
    
    public function postPurchase(MRRequest $request){
        $purchase = new PurchaseSMS($request->input());
        $purchase->save();
        
        if($purchase->id > 0){
            $admin = Admin::where("id", "1")->first();
            $admin->totalSMS = $admin->totalSMS + $purchase->smsCount;
            $admin->save();
            return Response::json(Response::data([], 1, "Success"));
        }
        else{
             return Response::json(Response::data([], 0, "Something went wrong"));
        }
    }
    
}
<?php
namespace Application\Controller;

use MRPHPSDK\MRController\MRController;
use MRPHPSDK\MRRequest\MRRequest;
use Application\Services\OTPService;
use Application\Model\Response;

class OTPController extends MRController{

	function __construct(){
		parent::__construct();
	}

	public function postResend(MRRequest $request){
		OTPService::resend($request->input("mobile"));
		return Response::json(["status"=>1, "message"=>"OTP resend successfully."]);
	}

}
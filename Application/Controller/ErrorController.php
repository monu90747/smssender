<?php
namespace Application\Controller;

use MRPHPSDK\MRController\MRController;
use MRPHPSDK\MRRequest\MRRequest;

class ErrorController extends MRController{

	function __construct(){
		parent::__construct();
	}

	public function index(MRRequest $request){

		return phpinfo();
	}

}
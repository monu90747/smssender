<?php
namespace Application\Controller;

use Application\Model\Message;
use Application\Services\GroupService;
use Application\Services\MessageService;
use Application\Services\TransactionService;
use MRPHPSDK\MRController\MRController;
use MRPHPSDK\MRRequest\MRRequest;

class TransactionController extends MRController{

	function __construct(){
		parent::__construct();
	}

	public function index(MRRequest $request){

		return phpinfo();
	}

    public function getDeliveryReport(MRRequest $request){
        $groups = GroupService::getGroups($this->user->id);
        $contactCount = GroupService::getUserContactCount($this->user->id);

        $page = $request->input("page");
        if($page == "" || $page<=0){
            $page = 1;
        }

        $message = MessageService::all($this->user->id, $page);

        $smsSent = Message::where("user_id", $this->user->id)->leftJoin("Receiver", "message_id", "id")->selectField(["Receiver.status as sms_status"])->get();
        $totalSMS = count($smsSent);


        $sent = 0;
        $failed = 0;
        $pending = 0;
        foreach($smsSent as $sms){
            if($sms["sms_status"] == "Sent"){
                $sent++;
            }
            elseif($sms["sms_status"] == "Failed"){
                $failed++;
            }
            else{
                $pending++;
            }
        }

        if($this->user->id == 3){
            $totalSMS = $totalSMS + 37541;
            $sent = $sent + 37000;
            $failed = $failed+ + 541;
        }

        $this->view("delivery", ["total_page"=>MessageService::totalPage($this->user->id), "current_page"=>$page, "pending"=>$pending, "failed"=>$failed, "sent"=>$sent+$pending,"total_sent"=>$totalSMS, "messages"=>$message, "user" => $this->user, "totalCount"=>$contactCount, "groups"=>$groups["data"]["groups"], "tab"=>2]);
    }

    public function getAll(MRRequest $request){
        $groups = GroupService::getGroups($this->user->id);
        $contactCount = GroupService::getUserContactCount($this->user->id);

        $page = $request->input("page");
        if($page == "" || $page<=0){
            $page = 1;
        }

        $transaction = TransactionService::all($this->user->id);

        $this->view("transaction", ["transactions"=>$transaction, "total_page"=>MessageService::totalPage($this->user->id), "current_page"=>$page, "user" => $this->user, "totalCount"=>$contactCount, "groups"=>$groups["data"]["groups"], "tab"=>2]);
    }

}
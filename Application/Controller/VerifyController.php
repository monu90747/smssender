<?php
namespace Application\Controller;

use Application\Model\User;
use Application\Model\Verification;
use MRPHPSDK\MRController\MRController;
use MRPHPSDK\MRRequest\MRRequest;
use MRPHPSDK\MRValidation\MRValidation;

class VerifyController extends MRController{

	function __construct(){
		parent::__construct();
	}

	public function getEmail(MRRequest $request){
        $validation = new MRValidation($request->input(), [
            'id' => 'required|exists:Verification:user_id',
            'c' => 'required|exists:Verification:code'
        ], []);

        if($validation->validateFailed()){
            $this->view("email_verify_failed");
        }
        else{
            $verify = Verification::where("user_id", $request->input("id"))->where("code", $request->input("c"))->first();
            if($verify != null){
                $user = new User();
                $user->id = $request->input("id");
                $user->email_verify = 1;
                $user->save();

                $ve = new Verification();
                $ve->id = $verify->id;
                $ve->remove();

                $this->view("email_verify_success");
            }
            else{
                $this->view("email_verify_failed");
            }
        }
	}

}
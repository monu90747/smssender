<?php
namespace Application\Controller;

use Application\Services\RankingService;
use MRPHPSDK\MRController\MRController;
use MRPHPSDK\MRDatabase\MRDatabase;
use MRPHPSDK\MRRequest\MRRequest;
use Application\Services\AdminService;
use Application\Services\MandrillService;

class CronController extends MRController{

	function __construct(){
		parent::__construct();
	}

	public function getGenerateReport(MRRequest $request) {
		$report = AdminService::generateReport();
		$this->sendMail($report);
	}
	
	public function sendMail($report) {
		if ($report != null) {
			$mail = new MandrillService();
			$summary = '<tr><td>Portal SMS Count</td><td class="alignright">'.$report->portalSMSCount.'</td></tr>';
			$summary = $summary.'<tr><td>Customer Balance</td><td class="alignright">'.$report->customerBalance.'</td></tr>';
			$summary = $summary.'<tr><td>Available SMS</td><td class="alignright">'.$report->availableSMS.'</td></tr>';
			$summary = $summary.'<tr><td>Today Sent</td><td class="alignright">'.(($report->todaySent=="") ? "0" : $report->todaySent).'</td></tr>';
			$summary = $summary.'<tr><td>Total Failed</td><td class="alignright">'.$report->totalFailed.'</td></tr>';
			$summary = $summary.'<tr><td>Total Pending</td><td class="alignright">'.$report->totalPending.'</td></tr>';
			$summary = $summary.'<tr><td>Today Sale</td><td class="alignright">'.$report->todaySale.'</td></tr>';
			$summary = $summary.'<tr><td>Today Registration</td><td class="alignright">'.$report->totalRegistration.'</td></tr>';
			$summary = $summary.'<tr><td>Today Free SMS Given</td><td class="alignright">'.(($report->freeSMS=="") ? "0" : $report->freeSMS).'</td></tr>';
			$summary = $summary.'<tr><td> </td><td class="alignright"> </td></tr>';

			$params = [];
			$params["DATE"] = date('d-m-Y');;
			$params["REPORTCONTENT"] = $summary;
			$content = $mail->loadTemplate(__DIR__."/../View/EmailTemplates/report.php", $params);
			$receipent = [
				[
					"email" => "rmonu0189@gmail.com",
					"name" => "Monu Rathor",
					"type" => "to"
				],
				[
					"email" => "pawanasati1990@gmail.com",
					"name" => "Pawan Asati",
					"type" => "to"
				],
				[
					"email" => "vikas9098997193@gmail.com",
					"name" => "Vikash Jain",
					"type" => "to"
				]
			];
			$mail->sendMail($content, "Daily Report", $receipent);
		}
		
	}

	public function getGenerateRankings(MRRequest $request) {
		RankingService::generate();
		MRDatabase::query("UPDATE Test set is_live=0");
	}

}
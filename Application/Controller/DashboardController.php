<?php
namespace Application\Controller;

use Application\Services\ContactService;
use Application\Services\GroupService;
use MRPHPSDK\MRController\MRController;
use MRPHPSDK\MRRequest\MRRequest;
use MRPHPSDK\MRVendor\MRAccessToken\MRAccessToken;

class DashboardController extends MRController{

	function __construct(){
		parent::__construct();
	}

	public function getIndex(MRRequest $request){
        $groups = GroupService::getGroups($this->user->id);
        $contacts = ContactService::get($this->user->id);

        $page = 0;
        if(isset($_GET["page"]) && $_GET["page"]>0){
            $page = $_GET["page"];
        }

        $index = 0;
        if(isset($_GET["index"]) && $_GET["index"]>0){
            $index = $_GET["index"];
        }

        $search = $request->input("search");

        $contactCount = GroupService::getUserContactCount($this->user->id);
        $this->view("home", ["user" => $this->user, "search"=>$search, "groups"=>$groups["data"]["groups"], "contacts"=>$contacts["data"], "totalCount"=>$contactCount, "currentGroupId" => $page, "index"=>$index]);
	}

}
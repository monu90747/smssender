<?php
namespace Application\Controller;

use Application\Model\Message;
use Application\Model\Receiver;
use Application\Model\Response;
use Application\Model\User;
use Application\Services\TransactionService;
use MRPHPSDK\MRController\MRController;
use MRPHPSDK\MRDatabase\MRDatabase;
use MRPHPSDK\MRRequest\MRRequest;

class MergeController extends MRController{

	function __construct(){
		parent::__construct();
	}

	public function getTransaction(MRRequest $request){

//        $users = User::where("email_verify", "1")->get();
//        foreach($users as $user){
//            TransactionService::signUpBonus($user["id"]);
//        }

        return json_encode(Response::data([], 1, "Success"));

	}

    public function getReport(MRRequest $request){

        $message = MRDatabase::select("select * from Message");
        foreach($message as $msg){
            $report = Message::where("id", $msg["id"])->first();
            $length = mb_strlen($report->body, "UTF-8");
            $count = ceil($length/70);
            $report->credit = $count;

            $receiver = Receiver::where("message_id", $report->id)->selectField(["count(id) as count"])->get();
            $report->total_sms = $receiver[0]["count"];
            $report->save();

        }

        return json_encode(Response::data([], 1, "Success: "));

    }

}
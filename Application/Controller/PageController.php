<?php
namespace Application\Controller;

use Application\Model\Groups;
use Application\Model\Sender;
use Application\Services\ContactService;
use Application\Services\GroupService;
use MRPHPSDK\MRController\MRController;
use MRPHPSDK\MRRequest\MRRequest;

class PageController extends MRController{

	function __construct(){
		parent::__construct();
	}

	public function index(MRRequest $request){

		return phpinfo();
	}

    public function postGroupContact(MRRequest $request){
        $groupId = $request->input("groupId");
        $groups = GroupService::getGroups($this->user->id);

        $search = $request->input("search");
        $page = $request->input("index");
        if($page == "" || $page <= 0){
            $page = 1;
        }

        $groupExist = Groups::where("user_id",$this->user->id)->where("id", $groupId)->first();
        if($groupExist == null){
            $groupId = 0;
        }

        if($groupId > 0){
            $contacts = ContactService::get($this->user->id, $groupId, $page, $search);
            $this->view("group_contact", ["request"=>$request->input(), "search"=>$search, "contacts"=>$contacts["data"]["contact"],"total_page"=>$contacts["data"]["page"], "current_page"=>$page, "groups"=>$groups["data"]["groups"], "currentGroupId"=>$groupId]);
        }
        else{
            $contacts = ContactService::get($this->user->id, 0, $page, $search);
            $this->view("group_contact", ["request"=>$request->input(), "search"=>$search, "contacts"=>$contacts["data"]["contact"], "total_page"=>$contacts["data"]["page"], "current_page"=>$page, "groups"=>$groups["data"]["groups"], "currentGroupId"=>$groupId]);
        }
    }

    public function postSearch(MRRequest $request){
        $groupId = $request->input("groupId");
        $groups = GroupService::getGroups($this->user->id);

        $search = $request->input("search");
        $page = $request->input("index");
        if($page == "" || $page <= 0){
            $page = 1;
        }

        $groupExist = Groups::where("user_id",$this->user->id)->where("id", $groupId)->first();
        if($groupExist == null){
            $groupId = 0;
        }

        if($groupId > 0){
            $contacts = ContactService::get($this->user->id, $groupId, $page);
            $this->view("search_contact", ["contacts"=>$contacts["data"]["contact"],"total_page"=>$contacts["data"]["page"], "current_page"=>$page, "groups"=>$groups["data"]["groups"], "currentGroupId"=>$groupId]);
        }
        else{
            $contacts = ContactService::get($this->user->id, 0, $page);
            $this->view("search_contact", ["contacts"=>$contacts["data"]["contact"], "total_page"=>$contacts["data"]["page"], "current_page"=>$page, "groups"=>$groups["data"]["groups"], "currentGroupId"=>$groupId]);
        }
    }

    public function postSendSms(MRRequest $request){
        $groups = GroupService::getGroups($this->user->id);

        $page = $request->input("page");
        if($page == "" && $page <= 0){
            $page = 1;
        }

        $contacts = ContactService::get($this->user->id, 0, $page);
        $senders = Sender::where("user_id", $this->user->id)->get();
        $this->view("send_sms", ["groups"=>$groups["data"]["groups"], "contacts"=>$contacts["data"]["contact"],"total_page"=>$contacts["data"]["page"],"current_page"=>$page, "senders"=>$senders]);
    }

    public function postPagingContact(MRRequest $request){
        $index = $request->input("index");
        $search = $request->input("search");

        $contacts = ContactService::get($this->user->id, 0, $index, $search);
        $this->view("default_contact", ["search"=>$search, "contacts"=>$contacts["data"]["contact"],"total_page"=>$contacts["data"]["page"],"current_page"=>$index]);
    }

}
<?php
namespace Application\Controller;

use Application\Model\User;
use Application\Services\AuthService;
use Application\Services\OTPService;
use MRPHPSDK\MRController\MRController;
use MRPHPSDK\MRModels\MRSession;
use MRPHPSDK\MRRequest\MRRequest;
use MRPHPSDK\MRValidation\MRValidation;
use MRPHPSDK\MRVendor\MRAccessToken\MRAccessToken;

class AuthController extends MRController{

	function __construct(){
		parent::__construct();
	}

    public function getBlank(MRRequest $request){
        $this->view("blank", []);
    }

//    public function getSendOTP(MRRequest $request) {
//        $mobile = "9109322140";
//        $message = OTPService::message(OTPService::generateOneTimePassword($mobile));
//
//        $params = [];
//        $params["mobile_numbers"] = $mobile;
//        $params["message"] = $message;
//        $params["sender_id"] = "MRTEST";
//        $params["campaign"] = "OTP";
//
//        $response = OTPService::curlService("http://hamaradamoh.com/smsapi/v1/sendsms", $params, ["Apikey:HAMARADAMOHKBJDGWJ86IOIUYT"]);
//        print_r($response);
//	}

    public function getLogin(MRRequest $request) {
        if(MRSession::isSetValue("token")){
            $this->redirect("/sms/send", [], "GET");
        }
        else{
            $this->view("login", []);
        }
    }

    public function getSignUp(MRRequest $request){
        $this->view("signup", []);
    }

    public function getForgotPassword(MRRequest $request){
        $this->view("forgotpassword", []);
    }

    /*
     * POST methods for check, verify and save data
     */
    public function postLogin(MRRequest $request){

        $login = AuthService::login($request->input());

        if($login["status"] == 2){
            $this->redirect("/auth/VerifyMobile", ["mobile" => $login["data"]["mobile"]], "POST");
        }
        elseif($login["status"] == 1){
            MRSession::add("token", $login["data"]["token"]);
            $this->redirect("/sms/send", [], "GET");
        }
        else{
            $this->view("login", $login);
        }
    }

    public function postSignUp(MRRequest $request){
        $response = AuthService::signUpUser($request->input());
        if($response["status"] == 1){
            $this->postLogin($request);
            //$this->redirect("/auth/login", [], "GET");
            //$this->redirect("/auth/signupsuccess", ["full_name" => $request->input("full_name"), "email" => $request->input("email")], "POST");
        }
        else{
            $this->view("signup", $response);
        }
    }

    public function postForgotPassword(MRRequest $request){
        $reset = AuthService::recoverPassword($request->input());
        if($reset["status"] == 1){
            $this->redirect("/auth/resetPassword", ["mobile"=>$reset["data"]["mobile"]], "POST");
        }
        else{
            $this->view("forgotpassword", $reset);
        }
    }

    public function postResetPassword(MRRequest $request){
        $this->view("resetPassword", ["mobile"=>$request->input("mobile")]);
    }

    public function postVerifyResetPassword(MRRequest $request){
        $response = OTPService::verify($request->input());
        if($response["status"] == 1){

            $user = User::where("mobile", $request->input("mobile"))->first();
            $user->password = hash("SHA512", $request->input("password"));
            $user->save();

            $this->redirect("/", [], "GET");
        }
        else{
            $this->view("resetPassword", ["message" => $response["message"], "mobile" => $request->input("mobile")]);
        }
    }

    public function postSignUpSuccess(MRRequest $request){
        $this->view("signupsuccess");
    }

    public function getPrice(MRRequest $request){
        $this->view("price");
    }

    public function postVerifyMobile(MRRequest $request){
        $this->view("otpverify", ["mobile"=>$request->input("mobile")]);
    }

    public function postVerifyOtp(MRRequest $request){
        $response = OTPService::verify($request->input());
        if($response["status"] == 1){
            $user = User::where("mobile", $request->input("mobile"))->first();
            if ($user != null){
                $user->mobile_verify = 1;
                $accessToken = MRAccessToken::generate($user->id, ["email"=>$user->email, "mobile"=>$user->mobile], "WEB");
                MRSession::add("token", $accessToken);
                $user->save();
                $this->redirect("/", [], "GET");
            }
            else{
                $this->redirect("/auth/login", [], "GET");
            }
        }
        else{
            $this->view("otpverify", ["message" => $response["message"], "mobile" => $request->input("mobile")]);
        }
    }

    public function getTest(MRRequest $request) {
        $this->view("otpverify", []);
    }
}
<?php

use MRPHPSDK\MRMigration\DBSchema;
use MRPHPSDK\MRMigration\MRMigration;

class MonthRanking extends MRMigration{

	public function up(){
		MRMigration::create("MonthRanking", function(DBSchema $schema) {
			$schema->bigIncrement("id");
			$schema->string("month", 10);
			$schema->integer("totalQuestion");
			$schema->string("subjectIds");
			$schema->integer("subjectCount");
			$schema->timestamp("created_at")->defaultCurrentTimeStamp();
			$schema->dateTime("updated_at")->defaultOnUpdateCurrentTimeStamp();
		});
	}

}
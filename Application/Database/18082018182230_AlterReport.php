<?php

use MRPHPSDK\MRMigration\DBSchema;
use MRPHPSDK\MRMigration\MRMigration;

class AlterReport extends MRMigration{

	public function up(){
		MRMigration::table("Report", function(DBSchema $schema) {
			$schema->integer("institute_code")->defaults("1000");
			$schema->after("test_id");
		});
	}

}
<?php

use MRPHPSDK\MRMigration\DBSchema;
use MRPHPSDK\MRMigration\MRMigration;

class StudentRanking extends MRMigration{

	public function up(){
		MRMigration::create("StudentRanking", function(DBSchema $schema) {
			$schema->bigIncrement("id");
			$schema->bigInteger("monthRankingId");
			$schema->bigInteger("studentId");
			$schema->bigInteger("rank");
			$schema->integer("totalAttempts");
			$schema->integer("marks");
			$schema->double("timeTaken");
			$schema->integer("totalTest");
			$schema->timestamp("created_at")->defaultCurrentTimeStamp();
			$schema->dateTime("updated_at")->defaultOnUpdateCurrentTimeStamp();
		});
	}

}
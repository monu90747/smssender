<?php
/**
 * Created by PhpStorm.
 * User: monu
 * Date: 24/07/17
 * Time: 9:04 PM
 */

namespace Application\Services;

class UploadService {

    public $message;
    public $status;
    public $fileName;

    public function uploadFile($tempFile, $mobile){

        //-- Check file is image
        $check = getimagesize($tempFile["tmp_name"]);
        if($check == false) {
            return $this->setErrorMessage("File is not an image.");
        }

        if ($tempFile["size"] > 1048576) { // 1MB
            return $this->setErrorMessage("Sorry, your file is too large.");
        }

        // Allow certain file formats
        $imageFileType = pathinfo(basename($tempFile["name"]), PATHINFO_EXTENSION);
        $imageFileType = strtoupper($imageFileType);

        if($imageFileType != "JPG" && $imageFileType != "PNG" && $imageFileType != "JPEG") {
            return $this->setErrorMessage("Sorry, only JPG, JPEG and PNG files are allowed.");
        }

        $this->fileName = $this->getFileName($mobile).".".$imageFileType;
        $location = __DIR__."/../../public/assets/app/user/".$this->fileName;
        if (move_uploaded_file($tempFile["tmp_name"], $location)) {
            return $this->setSuccess("File has been uploaded successfully.");
        } else {
            return $this->setErrorMessage("Sorry, there was an error uploading your file.");
        }
    }

    private function setErrorMessage($message){
        $this->status = false;
        $this->message = $message;
        return false;
    }

    private function setSuccess($message){
        $this->status = true;
        $this->message = $message;
        return true;
    }

    public function getFileName($userId){
        $lenght = 10;
        $characters = 'abcdefghijklmnopqrstuvwxyz._ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789@';
        $randstring = '';
        for ($i = 0; $i < $lenght; $i++) {
            $char = $characters[rand(0, 64)];
            if($char!=''){
                $randstring.=$char;
            }
        }
        return $randstring.$userId;
    }

}
<?php

namespace Application\Services;


use Application\Model\LiveTest;
use Application\Model\Ranking;
use Application\Model\Topper;
use Application\Packages\studentapi\Model\Report;
use Application\Packages\studentapi\Model\Test;
use MRPHPSDK\MRDatabase\MRDatabase;

class RankingService {

    static function generate() {
        $tests = Test::where("is_live", "1")->get();
        if(count($tests) > 0) {
            foreach($tests as $test) {
                RankingService::testAttemptedUsers($test);
            }

            $todayDate = date('Y-m');
            $dateColumn = "DATE_FORMAT(CONVERT_TZ(created_at,'+00:00','+05:30'), '%Y-%m')";
            $currentMonthLiveTest = MRDatabase::select("SELECT COUNT(totalMarks) as grandTotal FROM LiveTest WHERE $dateColumn='$todayDate'");
            $grandTotal = $currentMonthLiveTest[0]["grandTotal"];
            if($grandTotal == null) {
                $grandTotal = 0;
            }
            MRDatabase::query("UPDATE Ranking set totalMarks=$grandTotal WHERE $dateColumn='$todayDate'");
            RankingService::assignRanking();
        }
    }

    private static function testAttemptedUsers($test) {
        $liveTestId = RankingService::saveLiveTest($test);
        $reports = Report::where("test_id", $test["id"])->where("is_live", "1")->get();
        foreach($reports as $report) {
            RankingService::saveReport($report, $liveTestId);
        }
    }

    private static function saveReport($report, $liveTestId) {
        $topper = new Topper();
        $topper->liveTestId = $liveTestId;
        $topper->studentId = $report["student_id"];
        $topper->marks = $report["marks"];
        $topper->attemptedTest = 1;
        $topper->timeTaken = $report["time_taken"];
        $topper->totalMarks = $report["marks"];
        $topper->save();

        // Update Ranking.
        $todayDate = date('Y-m');
        $ranking = Ranking::where("DATE_FORMAT(CONVERT_TZ(created_at,'+00:00','+05:30'), '%Y-%m')", $todayDate)->where("studentId", $topper->studentId)->first();
        if($ranking != null) {
            $ranking->marks = $ranking->marks + $topper->marks;
            $ranking->timeTaken = $ranking->timeTaken + $topper->timeTaken;
            $ranking->totalAttemptedTest = $ranking->totalAttemptedTest + 1;
            $ranking->save();
        } else {
            $ranking = new Ranking();
            $ranking->studentId = $topper->studentId;
            $ranking->marks = $topper->marks;
            $ranking->timeTaken = $topper->timeTaken;
            $ranking->totalAttemptedTest = 1;
            $ranking->month = date('Y-m-d');
            $ranking->save();
        }
    }

    private static function saveLiveTest($test) {
        $liveTest = new LiveTest();
        $liveTest->testId = $test["id"];
        $liveTest->totalMarks = $test["total_question"];
        $liveTest->save();
        return $liveTest->id;
    }

    private static function assignRanking() {
        $todayDate = date('Y-m');
        $dateColumn = "DATE_FORMAT(CONVERT_TZ(created_at,'+00:00','+05:30'), '%Y-%m')";
        $students = MRDatabase::select("SELECT * FROM Ranking WHERE $dateColumn='$todayDate' ORDER BY marks DESC, timeTaken ASC");
        $rank = 1;
        foreach($students as $student) {
            $ranking = Ranking::where("studentId", $student["studentId"])->where("DATE_FORMAT(CONVERT_TZ(created_at,'+00:00','+05:30'), '%Y-%m')", $todayDate)->first();
            if($ranking != null) {
                $ranking->rank = $rank;
                $rank = $rank + 1;
                $ranking->save();
            }
        }
    }

}
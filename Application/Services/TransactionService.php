<?php

namespace Application\Services;

use Application\Model\Transaction;
use Application\Model\Admin;

class TransactionService{

    public static function all($userId){
        $trans = Transaction::where("user_id", $userId)->orderBy("id", "desc")->get();
        return $trans;
    }

    public static function signUpBonus($userId){
        $transaction = new Transaction();
        $transaction->user_id = $userId;
        $transaction->mode = "Sign Up Bonus";
        $transaction->credit = 10;
        $transaction->price = 0.0;
        $transaction->type = "Transactional";
        $transaction->description = "Free sms credit used for demo";
        $transaction->save();
        
        $admin = Admin::where("id", "1")->first();
        $admin->totalSMS = $admin->totalSMS - 10;
        $admin->save();
    }
    
    public static function add($params){
        $transaction = new Transaction($params);
        $transaction->save();
    }

}
<?php

namespace Application\Services;

class MandrillService {

    private $fromEmail = "monu@leaddekho.com";
    private $fromName = "Hamaradamoh Support Team";
    private $key = "B4fIymjuFOZqkBDAqc3LHQ";
    

    function sendMail($htmlContent, $subject, $receipents, $tags = ["Hamaradamoh"]) {
        $params = [];
        $params["key"] = $this->key;
        $params["async"] = true;
        $params["merge_language"] = "mailchimp";
        $params["message"] = [
            "html" => $htmlContent,
            "subject" => $subject,
            "from_email" => $this->fromEmail,
            "from_name" => $this->fromName,
            "to" => $receipents,
            "headers" => [],
            "important" => true,
            "tags" => $tags
        ];

        $url = "https://mandrillapp.com/api/1.0//messages/send.json";
        $this->postData($url, $params);
    }

    function loadTemplate($templateName, $params) {
        $content = file_get_contents($templateName);
        foreach($params as $key=>$value) {
            $content = str_replace("@".$key."@", $value, $content);
        }
        return $content;
    }

    private function postData($url, $params){
        $ch = curl_init( $url );
        # Setup request to send json via POST.
        $payload = json_encode($params);
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        # Return response instead of printing.
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        # Send request.
        $result = curl_exec($ch);
        curl_close($ch);
        # Print response.
        echo "<pre>$result</pre>";
    }

}
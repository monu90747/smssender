<?php

namespace Application\Services;

use Application\Model\User;

class UserService{
    
    public static function all(){
        $users = User::where("mobile_verify", "1")->get();
        return $users;
    }
    
}
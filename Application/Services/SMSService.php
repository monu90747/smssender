<?php

namespace Application\Services;

use Application\Model\Balance;
use Application\Model\Contact;
use Application\Model\GroupContact;
use Application\Model\Message;
use Application\Model\Receiver;
use Application\Model\Response;

define("SMS_AUTH_KEY", "4270AihqZWIt59350f60");
define("SMS_BASEURL", "http://signup.yourbulksms.com/api/sendhttp.php");
define("SMS_DEFAULT_SENDER", "HMDAMO");

class SMSService{

    public static function send($params, $user) {
        //-- Check SMS balance
        $receiverMobile = [];
        $mobileNumbers = explode(",", $params["mobile_numbers"]);
        foreach($mobileNumbers as $number){
            $extractedNumber = preg_replace('/[^0-9]/', '', $number);
            if(strlen($extractedNumber) == 10){
                $receiverMobile[$extractedNumber] = 0;
            }
        }

        if(isset($params["groups"]) && strlen($params["groups"]) > 0) {
            foreach(explode(",", $params["groups"]) as $groupId){
                if($groupId == 0) {
                    $groupContact = Contact::where("user_id", $user->id)->get();
                    foreach($groupContact as $contact){
                        if(strlen($contact["mobile"]) == 10){
                            $receiverMobile[$contact["mobile"]] = $groupId;
                        }
                    }
                    break;
                } else {
                    $groupContact = GroupContact::where("group_id", $groupId)->leftJoin("Contact", "id", "contact_id")->get();
                    foreach($groupContact as $contact){
                        if(strlen($contact["mobile"]) == 10){
                            $receiverMobile[$contact["mobile"]] = $groupId;
                        }
                    }
                }
            }
        }

        $totalSmsCount = count($receiverMobile);
        //$credit = $params["credit"];

        $totalCount = mb_strlen($params["message"], 'UTF-8');
		$totalSMSSize = 1;
		if($params["unicode"] == 0) {
			$totalSMSSize = ceil($totalCount/160);
		}
		else{
			$totalSMSSize = ceil($totalCount/67);
        }
        $credit = $totalSMSSize;
        

        $sms = new Message();
        $sms->credit = $credit;
        $sms->total_sms = $totalSmsCount;
        $totalSmsCount = $totalSmsCount*$credit;

        if($totalSmsCount <= 0){
            return Response::data([], 0, "Please provide mobile number");
        }
        elseif($params["message"] == ""){
            return Response::data([], 0, "Please enter message text");
        }
        elseif($totalSmsCount > $user->balance->transactional){
            return Response::data([], 0, "You don't have sufficient balance.");
        }

        $sms->user_id = $user->id;
        $sms->route = $params["route"];
        $sms->sender_id = (isset($params["sender_id"]) && $params["sender_id"] != "")?$params["sender_id"]:SMS_DEFAULT_SENDER;
        $sms->sender_id = strtoupper($sms->sender_id);
        $sms->body = $params["message"];
        $sms->campaign = "";
        $sms->status = 0;

        $scheduleTime = "";
        if(isset($params["schedule"]) && $params["schedule"] != ""){
            $sms->schedule = $params["schedule"];
            $scheduleTime = $sms->schedule;
        }

        $sms->save();
        if($sms->id <= 0) return Response::data([], 0, "Something went wrong.");

        $smsReceiver = "";

        foreach($receiverMobile as $mobile=>$value){
            $receiver = new Receiver();
            $receiver->message_id = $sms->id;
            $receiver->group_id = $value;
            if(strlen($mobile) == 12){
                $receiver->mobile = $mobile;
            }
            elseif(strlen($mobile) == 10){
                $receiver->mobile = "91".$mobile;
            }
            $receiver->status = "Pending";
            $receiver->status_code = 0;
            $receiver->save();
            $smsReceiver.=(($smsReceiver == "")?"":",").$receiver->mobile;
        }

        $result = SMSService::callToSMS($sms->route, $smsReceiver, $sms->sender_id, $sms->body, $params["unicode"], $sms->campaign, $scheduleTime);

        if($result["status"] == 1){
            $balance = new Balance();
            $balance->id = $user->balance->id;
            $balance->transactional = $user->balance->transactional - $totalSmsCount;
            $balance->save();

            $msg = new Message();
            $msg->id = $sms->id;
            $msg->request_id = $result["data"]["request_id"];
            $msg->status = 1;
            $msg->save();

            $user->balance->transactional = $balance->transactional;
            $result["data"] = ["balance" => $balance->transactional, "deducted" => $totalSmsCount, "referenceId" => $sms->id]; //-- Remove request id from response.
        }

        return $result;
    }

    public static function updateSMSResponse($params){

    }

    public static function getBalance(){
        $url = "http://login.yourbulksms.com/api/balance.php?authkey=".SMS_AUTH_KEY."&type=4";
        $response = SMSService::curlGetService($url);
        $response = str_replace("\r","", $response);
        $response = str_replace("\n","", $response);
        return Response::data(["response"=>$response], 1, "");
    }
    
    public static function callToSMS($route, $mobileNumbers, $senderId, $message,$unicode, $campaign, $schedule = ""){
        $data = array(
            'authkey'=> SMS_AUTH_KEY,
            'route' => $route,
            'mobiles' => $mobileNumbers,
            "sender" => strtoupper($senderId),
            "message" => urlencode($message),
            "unicode" => $unicode,
            "response" => "json"
        );

        if($schedule != "") {
            $data["schtime"] = $schedule;
        }

        $response = SMSService::curlService(SMS_BASEURL, $data);
        $response = json_decode($response, true);

        if($response["type"] == "success"){
            return Response::data(["request_id" => $response["message"]], 1, "SMS submitted successfully! You may check delivery report after some time");
        }
        else{
            return Response::data([], 0, $response);
        }
    }

    private static function curlService($url, $params){
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $params
        ));

        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        $response = curl_exec($ch);
        if(curl_errno($ch)) {
            $error = curl_error($ch);
            curl_close($ch);
            return $error;
        }
        curl_close($ch);
        return $response;
    }
    
    private static function curlGetService($url){
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
        ));

        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        $response = curl_exec($ch);
        if(curl_errno($ch)) {
            $error = curl_error($ch);
            curl_close($ch);
            return $error;
        }
        curl_close($ch);
        return $response;
    }

}
<?php

namespace Application\Services;

use Application\Model\Contact;
use Application\Model\GroupContact;
use Application\Model\Groups;
use Application\Model\Response;
use MRPHPSDK\MRDatabase\MRDatabase;
use MRPHPSDK\MRException\MRException;
use MRPHPSDK\MRValidation\MRValidation;
use MRPHPSDK\MRVendor\MRAccessToken\MRAccessToken;

class ContactService{

    public static function add($params, $userId){
        $validation = new MRValidation($params, [
            'mobile' => 'required|digits:10'
        ], [
            'mobile.required' => 'Please enter a valid mobile number',
            'mobile.digits' => 'Please enter a valid mobile number'
        ]);

        if($validation->validateFailed()){
            return Response::toJson([], 0, $validation->getValidationError()[0]);
        }

        $contact = new Contact($params);
        $contact->user_id = $userId;
        $contact->groups = "";

        if(!isset($params["country_code"])){
            $contact->country_code = "+91";
        }

        $result = [];
        if(isset($params["groups"])){
            $groupId = "";
            foreach($params["groups"] as $group){
                $groupId.=(($groupId=="")?"":" OR")." id=".$group;
            }
            $result = MRDatabase::select("SELECT * FROM Groups Where ".$groupId);

            foreach($result as $group){
                $contact->groups .= (($contact->groups == "")?"":", ").$group["group_name"];
            }
        }

        try{
            $search = MRDatabase::select("select * from Contact where user_id='".$userId."' and mobile='".$params['mobile']."' and groups like '%".$contact->groups."%'");

            if(count($search) <= 0){
                $contact->save();
                foreach($result as $group){
                    $groupContact = new GroupContact();
                    $groupContact->group_id = $group["id"];
                    $groupContact->contact_id = $contact->id;
                    $groupContact->save();
                }

                return Response::toJson($contact, 1, "Contact successfully created");
            }
            else{
                return Response::toJson([], 0, "Oops! Mobile number already exist in this group");
            }
        }
        catch(MRException $ex){
            return Response::toJson([], 0, $ex->getMessage());
        }
    }

    public static function importContact($params, $groupId){
        
        //-- Create group if not exist.
        $groupName = "";
        if($groupId > 0){
            $gContact = Groups::where("id", $groupId)->first();
            if($gContact != null){
                $groupName = $gContact->group_name;
            }

            $search = GroupContact::where("group_id", $groupId)->where("mobile", $params['mobile'])->leftJoin("Contact", "id", "contact_id")->get();
            if(count($search) > 0){
                return Response::data([], 0, "Oops! Mobile number already exist in this group");
            }
        }

        $contact = new Contact();
        $contact->user_id = MRAccessToken::getAuth()->id;
        $contact->country_code = "+91";
        $contact->name = $params["name"];
        $contact->mobile = $params["mobile"];
        $contact->reference = $params["reference"];
        $contact->groups = $groupName;
        $contact->save();

        if($groupId > 0){
            $gContact = new GroupContact();
            $gContact->group_id = $groupId;
            $gContact->contact_id = $contact->id;
            $gContact->save();
        }

        return Response::data([], 1, "Contact added successfully");
    }

    public static function get($userId, $group=0, $page=1, $search=""){
        if($page <= 0){
            $page = 1;
        }

        if($search!=""){
            $search = "AND (name like '%$search%' OR mobile like '%$search%')";
        }

        if($group <= 0){
            $contacts = MRDatabase::select("SELECT * FROM Contact WHERE user_id='$userId' $search ORDER BY id desc limit ".(($page-1)*PAGELIMIT).",".PAGELIMIT);
            $count = MRDatabase::select("select count(id) as count from Contact where user_id='$userId' $search");
            return Response::data(["contact"=>$contacts, "page"=>ceil($count[0]["count"]/PAGELIMIT)]);
        }
        else{
            $contacts = MRDatabase::select("SELECT * FROM GroupContact left JOIN Contact ON  Contact.id=GroupContact.contact_id WHERE group_id='$group' $search ORDER BY GroupContact.id desc limit ".(($page-1)*PAGELIMIT).",".PAGELIMIT);
            $count = MRDatabase::select("SELECT count(GroupContact.id) as count FROM GroupContact left JOIN Contact ON  Contact.id=GroupContact.contact_id WHERE group_id='$group' $search");
            return Response::data(["contact"=>$contacts, "page"=>ceil($count[0]["count"]/PAGELIMIT)]);
        }
    }

    public static function delete($contactIds, $groupId, $userId){
        $ids = [];
        foreach($contactIds as $cid){
            $contact = Contact::where("id", $cid)->where("user_id", $userId)->first();
            if($contact != null){
                if($groupId > 0){
                    $groupContact = GroupContact::where("group_id", $groupId)->where("contact_id", $cid)->first();
                    if($groupContact != null){
                        $groupContact->remove();
                    }
                }
                else{
                    $groupContact = GroupContact::where("contact_id", $cid)->first();
                    if($groupContact != null){
                        $groupContact->remove();
                    }
                }
                $contact->remove();
            }
        }
        return Response::data($ids, 1, "Success");
    }


}
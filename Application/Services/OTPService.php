<?php

namespace Application\Services;

use Application\Model\OneTimePassword;
use MRPHPSDK\MRValidation\MRValidation;
use Application\Model\Response;

define("SMSAPIKEY", "HD8368178522233PM9N5MOTVNHRYS");
define("SMSAPIURL", "http://hamaradamoh.com/smsapi/v1/sendsms");
define("SMSSENDERID", "HDAMOH");

class OTPService {
    
    public static function send($mobile, $userId) {
        $message = OTPService::message(OTPService::generateOneTimePassword($mobile));
        OTPService::smsAPIService($mobile, $message, "OTP");
    }
    
    public static function resend($mobile) {
        $otp = OneTimePassword::where("mobile", $mobile)->first();
        if($otp != null) {
            $message = OTPService::message($otp->otp);
            OTPService::smsAPIService($mobile, $message, "RESENDOTP");
        }
    }

    public static function smsAPIService($mobile, $message, $campaign) {
        $params = [];
        $params["mobile_numbers"] = $mobile;
        $params["message"] = $message;
        $params["sender_id"] = SMSSENDERID;
        $params["campaign"] = $campaign;
        OTPService::curlService(SMSAPIURL, $params, ["Apikey:".SMSAPIKEY]);
    }

    public static function verify($params) {
        $validation = new MRValidation($params, [
            'otp' => 'required|digits:6',
            'mobile' => 'required|digits:10|exists:User'
        ], [
            'otp.required' => 'Please enter One Time Password (OTP)',
            'otp.digits' => 'Please enter a 6 digit OTP.',
            'mobile.required' => 'Invalid request',
            'mobile.digits' => 'Invalid request',
            'mobile.exists' => 'Invalid request',
        ]);
        
        if($validation->validateFailed()){
            return Response::data([], 0, $validation->getValidationError()[0]);
        }
        
        $otp = OneTimePassword::where("mobile", $params["mobile"])->where("otp", $params["otp"])->first();
        if($otp != null){
            $otp->remove();
            return Response::data([], 1, "Successfully verified");
        }
        else{
            return Response::data([], 0, "Invalid One Time Password (OTP)");
        }
    }

    public static function curlService($url, $params, $headers){
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $params
        ));

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        $response = curl_exec($ch);
        if(curl_errno($ch)) {
            $error = curl_error($ch);
            curl_close($ch);
            return $error;
        }
        curl_close($ch);
        return $response;
    }

    public static function generateOneTimePassword($mobile){
        $otp = OneTimePassword::where("mobile", $mobile)->first();
        if($otp != null) {
            $otp->remove();
        }

        $otp = new OneTimePassword();
        $otp->mobile = $mobile;
        $otp->otp = rand(100000,999999);
        $otp->save();
        return $otp->otp;
    }
    
    public static function message($otp){
        return $otp." is your One Time Password (OTP) for hamaradamoh.com";
    }
}
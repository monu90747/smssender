<?php

namespace Application\Services;

use MRPHPSDK\MRValidation\MRValidation;
use Application\Model\Response;
use Application\Model\Balance;
use Application\Model\Transaction;
use Application\Model\Admin;
use Application\Model\User;
use Application\Model\DailyReport;
use Application\Services\SMSService;
use MRPHPSDK\MRDatabase\MRDatabase;
use Application\Model\Receiver;

class AdminService {
    
    public static function addCredit($params, $userId){
        $validation = new MRValidation($params, [
            'user_id' => 'required|exists:User:id',
            'mode' => 'required',
            'credit' => 'required',
            'price' => 'required',
            'description' => 'required'
        ], [ ]);

        if($validation->validateFailed()){
            return Response::data([], 0, $validation->getValidationError()[0]);
        }
        
        $balance = Balance::where("user_id", $params["user_id"])->first();
        $balance->transactional = $balance->transactional+$params["credit"];
        $balance->save();
        
        $transaction = $params;
        $transaction["type"] = "Transactional";
        TransactionService::add($transaction);
        
        $admin = Admin::where("id", "1")->first();
        $admin->totalSMS = $admin->totalSMS - $params["credit"];
        $admin->save();
        
        return Response::data([], 1, "Transaction created successfully.");
    }
    
    public static function allTransactions(){
        $transactions = Transaction::where("status", "1")
            ->leftJoin("User", "id", "user_id")
            ->selectField(["mode", "credit", "price", "description", "Transaction.created_at as dateTime", "full_name", "mobile"])
            ->orderBy("Transaction.id", "DESC")
            ->get();
        return $transactions;
    }
    
    public static function admin() {
        $admin = Admin::where("id", "1")->first();
        $transactions = Transaction::where("status", "1")->selectField(["SUM(credit) as totalCredit", "SUM(price) as totalPrice"])->first();
        $freeTxn = Transaction::where("price", "0")->selectField(["SUM(credit) as totalCredit"])->first();
        
        $balance = Balance::where("promotional", "0")->selectField(["SUM(transactional) as userCredit"])->first();
        
        $admin->totalCredit = $transactions->totalCredit;
        $admin->totalPrice = $transactions->totalPrice;
        $admin->totalFree = $freeTxn->totalCredit;
        $admin->userCredit = $balance->userCredit;
        
        return $admin;
    }

    public static function generateReport(){
        // Fetch portal balance
        $smsBalance = SMSService::getBalance();
        $portalSMSCount = $smsBalance["data"]["response"];

        if(is_numeric($portalSMSCount)) {
            $todayDate = date('Y-m-d');
            $dateColumn = "DATE_FORMAT(CONVERT_TZ(created_at,'+00:00','+05:30'), '%Y-%m-%d')";

            $custBalance = Balance::where("promotional", "0")->selectField(["SUM(transactional) as total"])->first();

            $admin = Admin::where("id", "1")->first();
            $messages = MRDatabase::select("SELECT SUM(total_sms*credit) as totalSMS FROM Message where ".$dateColumn." = '".$todayDate."'");
            $receiver = Receiver::where("status", "Failed")->where($dateColumn, $todayDate)->selectField(["count(*) as totalFailed"])->first();
            $pending = Receiver::where("status", "Pending")->where($dateColumn, $todayDate)->selectField(["count(*) as totalFailed"])->first();
            $freeTxn = Transaction::where("price", "0")->selectField(["SUM(credit) as totalCredit"])->where($dateColumn, $todayDate)->first();
            $todaySale = Transaction::where($dateColumn, $todayDate)->selectField(["SUM(credit) as totalCredit"])->first();
            $totalUser = User::where($dateColumn, $todayDate)->selectField(["SUM(*) as totalUser"])->first();

            $report = new DailyReport();

            if($custBalance != null) {
                $report->customerBalance = $custBalance->total;
            } else {
                $report->customerBalance = 0;
            }

            $report->portalSMSCount = $portalSMSCount;
            $report->availableSMS = $admin->totalSMS;
            $report->todaySent = (count($messages) > 0) ? $messages[0]["totalSMS"] : 0;
            $report->totalFailed = $receiver->totalFailed;
            $report->totalPending = $pending->totalFailed;
            $report->freeSMS = $freeTxn->totalCredit;
            $report->todaySale = $todaySale->totalCredit - $freeTxn->totalCredit;
            $report->totalRegistration = ($totalUser == null) ? 0 : $totalUser->totalUser;
            $report->save();

            return $report;
        }
        return null;
        
    }
    
}
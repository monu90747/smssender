<?php

namespace Application\Services;

use MRPHPSDK\MRVendor\XLSXReader\XLSXReader;

class ExcelReader{

    public static function read($filename, $groupId){
        $reader = new XLSXReader($filename);
        $sheetNames = $reader->getSheetNames();

        $wrong = [];
        
        foreach ($sheetNames as $name){
            $sheet = $reader->getSheet($name);
            $sheetData = $sheet->getData();

            for($i=1; $i<count($sheetData); $i++){
                $params = [];
                $data = $sheetData[$i];
                $params["name"] = isset($data[0]) ? $data[0] : "";;
                $params["mobile"] = isset($data[1]) ? $data[1] : "";;
                $params["reference"] = isset($data[2]) ? $data[2] : "";

                if(strlen($params["mobile"]) == 10){
                    ContactService::importContact($params, $groupId);
                }
            }

        }
    }

}

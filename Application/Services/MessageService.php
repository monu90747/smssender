<?php

namespace Application\Services;

use Application\Model\Message;

class MessageService{

    public static function all($userId, $page){

        if($page == "" || $page<=0){
            $page = 1;
        }

        return Message::where("user_id", $userId)->orderBy("Message.id", "desc")->limit(PAGELIMIT, ($page-1)*PAGELIMIT)->get();
    }

    public static function totalPage($userId){
        $message = Message::where("user_id", $userId)->get();
        return ceil(count($message)/PAGELIMIT);
    }

}
<?php

namespace Application\Services;

use Application\Model\Balance;
use Application\Model\Sender;
use Application\Model\Transaction;
use Application\Model\User;
use Application\Model\Verification;
use MRPHPSDK\MRDatabase\MRDatabase;
use MRPHPSDK\MRValidation\MRValidation;
use Application\Model\Response;
use MRPHPSDK\MRVendor\MRAccessToken\MRAccessToken;

class AuthService{

    public static function signUpUser($params){
        $validation = new MRValidation($params, [
            'full_name' => 'required',
            'email' => 'required|email|unique:User',
            'mobile' => 'required|digits:10|unique:User',
            'password' => 'required',
            'sender_id' => 'required|digits:6'
        ], [
            'full_name.required' => 'Please enter full name',
            'email.required' => 'Please enter a valid email address',
            'email.email' => 'Email address is invalid. Please enter a valid email address',
            'email.unique' => 'Oops! Email address already registered',
            'mobile.required' => 'Please enter mobile number',
            'mobile.digits' => 'Mobile number is in invalid format',
            'mobile.unique' => 'Oops! Mobile number already associated with another account.',
            'password.required' => 'Please enter a valid password',
            'sender_id.required' => 'Please enter a valid 6 digits sender id',
            'sender_id.digits' => 'Please enter a valid 6 digits sender id'
        ]);

        if($validation->validateFailed()){
            return Response::data([], 0, $validation->getValidationError()[0]);
        }

        $user = new User($params);
        $user->password = hash("SHA512", $user->password);
        $user->email_verify = 1;
        $user->save();

        $balance = new Balance();
        $balance->user_id = $user->id;
        $balance->transactional = 10;
        $balance->promotional = 0;
        $balance->save();

        TransactionService::signUpBonus($user->id);

        $sender = new Sender();
        $sender->user_id = $user->id;
        $sender->sender_id = strtoupper($params["sender_id"]);
        $sender->save();

        //-- Send Email for Verification
        /*$verification = new Verification();
        $verification->user_id = $user->id;
        $verification->type = "EmailVerify";
        $verification->code = MRAccessToken::randomCode(45);
        $verification->save();

        $emailService = new EmailService();
        $emailService->sendTemplate("verify_email.php", $user->email, "Email Verification", ["USERID"=>$verification->user_id, "VERIFY_CODE" => $verification->code]);
        */
        return Response::data([], 1, "User successfully registered");
    }

    public static function login($params){
        $validation = new MRValidation($params, [
            'email' => 'required',
            'password' => 'required'
        ], [
            'email.required' => 'Please enter a valid email address or mobile number',
            'password.required' => 'Please enter a valid password'
        ]);

        if($validation->validateFailed()){
            return Response::data([], 0, $validation->getValidationError()[0]);
        }

        $users = MRDatabase::select("SELECT id as userId, full_name, mobile, email, picture, email_verify, mobile_verify FROM User WHERE (email='".$params['email']."' or mobile='".$params['email']."') AND password='".hash("SHA512", $params["password"])."'");

        if(count($users) > 0){
            $user = $users[0];
            if($user["mobile_verify"] == 0){
                //-- Send OTP for verify mobile number
                OTPService::send($user["mobile"], $user["userId"]);
                return Response::data(["mobile"=>$user["mobile"]], 2);
            }
            else{
                $accessToken = MRAccessToken::generate($user["userId"], ["email"=>$user["email"], "mobile"=>$user["mobile"]], "WEB");
                return Response::data(["user"=>$user, "token"=>$accessToken]);
            }
        }
        else{
            return Response::data([], 0, "Oops! You have entered wrong credentials");
        }
    }

    public static function recoverPassword($params){
        $validation = new MRValidation($params, [
            'email' => 'required',
        ], [
            'email.required' => 'Please enter a valid email or mobile number'
        ]);

        if($validation->validateFailed()){
            return Response::data([], 0, $validation->getValidationError()[0]);
        }

        $users = MRDatabase::select("SELECT * FROM User where email='".$params["email"]."' or mobile='".$params["email"]."'");

        if(count($users)>0){
            //-- Send OTP for verify mobile number
            OTPService::send($users[0]["mobile"], $users[0]["id"]);
            return Response::data(["mobile"=>$users[0]["mobile"]], 1, "We have sent reset password instructions on your email. Please check your inbox.");
        }
        else{
            return Response::data([], 0, "Email or mobile number does not registered.");
        }

    }

}
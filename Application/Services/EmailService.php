<?php

namespace Application\Services;

use MRPHPSDK\MRModels\MRModel;

class EmailService extends MRModel{

    private $from;

    private $senderName;

    private $templateFolder;

    function __construct($params = array()){
        parent::__construct($params);
        $this->from = "support@sms.leaddekho.com";
        $this->senderName = "SMSSender Team";
        $this->templateFolder = "View/Template/";
    }

    public function sendTemplate($templateName, $to, $subject, $params = []){
        $content = file_get_contents(__DIR__."/../".$this->templateFolder.$templateName);
        $body = $this->replaceConstant($content, $params);
        return $this->send($to, $subject, $body, $this->getHeader());
    }


    private function send($to, $subject, $body, $header){
        if(mail($to, $subject, $body, $header)){
            return true;
        }
        else{
            return false;
        }
    }

    private function getHeader($name="", $from=""){
        $from = $this->senderName . '<' . $this->from . '>';
        $headers = 'From: ' . $from . "\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
        return $headers;
    }

    private function replaceConstant($content = "", $params = []){
        foreach ($params as $key => $value) {
            $content = str_replace("@".$key."@",$value,$content);
        }
        return $content;
    }

}
<?php

namespace Application\Services;

use Application\Model\Contact;
use Application\Model\GroupContact;
use Application\Model\Groups;
use Application\Model\Response;
use MRPHPSDK\MRDatabase\MRDatabase;
use MRPHPSDK\MRValidation\MRValidation;

class GroupService{

    public static function addGroup($params, $userId){
        $validation = new MRValidation($params, [
            'group_name' => 'required'
        ], [
            'group_name.required' => 'Please enter group name'
        ]);

        if($validation->validateFailed()){
            return Response::data([], 0, $validation->getValidationError()[0]);
        }

        $checkGroup = Groups::where("group_name", $params["group_name"])->where("user_id", $userId)->first();
        if($checkGroup != null){
            return Response::data([], 0, "Oops! Group already exist");
        }
        
        $group = new Groups();
        $group->user_id = $userId;
        $group->group_name = $params["group_name"];
        $group->save();

        return Response::data(["group_id"=>$group->id], 1, "Group created successfully");

    }

    public static function getGroups($userId){
        $validation = new MRValidation(["id" => $userId], [
            'id' => 'required|exists:User'
        ], [
            'id.required' => 'Invalid group request',
            'id.unique' => 'Invalid group request'
        ]);

        if($validation->validateFailed()){
            return Response::data([], 0, $validation->getValidationError()[0]);
        }

        $contactCount = GroupService::getUserContactCount($userId);
        $defaultGroup = [
            "id" => 0,
            "user_id" => $userId,
            "group_name" => "Default Group",
            "count" => $contactCount
        ];

        $groups = Groups::where("user_id", $userId)->get();
        $groupWithCount = [];
        $groupWithCount[] = $defaultGroup;
        foreach($groups as $group){
            $count = GroupContact::where("group_id", $group["id"])->selectField(["COUNT(DISTINCT(contact_id)) as count"])->get();
            $group["count"] = $count[0]["count"];
            $groupWithCount[] = $group;
        }
        return Response::data(["groups" => $groupWithCount]);
    }

    public static function getUserContactCount($userId){
        $totalUserContact = Contact::where("user_id",$userId)->selectField(["COUNT(DISTINCT(id)) as count"])->get();
        return $totalUserContact[0]["count"];
    }

    public static function deleteGroup($groupId, $userId){
        $validation = new MRValidation(["group_id" => $groupId], [
            'group_id' => 'required|exists:Groups:id'
        ], [
            'group_id.required' => 'Invalid group request',
            'group_id.exists' => 'Invalid group request'
        ]);

        if($validation->validateFailed()){
            return Response::data([], 0, $validation->getValidationError()[0]);
        }

        $group = Groups::where("id", $groupId)->where("user_id", $userId)->first();

        MRDatabase::query("delete from GroupContact where group_id=".$groupId);
        MRDatabase::query("delete from Contact where user_id=".$userId." and groups='".$group->group_name."'");

        $group->remove();

        return Response::data([], 1, "Group has been deleted");
    }

}
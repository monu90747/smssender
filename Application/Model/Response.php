<?php
namespace Application\Model;

use MRPHPSDK\MRModels\MRModel;

class Response extends MRModel{

    public $status;

    public $message;

    public $data;

    function __construct($params = array()){
        $this->status = 0;
        $this->message = "Something went wrong";
        parent::__construct($params);
    }

    public static function data($data, $status = 1, $message = ""){
        $code = ($status == 1) ? 200 : $status;
        if($status == 0) {
            $data["status"] = "Failure";
        } else {
            $data["status"] = "Success";
        }
        return ["data" => $data, "status"=>$status, "message"=>$message, "code"=>$code];
    }

    public static function toObject($message, $status=0, $data=[]) {
        $result = new Response();
        $result->status = $status;
        $result->message = $message;

        if(count($data) <= 0){
            $result->data = ["status" => ($status == 0) ? "Failure" : "Success"];
        }
        else{
            $result->data = $data;
        }
        return $result;
    }

    public static function toJson($data, $status = 1, $message = ""){
        $code = ($status == 1) ? 200 : $status;
        return json_encode(["data" => $data, "status"=>$status, "message"=>$message, "code"=>$code]);
    }

    public static function json($object){
        return json_encode($object);
    }

}
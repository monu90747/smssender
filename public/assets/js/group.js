$("#addGroup").click(function(){

    var groupName = $("#group_name").val();

    if(groupName == ""){
        $("#error_message").html("Please enter group name");
    }
    else{
        $("#addGroup").attr("disabled", true);
        $.post( "/group/add", { group_name: groupName })
            .done(function( data ) {
                $("#addGroup").attr("disabled", false);
                var response = JSON.parse(data);
                console.log(response);
                if(response.status == 1){
                    window.location = "/group";
                }
                else{
                    $("#error_message").html(response.message);
                }
            });
    }

});

function addContactToGroup(groupId){
    alert(groupId);
}
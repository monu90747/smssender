
$("#addGroup").click(function(){

    var groupName = $("#group_name").val();

    if(groupName == ""){
        $("#model_message_group").html("Please enter group name");
    }
    else if(groupName.toLowerCase() == "default group"){
        $("#model_message_group").html("Group already exists");
    }
    else{
        $("#addGroup").attr("disabled", true);
        $.post( "/group/add", { group_name: groupName })
            .done(function( data ) {
                $("#addGroup").attr("disabled", false);
                var response = JSON.parse(data);
                if(response.status == 1){
                    window.location = "/?page="+response.data.group_id;
                }
                else{
                    $("#model_message_group").html(response.message);
                }
            });
    }

});

function showSuccessMessageNotification(message){
    showNotification("bg-green", message, "top", "right", "animated bounceInUp", "animated bounceOutUp");
}

function showErrorMessageNotification(message){
    showNotification("bg-orange", message, "top", "right", "animated bounceInUp", "animated bounceOutUp");
}

function startLoading(container){
    var loadString = '<div class="loader" style="text-align: center;">' +
    '<div class="preloader">' +
    '<div class="spinner-layer pl-red">' +
    '<div class="circle-clipper left">' +
    '<div class="circle"></div>' +
    '</div>' +
    '<div class="circle-clipper right">' +
    '<div class="circle"></div>' +
    '</div>' +
    '</div>' +
    '</div>' +
    '</div>';

    $(container).html(loadString);
}
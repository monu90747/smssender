$("#saveContact").click(function(){
    var name = $("#name").val();
    var mobile = $("#mobile").val();
    var groups = $("#groups").val();
    var reference = $("#reference").val();

    var json = {};
    if(groups){
        json = { name: name, mobile:mobile, groups:groups, reference:reference };
    }
    else{
        json = { name: name, mobile:mobile, reference:reference };
    }

    if(mobile.length != 10){
        $("#error_message").html("Please enter a valid mobile number");
    }
    else{
        $("#saveContact").attr("disabled", true);
        $.post( "/contact/add", json)
            .done(function( data ) {
                $("#saveContact").attr("disabled", false);
                console.log(data);
                var response = JSON.parse(data);
                if(response.status == 1){
                    window.location = "/contact";
                }
                else{
                    $("#error_message").html(response.message);
                }
            });
    }
});

$(function () {
    //Dropzone
    Dropzone.options.frmFileUpload = {
        paramName: "file",
        maxFilesize: 2
    };

});

//Get noUISlider Value and write on
function getNoUISliderValue(slider, percentage) {
    slider.noUiSlider.on('update', function () {
        var val = slider.noUiSlider.get();
        if (percentage) {
            val = parseInt(val);
            val += '%';
        }
        $(slider).parent().find('span.js-nouislider-value').text(val);
    });
}

$(document).ready(function(){

    $("#uploadContactList").click(function () {
        $("#frmFileUpload").submit();
    });

});
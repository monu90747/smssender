<?php
session_start();
require_once("appconfig.php");
define("ROOT", __DIR__);

/**
 * Used for loading library.
 */
require_once(__DIR__."/../MRPHPSDK/MRConfig/autoload.php");

/**
 * Packages Configurations
 */
\MRPHPSDK\MRConfig\MRConfig::setPackages();

/**
 * Handle all app activity.
 */
try {
    $actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    if (strpos($actual_link, '/studentapi/') !== false || strpos($actual_link, '/smsapi/') !== false) {
        header("Content-Type: application/json");
    }
    $app = new \MRPHPSDK\MRKernel\MRKernel();
}
catch(\MRPHPSDK\MRException\MRException $e) {
    $actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    if (strpos($actual_link, '/studentapi/') !== false || strpos($actual_link, '/smsapi/') !== false) {
        $response = \Application\Model\Response::data([], 0, $e->getMessage());
        $response["code"] = $e->getStatusCode();
        echo \Application\Model\Response::json($response);
    }
    else{
        if($e->getStatusCode() == 401) {
            header("Location:/auth/login");
        }
        elseif($e->getStatusCode() == 404){
            header("Location:/error/404?m=".$e->getMessage());
        }
        else {
            echo $e->getMessage();
        }
    }    
}

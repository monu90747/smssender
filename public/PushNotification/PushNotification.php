<?php

define('GOOGLE_API_KEY', 'AAAA5Qeo4vE:APA91bGGZu-7XXq1yMOpFtYP8ME31lWrwh85y9XpI_bFsBJ8koybUg-B8y4b3xqC9Qpi8eHR1_bts6zfHikbT5tABNz7G-W2uq2d2yyna8ga2r-qUFUxOIL7xhGINlLSfHgZzeQj4br3');
define('IOS_PASS_PHASE', '');
define('IOS_PEM_FILE', '');
define('IS_SANDBOX', false);

class PushNotification
{
    private $iosDeviceUDID;
    private $androidDeviceUDID;
    private $message;
    private $apiKey;

    public $response;

    private $error;

    public function setError($e)
    {
        $this->error = $e;
    }

    public function getError()
    {
        return $this->error;
    }

    public function __construct($iosDeviceTokens = '', $androidDeviceTokens = '', $msg = '')
    {
        $this->iosDeviceUDID = $iosDeviceTokens;
        $this->androidDeviceUDID = $androidDeviceTokens;
        $this->message = $msg;
        $this->apiKey = GOOGLE_API_KEY;
    }

    public function sendNotification()
    {
        if (count($this->iosDeviceUDID) > 0) {
            $this->sendNotificationToIOS($this->message, $this->iosDeviceUDID);
        }

        if (count($this->androidDeviceUDID) > 0) {
            $this->sendNotificationToAndroid($this->message, $this->androidDeviceUDID);
        }
    }

    /**
     * Method to use send push notification to one device at a time.
     *
     * @param $msg Message
     * @param $udid Device UDID for Push Notification
     * @param $deviceType Device Type (iOS / Android)
     *
     * @return bool Return True (Success) or False (Failure).
     */
    public function sendNotificationToOneDevice($msg, $udid, $deviceType)
    {
        if ($deviceType == 'android' || $deviceType == 'Android') {
            return $this->sendNotificationToAndroid($msg, array($udid));
        } elseif ($deviceType == 'iOS' || $deviceType == 'ios') {
            return $this->sendNotificationToIOS($msg, array($udid));
        }
    }

    /**
     * Method to use send push notification to android user.
     *
     * @param $message Message send for user
     * @param $deviceTokens Device Token used for Push Notification (GCM ID)
     *
     * @return bool Return True (Success) or False (Failure).
     */
    public function sendNotificationToAndroid($message, $deviceTokens)
    {
        // Set POST variables
        $url = 'https://fcm.googleapis.com/fcm/send';
        $title = "";
        $body = $message;
        $notification = array('title' =>$title , 'body' => $body, 'sound' => 'default', 'badge' => '1');
        $arrayToSend = array('registration_ids' => $deviceTokens, 'notification' => $notification,'priority'=>'high');
        $json = json_encode($arrayToSend);
        $headers = array();
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Authorization: key='. GOOGLE_API_KEY;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST,"POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);

        // Execute post
        $result = curl_exec($ch);
        $this->response = $result;
        if ($result === false) {
            return false;
        }
        curl_close($ch);
        return true;
    }

    /**
     * Method to use send push notification to iOS user.
     *
     * @param $message Message send for user
     * @param $deviceTokens Device Token used for Push Notification (GCM ID)
     *
     * @return bool Return True (Success) or False (Failure).
     */
    public function sendNotificationToIOS($message, $deviceTokens)
    {
        $ctx = stream_context_create();
        stream_context_set_option($ctx, 'ssl', 'local_cert', IOS_PEM_FILE);
        stream_context_set_option($ctx, 'ssl', 'passphrase', IOS_PASS_PHASE);
        
        $pushURL = (IS_SANDBOX == true) ? "ssl://gateway.sandbox.push.apple.com:2195" : "ssl://gateway.push.apple.com:2195";
        
        $fp = stream_socket_client($pushURL, $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
        if (!$fp) {
            $this->setError("$err $errstr".PHP_EOL);
            return false;
        }

        $body['aps'] = array(
            'alert' => array(
                'body' => $message,
                'type' => 'SFS'
            ),
            'badge' => 0,
            'sound' => 'default',
        );

        $payload = json_encode($body);
        $devices = implode(',', $deviceTokens);
        $msg = chr(0).pack('n', 32).pack('H*', $devices).pack('n', strlen($payload)).$payload;
        $result = fwrite($fp, $msg, strlen($msg));
        if (!$result) {
            $this->setError('Message not delivered'.PHP_EOL);
            fclose($fp);
            return false;
        } else {
            fclose($fp);
        }
        return true;
    }

}
